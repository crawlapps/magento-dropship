# Shopify APP - Magento Dropship app 

### Installation
Install the dependencies and devDependencies.

For both environment
```sh
$ composer install
$ cp .env.example .env 
$ nano .env // set all credentials(ex: database, shopify api key and secret, mail credentials)
$ php artisan key:generate
$ php artisan migrate
```

(1) Edit config in .env 

    QUEUE_CONNECTION=database
       

    $ php artisan queue:table




 (2) Update Database Details in .env file
 
 
     DB_CONNECTION=mysql
     DB_HOST=127.0.0.1
     DB_PORT=3306
     DB_DATABASE=laravel
     DB_USERNAME=root
     DB_PASSWORD=

_Below second Database setting is Magento database Details store._

    DB_CONNECTION_SECOND=mysql
    DB_HOST_SECOND=3.13.46.7
    DB_PORT_SECOND=3306
    DB_DATABASE_SECOND=
    DB_USERNAME_SECOND=
    DB_PASSWORD_SECOND=


  (3) Edit Shopify APP Credential Here in .env file
  
    SHOPIFY_API_KEY=
    SHOPIFY_API_SECRET=


 (4) For development environments...

```sh
$ npm install
$ npm run dev
```
   For production environments...

```sh
$ npm install --production
$ npm run prod
```
  (5) Setup superviser for Queue JOB in Server
  
       


Extra commands

```sh
$ sudo supervisorctl reread && sudo supervisorctl update && sudo supervisorctl restart [superviser-name]
```

  (6) Setup CRON job in server
  
  **$ crontab -e**
  
  ```cd /var/www/html/shopify-apps/magento-dropship && php artisan schedule:run >> /dev/null 2>&1``` 


   Note* : **/var/www/html/shopify-apps/magento-dropship**
           _this Url is your server project root path._


  (7) Run This commands in project root for Storage permisstion.
  
      $ sudo chown -R www-data:www-data storage
      $ sudo chmod -R 775 storage
      $ sudo chmod -R 775 bootstrap/cache
        
   (8) Use This Command for clear cache App
   
     $  php artisan optimize:clear     
     $  php artisan config:clear
     $  php artisan cache:clear
     

   
### Used Shopify Tools

* Admin rest-api, graphQL api
* App-bridge


