<?php namespace App\Jobs;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Osiset\ShopifyApp\Objects\Values\ShopDomain;
use stdClass;
use DB;
class ProductsDeleteJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Shop's myshopify domain
     *
     * @var ShopDomain|string
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;

    /**
     * Create a new job instance.
     *
     * @param string   $shopDomain The shop's myshopify domain.
     * @param stdClass $data       The webhook data (JSON decoded).
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        try {
            logger("===============START :: Product Delete Job ===============");

            logger("shopDomain");
            logger($this->shopDomain);

            // Convert domain
            //$this->shopDomain = ShopDomain::fromNative($this->shopDomain);

            $shop = User::where('name', $this->shopDomain)->first();

            $sh_product_id = $this->data->id;

            logger("Delete :: Product ID :: ".$sh_product_id);


            $product = DB::table("products")->where('user_id', $shop->id)->where('shopify_id', $sh_product_id)->first();


            if( $product ){
                $productID = $product->id;

                logger('===== Delete Product Webhook :: Shop ID :: ' .$shop->id .' Product ID :: '.$productID.' ========');

                $variantRecs = DB::table('variants')->where('product_id', $productID)->get();


                foreach ($variantRecs as $vRec) {
                    DB::table('attributes')->where('variant_id', $vRec->id)->delete();
                }

                // Delete variants
                DB::table('variants')->where('product_id', $productID)->delete();

                // Delete images
                DB::table('images')->where('product_id', $sh_product_id)->delete();

                // Delete tags
                DB::table('tags')->where('product_id', $productID)->delete();

                // Delete product rec
                DB::table('products')->where('id', $productID)->delete();
            }


            logger("===============END :: Product Delete Job ===============");
        }catch(\Exception $e ){
            logger('ERROR :: Delete Product Webhook');
            logger(json_encode($e));
        }

        // Do what you wish with the data
        // Access domain name as $this->shopDomain->toNative()
    }
}
