<?php

namespace App\Jobs;

use App\Models\User;
use App\Traits\MagentoExportInfoTrait;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use DB;
class AddCollectionsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use MagentoExportInfoTrait;

    private $shop_id = '';
    private $data = [];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($shop_id,$data)
    {
        $this->shop_id = $shop_id;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        logger("===============START :: JOB :: Add Collections ===============");
        $shop = User::where('id', $this->shop_id)->first();
        logger(json_encode($this->data));

        $membership = null;
        $membership =  $this->getMembership($shop->id);

       // $MagentoStoreID = getMagentoStoreID($membership);

        try{
            $selectedCollections =  (array) $this->data;

            $collections = DB::table('magento_catalog_category_entity as cat')
                ->join('magento_catalog_category_entity_varchar as cv', 'cat.entity_id', '=', 'cv.entity_id')
                ->join('magento_eav_attribute as att', 'att.attribute_id', '=', 'cv.attribute_id')
                ->join('magento_eav_entity_type as aty', 'att.entity_type_id', '=', 'aty.entity_type_id')
                ->select('cat.*','cv.value as category_name')
                ->whereNotNull('cv.value')
                ->where('aty.entity_type_code','=','catalog_category')
                ->where('att.attribute_code','=','name')
                ->whereIn('cat.entity_id',$selectedCollections)
                ->get();

            $shope_data = getShopData($shop->id); //Helper

            if(isset($collections) && count($collections)>0) {

                logger("==========Selected collections are ============");

                for ($i = 0; $i < count($collections); $i++) {

                    logger("===============Create a new collections::Shopify== ".$i);

                    $collectionInfo = $collections[$i];

                    $shopifyVariants = [];

                    logger("===============Collection name====================");

                    logger($collectionInfo->category_name);

                    // Initialize Shopify product
                    $shopifyCollection = [
                        'title' => $collectionInfo->category_name,
                        'metafields' => [
                            [
                                'key' => 'magentodropship',
                                'value' => 'bymagento',
                                'value_type' => 'string',
                                'namespace' => 'dropshipper'
                            ]
                        ]
                    ];

                    $collectionReq = $shop->api()->rest('POST', '/admin/api/'. env('SHOPIFY_API_VERSION') .'/custom_collections.json', ['custom_collection' => $shopifyCollection]);

                    logger("custom_collections::Resposne");
                    logger(json_encode($collectionReq));


                    $get_asset_call_limit = collect($collectionReq['response']->getHeader('x-shopify-shop-api-call-limit'))->first();

                    if ($get_asset_call_limit == '39/40') {

                        logger("Delay the script execution for 1 seconds ");
                        logger("Get assets, Waiting 1 seconds for a credit");
                        sleep(1);

                    } // Delay the script execution for 1 seconds }


                    if ($collectionReq['errors']) {
                        throw new \Exception('Shopify API error: '.json_encode((array) $collectionReq['body']));
                    }

                    if (!$collectionReq['body']['custom_collection']['id']) {
                        throw new \Exception('Could not retreieve collection id from Shopify API');
                    }

                    if (!$collectionReq['errors']) {
                        logger("===========Shopify collection Response==============");
                       // logger(json_encode($collectionReq['body']));


//====== SAVE :: collection in Database =================

                        $collection = $collectionReq['body']['custom_collection'];

                        $collectionRec = [
                            'user_id' => $shop->id,
                            'shopify_id' => $collection['id'],
                            'magento_collection_id' => $collectionInfo->entity_id,
                            'shopify_handle' => $collection['handle'],
                            'title' => $collection['title'],
                            'sort_order' => $collection['sort_order'],
                            'source' => "Magento",
                            'status' => 1,
                            'is_exist_in_shopify' => 1,
                            'updated_at' => date("Y-m-d H:i:s"),
                        ];

                        $collectionID = DB::table('collections')->insertGetId($collectionRec);


//====== SAVE :: collection in Database :: END=================

                    }
                } // END collection:: LOOP
            }// END collection:: IF

            return response()->json(['data' => 'success'], 200);

            logger("===============END :: JOB :: Add Collections ===============");

        }catch( \Exception $e ){
            logger("===============ERROR :: JOB :: Add Collections ===============");
            logger(json_encode($e->getMessage()));

        }
    }

}
