<?php

namespace App\Jobs;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;
use DB;

class AutoUpdateInventoryJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            logger('========== START:: AutoUpdate Inventory :: JOB =========');

            $mysql2 = mysql2Con();

            $productInfo = DB::table('products')->where("is_exist_in_shopify",1)->where("is_auto_update_inventory",1)->get();

           // logger("productInfo");
           // logger(json_encode($productInfo));

            if(count($productInfo)>0) {

                foreach ($productInfo as $key=>$product) {

                    $user_id = $product->user_id;
                    $product_id = $product->shopify_id;
                    $magento_product_id = $product->magento_product_id;

                    logger("user_id :: ".$user_id." product_id :: ".$product_id);

                    $shop = User::where('id', $user_id)->first();

                    if($shop){

                    $endPoint = '/admin/api/'. env('SHOPIFY_API_VERSION') .'/products/'. $product_id .'/variants.json';

                    $apiVariants = $shop->api()->rest('GET', $endPoint);   // shopify images result.

                    logger("==variants api res==");

                    logger(json_encode($apiVariants));

                    if (!$apiVariants['errors']) {

                            $apiVariantsData = $apiVariants['body']['variants'];

                    if(count($apiVariantsData)>0){

                        for ($v = 0; $v < count($apiVariantsData); $v++) {

                            $apiVariantsId = $apiVariantsData[$v]['id'];
                            $inventory_id = $apiVariantsData[$v]['inventory_item_id'];

                            logger("apiVariantsId :: ".$apiVariantsId);
                            logger("inventory_item_id :: ".$inventory_id);

                           if( $inventory_id != '' ){


                               $variant_db = DB::table('variants')->select('source_id')->where('product_id','=',$product->id)->where('shopify_id','=',$apiVariantsId)->first();


                                if($variant_db){

                                    $inventory_quantity = 0;

                                    $source_id = $variant_db->source_id;

                                    logger("source_id ".$source_id);

                                    $product_quantity  = $mysql2->table('cataloginventory_stock_item')->select('qty')->where('product_id','=',$source_id)->first();

                                    logger("product_quantity");
                                    logger(json_encode($product_quantity));

                                    if($product_quantity) {
                                        $inventory_quantity = (int) $product_quantity->qty;

                                        $this->getVariantInventoryLevelsAndSetQuantity($shop, $inventory_id,
                                            $inventory_quantity, $apiVariantsId);
                                    }

                                }



                            }else{
                                  logger('====================== Inventory id not found ======================');
                            }
                         }
                      }
                   }
                }
             }
           }

        }catch( \Exception $e ){
            logger('========== ERROR:: AutoUpdate Inventory JOB=========');
            logger($e);
        }
    }

    public function getVariantInventoryLevelsAndSetQuantity($shop, $inventory_id,$inventory_quantity,$apiVariantsId){
        try{
            logger('====================== START:: getVariantInventoryLevelsAndSet ======================');

                            $parameter['inventory_item_ids'] = $inventory_id;
                            $endPoint = '/admin/api/'.env('SHOPIFY_API_VERSION').'/inventory_levels.json';
                            $inventory_levels = $shop->api()->rest('GET',$endPoint,$parameter);

                            logger("===========inventory_levels========");
                            logger(json_encode($inventory_levels));

                               if (!$inventory_levels['errors']) {

                                    $inventoryLevels = $inventory_levels['body']['inventory_levels'];

                                    if( !empty($inventoryLevels) ){

                                         foreach ( $inventoryLevels as $ikey=>$ival ){

                                            $endPoint = '/admin/api/'. env('SHOPIFY_API_VERSION') .'/inventory_levels/set.json';

                                                     $set = [
                                                                'location_id' => $ival->location_id,
                                                                'inventory_item_id' => $ival->inventory_item_id,
                                                                'available' => $inventory_quantity,
                                                            ];

                                                     $inventory_quantity_set = $shop->api()->rest('POST', $endPoint, $set);

                                                     logger("==================================================");
                                                     logger("inventory_quantity_set :: SET");

                                                        if (!$inventory_quantity_set['errors']) {

                                                                  $db_variant = DB::table('variants')
                                                                    ->where('shopify_id', $apiVariantsId)
                                                                    ->update([
                                                                        'inventory_quantity' => $inventory_quantity
                                                                    ]);
                                                }
                                         }
                                    }
                             }

            logger('====================== END:: getVariantInventoryLevelsAndSet ======================');
            return [];
        }catch( \Exception $e ){
            logger('====================== ERROR:: getVariantInventoryLevelsAndSet ======================');
            logger($e);
        }
    }

}
