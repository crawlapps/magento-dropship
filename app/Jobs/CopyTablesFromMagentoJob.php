<?php

namespace App\Jobs;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use DB;
use App\Traits\MagentoExportInfoTrait;

class CopyTablesFromMagentoJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use MagentoExportInfoTrait;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            logger('========== START:: Auto Copy magento Tables :: JOB =========');

            $tables = [
                'core_config_data' => 'config_id',
                'eav_attribute' => 'attribute_id',
                'eav_attribute_option' => 'option_id',
                'eav_attribute_option_value' => 'value_id',
                'eav_entity_type' => 'entity_type_id',
                'catalog_product_entity' => 'entity_id',
                'catalog_product_super_link' => 'link_id',
                'catalog_product_index_price' => 'entity_id',
                'catalog_product_super_attribute' => 'product_super_attribute_id',
                'catalog_product_super_attribute_label' => 'value_id',
                'catalog_product_entity_int' => 'value_id',
                'catalog_product_entity_decimal' => 'value_id',
                'catalog_product_entity_varchar' => 'value_id',
                'catalog_product_entity_text' => 'value_id',
                'cataloginventory_stock_item' => 'item_id',
                'catalog_category_entity' => 'entity_id',
                'catalog_category_entity_varchar' => 'value_id',
                'catalog_category_product' => 'entity_id',
                'review' => 'review_id',
                'review_detail' => 'detail_id',
                'review_entity' => 'entity_id',
                'review_status' => 'status_id',
                'magiccart_shopbrand' => 'shopbrand_id',
                'store' => 'store_id'
            ];


             $this->insertData($tables);
             $this->ExportIntegrationAPIProducts();
             $this->AutoUpdateInventory();

        } catch (\Exception $e) {
            logger('========== ERROR::  Auto Copy magento Tables :: JOB=========');
            logger($e);
        }
    }



//==============================================================================================================
//==========START :: TABLE :: insertData
//==============================================================================================================


    public function insertData($tables)
    {
        try {

            ini_set('MAX_EXECUTION_TIME', '-1');
            set_time_limit(0);
            logger('====================== START:: COPY :: (Table) insertData :: JOB======================');

            $mysql2 = mysql2Con();


            foreach ($tables as $tablename => $tableprimaryid) {
                logger("TABLE :: ".$tablename);

                DB::table('magento_'.$tablename)->truncate();


                $queryReq = $mysql2->table($tablename);

                //      if($tablename=="catalog_product_entity"){

                // $IntegrationProduct = $this->getIntegrationProduct();
                // $entity_ids = array_column($IntegrationProduct, 'product_id');
                //      	$queryReq = $queryReq->whereIn('entity_id',$entity_ids);

                //       	$this->deleteNotFoundProducts($entity_ids);

                //      }

                //  $records = $mysql2->table($tablename)->get()->toArray();

                $records = $queryReq->get()->toArray();

                if (count($records) > 0) {
                    $recordkeys = array_values(array_keys((array) $records[0]));

                    $entity = [];
                    foreach ($records as $recordk => $record) {
                        foreach ($recordkeys as $k => $v) {
                            //  $kkey = ($v == $tableprimaryid) ? 'id' : $v;
                            $entity[$recordk][$v] = $record->$v;
                            $entity[$recordk]['id'] = $entity[$recordk][$tableprimaryid];
                        }
                    }
                    logger("Total Records ".json_encode(count($entity)));
                    logger('magento_'.$tablename);

                    foreach (array_chunk($entity, 1000) as $chunk) {

                        $status = DB::table('magento_'.$tablename)->upsert($chunk, 'id');
                    }

                }

                // sleep(1);
            }

            logger('====================== END:: COPY :: (Table) insertData :: JOB ======================');
        } catch (\Exception $e) {
            logger('====================== ERROR:: COPY :: (Table) insertData :: JOB ======================');
            logger($e);
        }
    }
//==============================================================================================================
//==========END :: TABLE :: insertData
//==============================================================================================================


//==============================================================================================================
//==========START :: AutoUpdateInventoryJob
//==============================================================================================================


    public function AutoUpdateInventory()
    {
        try {
            logger('========== START:: AutoUpdate Inventory :: JOB =========');

           // $mysql2 = mysql2Con();

           // $IntegrationInventory = $this->getIntegrationInventory();

           // $entity_ids = array_column($IntegrationInventory, 'product_id');

            $membershipProducts = $this->getIntegrationProductFromLocal();

//            $productInfo = DB::table('products')->where("is_exist_in_shopify",1)->whereIn('magento_product_id',$entity_ids)->where("is_auto_update_inventory",1)->get();

            $productInfo = DB::table('products')->where("is_exist_in_shopify", 1)->get();

            logger("productInfo");
            logger(json_encode($productInfo));

            if (count($productInfo) > 0) {

                foreach ($productInfo as $key => $product) {

                    $user_id = $product->user_id;
                    $product_id = $product->shopify_id;
                    $magento_product_id = $product->magento_product_id;

                    logger("user_id :: ".$user_id." product_id :: ".$product_id);

                    $shop = User::where('id', $user_id)->first();

                    if ($shop) {

                        $endPoint = '/admin/api/'.env('SHOPIFY_API_VERSION').'/products/'.$product_id.'/variants.json';

                        $apiVariants = $shop->api()->rest('GET', $endPoint);   // shopify images result.

                        logger("==variants api res==");

                        logger(json_encode($apiVariants));

                        if (!$apiVariants['errors']) {

                            $apiVariantsData = $apiVariants['body']['variants'];

                            if (count($apiVariantsData) > 0) {

                                for ($v = 0; $v < count($apiVariantsData); $v++) {

                                    $apiVariantsId = $apiVariantsData[$v]['id'];
                                    $inventory_id = $apiVariantsData[$v]['inventory_item_id'];

                                    logger("apiVariantsId :: ".$apiVariantsId);
                                    logger("inventory_item_id :: ".$inventory_id);

                                    if ($inventory_id != '') {

                                        $variant_db = DB::table('variants')->select('source_id')->where('product_id',
                                            '=', $product->id)->where('shopify_id', '=', $apiVariantsId)->first();


                                        if ($variant_db) {

                                            $inventory_quantity = 0;

                                            $source_id = $variant_db->source_id;

                                            logger("source_id ".$source_id);

                                           // $product_quantity = $mysql2->table('cataloginventory_stock_item')->select('qty')->where('product_id',
                                              //  '=', $source_id)->first();


                                            if (in_array($source_id, array_column($membershipProducts, 'product_id'))) {

                                                $found_key = array_search($source_id,
                                                    array_column($membershipProducts, 'product_id'));

                                                logger("Found Key :: ".$found_key);

                                                $availableProduct = $membershipProducts[$found_key];


                                                logger("product_quantity");
                                                logger($availableProduct['qty']);


                                                if (@$availableProduct['qty']) {
                                                    $inventory_quantity = (int) $availableProduct['qty'];

                                                    $this->getVariantInventoryLevelsAndSetQuantity($shop, $inventory_id,
                                                        $inventory_quantity, $apiVariantsId);
                                                }
                                            }

                                        }


                                    } else {
                                        logger('====================== Inventory id not found ======================');
                                    }
                                }
                            }
                        }
                    }
                }
            }

        } catch (\Exception $e) {
            logger('========== ERROR:: AutoUpdate Inventory JOB=========');
            logger($e);
        }
    }
//==============================================================================================================
//==========END ::AutoUpdateInventoryJob
//==============================================================================================================


    public function getVariantInventoryLevelsAndSetQuantity($shop, $inventory_id, $inventory_quantity, $apiVariantsId)
    {
        try {
            logger('====================== START:: getVariantInventoryLevelsAndSet ======================');

            $parameter['inventory_item_ids'] = $inventory_id;
            $endPoint = '/admin/api/'.env('SHOPIFY_API_VERSION').'/inventory_levels.json';
            $inventory_levels = $shop->api()->rest('GET', $endPoint, $parameter);

            logger("===========inventory_levels========");
            logger(json_encode($inventory_levels));

            if (!$inventory_levels['errors']) {

                $inventoryLevels = $inventory_levels['body']['inventory_levels'];

                if (!empty($inventoryLevels)) {

                    foreach ($inventoryLevels as $ikey => $ival) {

                        $endPoint = '/admin/api/'.env('SHOPIFY_API_VERSION').'/inventory_levels/set.json';

                        $set = [
                            'location_id' => $ival->location_id,
                            'inventory_item_id' => $ival->inventory_item_id,
                            'available' => $inventory_quantity,
                        ];

                        $inventory_quantity_set = $shop->api()->rest('POST', $endPoint, $set);

                        logger("==================================================");
                        logger("inventory_quantity_set :: SET");

                        if (!$inventory_quantity_set['errors']) {

                            $db_variant = DB::table('variants')
                                ->where('shopify_id', $apiVariantsId)
                                ->update([
                                    'inventory_quantity' => $inventory_quantity
                                ]);
                        }
                    }
                }
            }

            logger('====================== END:: getVariantInventoryLevelsAndSet ======================');
            return [];
        } catch (\Exception $e) {
            logger('====================== ERROR:: getVariantInventoryLevelsAndSet ======================');
            logger($e);
        }
    }


    public function deleteNotFoundProducts($entity_ids)
    {

        logger("================Delete product :: Not Found product from API =======================");

        try {

            $products_exist = DB::table('magento_catalog_product_entity')->select('entity_id')->whereNotIn('entity_id',
                $entity_ids)->pluck('entity_id')->toArray();

            logger("Product to be delete");

            if (count($products_exist) > 0) {

                for ($p = 0; $p < count($products_exist); $p++) {

                    //logger("Delete :: ".$products_exist[$p]);

                    $products = DB::table('products')->where('magento_product_id', $products_exist[$p])->get();


                    logger(json_encode($products));

                    if (count($products) > 0) {
                        for ($i = 0; $i < count($products); $i++) {

                            $productRec = $products[$i];
                            $productID = $products[$i]->id;

                            $user_id = $productRec->user_id;

                            //  logger("DELETE :: Product :: ".$productID);

                            $variantRecs = DB::table('variants')->where('product_id', $productID)->get();

                            // Delete attributes
                            foreach ($variantRecs as $vRec) {
                                DB::table('attributes')->where('variant_id', $vRec->id)->delete();
                            }

                            // Delete variants
                            DB::table('variants')->where('product_id', $productID)->delete();

                            // Delete images
                            DB::table('images')->where('product_id', $productRec->shopify_id)->delete();

                            // Delete tags
                            DB::table('tags')->where('product_id', $productID)->delete();

                            // Delete product rec
                            DB::table('products')->where('id', $productID)->delete();

                            // Delete product from Shopify
                            // TODO: Verify deletion, throw exception if unsuccessful
                            if ($productRec->shopify_id) {

                                $shop = User::where('id', $user_id)->first();

                                if ($shop) {

                                    $productReq = $shop->api()->rest('DELETE',
                                        '/admin/api/'.env('SHOPIFY_API_VERSION').'/products/'.$productRec->shopify_id.'.json');
                                }
                            }
                        }
                    }

                    DB::table('magento_catalog_product_entity')->where('entity_id', $products_exist[$p])->delete();

                }

            }

            return true;

        } catch (\Exception $e) {
            // DB::rollBack();
            $errors = $e->getMessage();


            logger("======ERROR :: ==========Delete product :: Not Found product from API =======================");
            logger(json_encode($errors));

            return true;
        }

        //DB::commit();

        return true;
    }


    public function ExportIntegrationAPIProducts()
    {
        try {
            logger("====START :: CRON :: Export Integration Product =====");


            $integrationProduct = $this->getIntegrationProduct();

            if($integrationProduct) {

                DB::table('magento_integration_api_products')->truncate();

                $membershipProducts = (array) $integrationProduct;

                if (count($membershipProducts) > 0) {
                    foreach (array_chunk($membershipProducts, 100) as $chunk) {

                        foreach ($chunk as $item) {

                            $products_response = json_encode($item);

                            if (@$item['product_id']) {

                                DB::table('magento_integration_api_products')->updateOrInsert(
                                    ['product_id' => $item['product_id'], 'sku' => $item['sku']],
                                    [
                                        'product_id' => $item['product_id'],
                                        'sku' => $item['sku'],
                                        'products_response' => $products_response,
                                        'created_at' => date("Y-m-d H:i:s"),
                                        'updated_at' => date("Y-m-d H:i:s")
                                    ]
                                );
                            }
                        }
                    }
                }
            }

            logger("====END :: CRON :: Export Integration Product =====");

            return true;

        } catch (\Exception $e) {

            logger("====ERROR :: CRON :: Export Integration Product =====");
            logger(json_encode($e));
        }
    }


}
