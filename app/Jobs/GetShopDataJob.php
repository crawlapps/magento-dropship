<?php

namespace App\Jobs;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GetShopDataJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    private $shop_id = '';

    public function __construct($shop_id)
    {
        $this->shop_id = $shop_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
        logger("=========================START::GetShopDataJob===========================");


            $shop = User::where('id', $this->shop_id)->first();
            logger("==shop user==");
            logger(json_encode($shop));

            $fields = "id, email, name";
            $parameter['fields'] = $fields;

              $shop_result = $shop->api()->rest('GET', '/admin/api/'.env('SHOPIFY_API_VERSION').'/shop.json', $parameter);

            logger("==shop api res==");
            if (!$shop_result['errors']) {

                $data =  $shop_result['body']['shop'];
                logger(json_encode($data));

            }

        logger("=========================END::GetShopDataJob===========================");

        }catch(\Exception $e){
            logger("===================ERROR::GetShopDataJob=================");
            logger($e);
        }
    }
}
