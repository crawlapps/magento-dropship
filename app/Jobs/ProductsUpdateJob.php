<?php namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Osiset\ShopifyApp\Objects\Values\ShopDomain;
use stdClass;
use App\Models\User;
use DB;
class ProductsUpdateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Shop's myshopify domain
     *
     * @var ShopDomain|string
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;

    /**
     * Create a new job instance.
     *
     * @param string   $shopDomain The shop's myshopify domain.
     * @param stdClass $data       The webhook data (JSON decoded).
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        try {
            logger("===============START :: Product Update Job ===============");
            // Convert domain
            //$this->shopDomain = ShopDomain::fromNative($this->shopDomain);

            // Do what you wish with the data
            // Access domain name as $this->shopDomain->toNative()

            $user = User::where('name', $this->shopDomain)->first();

            $data = $this->data;

            logger("Data");
            logger(json_encode($data));

            $sh_productID = $data->id;

            logger("sh_productID");
            logger($sh_productID);

            logger("title");
            logger($data->title);

            $db_product = DB::table('products')->where('shopify_id', $sh_productID)->where('user_id', $user->id)->first();
            $sh_variants = $data->variants;

            if( $db_product ){

                if( count($sh_variants) > 1 ){
                    foreach ( $sh_variants as $svkey=>$svval ){

                        $db_variant = DB::table('variants')
                            ->where('product_id', $db_product->id)
                            ->where('shopify_id', $svval->id)
                            ->update([
                                      'shopify_price' => $svval->price,
                                      'inventory_quantity' => $svval->inventory_quantity,
                                      'title' => $data->title
                                     ]);
                    }
                }
            }


            logger("===============END :: Product Update Job ===============");
        }catch(\Exceptation $e){
            logger("===============ERROR :: Product Update Job ===============");
            logger(json_encode($e));

        }
    }
}
