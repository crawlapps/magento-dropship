<?php namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Osiset\ShopifyApp\Objects\Values\ShopDomain;
use stdClass;
use App\Models\User;
use DB;
class CollectionsDeleteJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Shop's myshopify domain
     *
     * @var ShopDomain|string
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;

    /**
     * Create a new job instance.
     *
     * @param string   $shopDomain The shop's myshopify domain.
     * @param stdClass $data       The webhook data (JSON decoded).
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Convert domain
       // $this->shopDomain = ShopDomain::fromNative($this->shopDomain);

        // Do what you wish with the data
        // Access domain name as $this->shopDomain->toNative

        try {
            logger("===============START :: Collections Delete Job ===============");

            logger("shopDomain");
            logger($this->shopDomain);

            // Convert domain
            //$this->shopDomain = ShopDomain::fromNative($this->shopDomain);

            $shop = User::where('name', $this->shopDomain)->first();

            $shopify_id = $this->data->id;

            logger("Delete :: Collections ID :: ".$shopify_id);

            DB::table('collections')->where('shopify_id', $shopify_id)->delete();

            $products = DB::table('products')->where('user_id', $shop->id)->whereJsonContains('collection_id', $shopify_id)->get();

            logger("producs");
            logger(json_encode($products));

            if(count($products)>0){

                foreach($products as $key=>$product) {
                    $sh_product_id = $product->shopify_id;

                    logger("sh_product_id ".$sh_product_id);

                    $collection_ids = $product->collection_id;

                    $row = json_decode($collection_ids, true);

                    logger("====before collection row====");
                    logger(json_encode($row));

                    if (is_array($row)) {

                        if (($key = array_search($shopify_id, $row)) !== false) {
                            unset($row[$key]);

                            logger("====after collection row====");
                            logger(json_encode($row));

                            DB::table('products')->where('shopify_id', $sh_product_id)->where('user_id', $shop->id)
                                ->update(['collection_id' => $row, 'updated_at' => date('Y-m-d H:i:s')]);
                        }

                  }

                }
            }

            logger("===============END :: Collections Delete Job ===============");

        }catch(\Exception $e ){
            logger('ERROR :: Delete Collections Webhook');
            logger(json_encode($e));
        }


    }
}
