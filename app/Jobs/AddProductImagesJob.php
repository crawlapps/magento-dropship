<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\User;
use App\Models\Images;
class AddProductImagesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $shop_id = '';
    private $db_line_items_id = '';
    private $product_id = '';

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($shop_id, $product_id, $db_line_items_id)
    {
        $this->shop_id = $shop_id;
        $this->product_id = $product_id;
        $this->db_line_items_id = $db_line_items_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            \Log::info("===================START:: AddProductImagesJob=================");

            $shop = User::where('id', $this->shop_id)->first();

            logger("==shop==");
            logger($shop);

            if ($shop) {

                $endPoint = '/admin/api/'. env('SHOPIFY_API_VERSION') .'/products/'. $this->product_id .'/images.json';

                $result = $shop->api()->rest('GET', $endPoint);   // shopify images result.

                logger("==images api res==");

                logger(json_encode($result));

                if ($result) {

                    $images =  $result['body']['images'];


                    logger("=============images===============");
                    logger(json_encode($images));

                    if($images) {
                        $image = $images[0];

                        logger("=============images::0===============");
                        logger(json_encode($image));

                        if($image) {

                            Images::firstOrNew(
                                [
                                    "product_id" => $this->product_id,
                                    "url" => $image['src'],
                                ]
                            );
                        }

                        logger("-----------------------------------");
                        logger("Product Image Data is Saved!");
                        logger("-----------------------------------");

                    }


                } else {
                    logger("error during call Get Product images API !");
                }



            } else {
                logger("-----------------------------------");
                logger("Shop Not Found!");
                logger("-----------------------------------");
            }


            //  logger("===================END:: AddProductImagesJob=================");

//            \Log::info("===================END:: AddProductImagesJob=================");
        }catch(\Exception $e){
            \Log::info("===================ERROR:: AddProductImagesJob=================");
            \Log::info($e);
        }
    }
}
