<?php

namespace App\Jobs;

use App\Models\MagentoUsersPlans;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Traits\MagentoExportInfoTrait;


class DeleteProducsPreviousPlanAPIJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    private $shop_id = '';
    private $membership = '';


    public function __construct($shop_id,$membership)
    {
        $this->shop_id = $shop_id;
        $this->membership = $membership;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            logger('=============== START:: delete:membership-products :: JOB =============');

            $membership = $this->membership;

            logger("membership. =>".$membership);
            logger("shop_id. =>".$this->shop_id);

            $shop = User::where('id', $this->shop_id)->first();

            $productRecsQuery =  DB::table('products')->where('user_id',$this->shop_id)->where("is_exist_in_shopify",1);

            $productInfo = $productRecsQuery->get();

            if(count($productInfo)>0) {

                foreach ($productInfo as $key => $product) {

                    $membership_for_free_store = $product->membership_for_free_store;
                    $membership_for_limited_store = $product->membership_for_limited_store;
                    $membership_for_enterprize_store = $product->membership_for_enterprize_store;

                    $condition=false;

                    if ($membership == "free") {
                        $condition  =  $membership_for_free_store !== 1;
                    }

                    if ($membership == "limited") {
                        $condition  =  $membership_for_limited_store !== 1;
                    }

                    if ($membership == "enterprize") {
                        $condition  =  $membership_for_enterprize_store !==1;
                    }


                    if($condition){

                        $product_sh_id = $product->shopify_id;

                        $productID = $product->id;

                        logger("DELETE :: Product using membership :: ".$productID);

                        $variantRecs = DB::table('variants')->where('product_id', $productID)->get();

                        // Delete attributes
                        foreach ($variantRecs as $vRec) {
                            DB::table('attributes')->where('variant_id', $vRec->id)->delete();
                        }

                        // Delete variants
                        DB::table('variants')->where('product_id', $productID)->delete();

                        // Delete images
                        DB::table('images')->where('product_id', $product_sh_id)->delete();

                        // Delete tags
                        DB::table('tags')->where('product_id', $productID)->delete();

                        // Delete product rec
                        DB::table('products')->where('id', $productID)->delete();

                        // Delete product from Shopify
                        // TODO: Verify deletion, throw exception if unsuccessful
                        if ($product_sh_id) {
                            $productReq = $shop->api()->rest('DELETE',
                                '/admin/api/'.env('SHOPIFY_API_VERSION').'/products/'.$product_sh_id.'.json');
                        }

                    }

                }
            }

            logger('=============== END:: delete:membership-products :: JOB=============');
        }catch( \Exception $e ){
            logger('=============== ERROR:: delete:membership-products :: JOB =============');
            logger($e);
        }
    }
}
