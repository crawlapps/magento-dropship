<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use DB;
use App\Models\User;
class DeleteCollectionsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $shop_id = '';
    private $data = [];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($shop_id,$data)
    {
        $this->shop_id = $shop_id;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        logger("===============START :: JOB :: Delete Collections ===============");
        $shop = User::where('id', $this->shop_id)->first();

        logger("shop");
        logger(json_encode($shop));

        logger(json_encode($this->data));

        try{
            $selectedCollections =  (array) $this->data;

            $collections = $selectedCollections['collection_ids'];
            $is_collection_delete_with_products = $selectedCollections['is_collection_delete_with_products'];

            logger("collection_ids  => ".json_encode($collections));
            logger("is_collection_delete_with_products => ".$is_collection_delete_with_products);

            if(isset($collections) && count($collections)>0) {

                logger("==========Selected collections are ============");

                for ($i = 0; $i < count($collections); $i++) {

                    logger("===============delete a collections::Shopify== ".$i);

                    $collection_shopify_id = $collections[$i];

                    logger($collection_shopify_id);

                    $this->deleteCollection($shop,$collection_shopify_id);

                    if($is_collection_delete_with_products=="true"){

                        logger("=========START :: collection delete with product=======");

                        $products = DB::table('products')->where('user_id', $shop->id)->whereJsonContains('collection_id', $collection_shopify_id)->get();

                         logger("producs");
                         logger(json_encode($products));

                         if(count($products)>0){

                             foreach($products as $key=>$product){
                                 $sh_product_id = $product->shopify_id;
                                 $this->deleteProduct($shop,$sh_product_id);
                             }
                         }

                        logger("=========END :: collection delete with product=======");

                    }else{
                        logger("only delete collection");
                    }

                } // END collection:: LOOP
            }// END collection:: IF

            return response()->json(['data' => 'success'], 200);

            logger("===============END :: JOB :: Delete Collections ===============");

        }catch( \Exception $e ){
            logger("===============ERROR :: JOB :: Delete Collections ===============");
            logger(json_encode($e->getMessage()));

        }
    }

     public function deleteCollection($shop,$custom_collection_id){
         logger('================= START ::  Delete custom_collections =================');
         try {

             $collectReq = $shop->api()->rest('DELETE', '/admin/api/'. env('SHOPIFY_API_VERSION') .'/custom_collections/' . $custom_collection_id . '.json');

         logger("===============delete :: collectReq=================");
         logger(json_encode($collectReq));

            DB::table('collections')->where('shopify_id', $custom_collection_id)->delete();


         return true;

         }catch( \Exception $e ){
             logger('================= ERROR::  Delete custom_collections =================');
             logger($e->getMessage());
             return true;
         }

     }

    public function deleteProduct($shop,$sh_product_id){
        logger('================= START ::  Delete Shopify Product =================');
        try {

            $productReq = $shop->api()->rest('DELETE', '/admin/api/'. env('SHOPIFY_API_VERSION') .'/products/' .$sh_product_id. '.json');

            logger("===============delete :: productReq=================");
            logger(json_encode($productReq));

            return true;

        }catch( \Exception $e ){
            logger('================= ERROR::  Delete Shopify Product =================');
            logger($e->getMessage());
            return true;
        }

    }

}
