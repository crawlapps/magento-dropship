<?php

namespace App\Jobs;

use App\Models\Images;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use DB as DBS;
use Symfony\Component\Intl\Currencies;
use App\Traits\MagentoExportInfoTrait;

class AddProductJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use MagentoExportInfoTrait; //Traits

    private $shop_id = '';
    private $data = [];
    private $auto_update_data = [];


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($shop_id,$data)
    {
        $this->shop_id = $shop_id;
        $this->data = $data['product_info'];
        $this->auto_update_data = $data['auto_update'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        logger("===============START :: JOB :: Add Product ===============");

        $shop = User::where('id', $this->shop_id)->first();

        logger("product_info data=================================================================================");

        logger(json_encode($this->data));

        logger("auto_update data===================================================================================");

        logger(json_encode($this->auto_update_data));

        try{

            $selectedProducts =  (array) $this->data;
            $selectedProductsInventory =  (array) $this->auto_update_data;

            $membershipProducts = $this->getIntegrationProductFromLocal();

            if(count($membershipProducts) > 0) {

                $APIProducts = array_column($membershipProducts,'product_id');

                $products = DBS::table('magento_catalog_product_entity as product')
                    ->select('product.entity_id', 'product.type_id')
                    ->whereIn('product.entity_id', $selectedProducts)
                    // ->where('product.type_id','=','configurable')
                    ->whereIn('type_id', ['configurable', 'simple'])
                    ->whereIn('product.entity_id', $APIProducts)
                    ->get();

                $shope_data = getShopData($shop->id); //Helper

                $magentoConfigData = $this->getMagentoConfigData();

                $from_currency = (isset($magentoConfigData['magento_currency'])) ? $magentoConfigData['magento_currency'] : "USD";

                $from_currency_symbol = Currencies::getSymbol($from_currency);

                $magento_site_url = (isset($magentoConfigData['magento_site_url'])) ? $magentoConfigData['magento_site_url'] : "https://hairbawse.com/";

                if (isset($products) && count($products) > 0) {


                     logger("==========Selected Products are ============");

                    for ($i = 0; $i < count($products); $i++) {

                        $productID = $products[$i]->entity_id;

                        logger("productID");
                        logger($productID);


                        $membership = null;
                        $membership = $this->getMembership($shop->id);
                        $membership_for_free_store = 0;
                        $membership_for_limited_store = 0;
                        $membership_for_enterprize_store = 0;

                        logger("membership");
                        logger($membership);
                        logger("==============================");

                       // $MagentoStoreID = getMagentoStoreID($membership);

                        if ($membership) {
                            if (in_array($productID, array_column($membershipProducts, 'product_id'))) {

                                $found_key = array_search($productID, array_column($membershipProducts, 'product_id'));

                                logger("Found Key :: ".$found_key);

                                $availableProduct = $membershipProducts[$found_key];

                                if ($availableProduct) {

                                    logger("membership => ".$membership);
                                    logger("availableProduct");
                                    logger($availableProduct);

                                    if ($membership == "free") {
                                        $available_membership = $availableProduct['available_for_free_store'];
                                        if ($available_membership == "1") {
                                            $membership_for_free_store = 1;
                                        }

                                    } else {
                                        if ($membership == "limited") {
                                            $available_membership = $availableProduct['available_for_limited_store'];
                                            if ($available_membership == "1") {
                                                $membership_for_limited_store = 1;
                                            }

                                        } else {
                                            if ($membership == "enterprize") {
                                                $available_membership = $availableProduct['available_for_enterprize_store'];
                                                if ($available_membership == "1") {
                                                    $membership_for_enterprize_store = 1;
                                                }

                                            }
                                        }
                                    }

                                }
                            }
                        }


                        $name = $availableProduct['name'] ? $availableProduct['name'] : '';

                        logger("===================membership_for_limited_store===================");
                        logger($membership_for_limited_store);

                        $productInfo = [];
                        $product_variants = [];

                        if ($products[$i]->type_id == "simple") {
                            logger("simple product adding...");
                            $productInfo = $this->GetMagentoSimpleProduct($productID, []);
                            $productInfo['variants'] = [];
                        }
                        if ($products[$i]->type_id == "configurable") {
                            logger("with variants product adding...");
                            $productInfo = $this->getMagentoConfigurableProduct($productID);
                            $product_variants = $this->GetMagentoProductVariant($productID);
                            $productInfo['variants'] = $product_variants;
                        }

                        $shopifyVariants = [];

                        // Initialize Shopify product
                        $shopifyProduct = [
                            'title' => $name,
                            'body_html' => $productInfo['description'],
                            'metafields' => [
                                [
                                    'key' => 'magentodropship',
                                    'value' => 'bymagento',
                                    'value_type' => 'string',
                                    'namespace' => 'dropshipper'
                                ]
                            ]
                        ];

                        $shopifyImages = [];
                        $variantMainImageURLs = [''];
                        $AllImages = [];

                        if ($productInfo['small_image']) {

                            $shopifyImages[] = ['src' => $productInfo['small_image'], 'position' => 1];
                            $variantMainImageURLs[0] = $productInfo['small_image'];

                        } else {
                            if ($productInfo['thumbnail']) {

                                $shopifyImages[] = ['src' => $productInfo['thumbnail'], 'position' => 1];
                                $variantMainImageURLs[0] = $productInfo['thumbnail'];

                            } else {
                                if ($productInfo['image']) {
                                    $shopifyImages[] = ['src' => $productInfo['image'], 'position' => 1];
                                    $variantMainImageURLs[0] = $productInfo['image'];
                                }
                            }
                        }


                        $productInfo_brand = $availableProduct['brand'] ? $availableProduct['brand'] : '';

                        if ($productInfo_brand) {
                            $shopifyProduct['product_type'] = $productInfo_brand;
                            $shopifyProduct['tags'] = $productInfo_brand;
                        }

                        if ($productInfo['meta_title']) {
                            $shopifyProduct['metafields_global_title_tag'] = $productInfo['meta_title'] ? $productInfo['meta_title'] : "";
                        }
                        if ($productInfo['meta_description']) {
                            $shopifyProduct['metafields_global_description_tag'] = $productInfo['meta_description'] ? $productInfo['meta_description'] : "";
                        }

                        $price = 0.00;
                        $to_currency = $shope_data->currency;

                        logger("from_currency => ".$from_currency);
                        logger("to_currency => ".$to_currency);


//                        if ($to_currency !== $from_currency) {
//                            $latestFromRate = LatestExchangeRates('USD', $from_currency);
//                            $latestToRate = LatestExchangeRates('USD', $to_currency);
//                        }

                        $currentMagentPrice = $productInfo['price'];
                        $price = floatval($currentMagentPrice);

                        $price = round($price, 2);

                        $source_price = floatval($productInfo['price']);

                        $shopify_price = floatval($price);
                        $inventory_quantity = 0;
                        $in_stock = 0;


                        $inventory_quantity = (int) (@$availableProduct['qty']) ? $availableProduct['qty'] : 0;

                        if ($productInfo['in_stock']) {
                            $in_stock = $productInfo['in_stock'];
                        }

                        // Add main variant

                        $newVariation = [];
                        $shopifyOptions = [];

                        // Add other variants
                        if ($productInfo['variants']) {

                            if (!empty($productInfo['variants'][0])) {
                                $cus_options = $productInfo['variants'][0]['attributes'];
                                foreach ($cus_options as $ckey => $cval) {
                                    $shopifyOptions[] = ['name' => $cval->attribute];
                                }
                                $shopifyProduct['options'] = $shopifyOptions;
                            }

                            for ($v = 0; $v < count($productInfo['variants']); $v++) {

                                $variants = $productInfo['variants'][$v];

                                logger("========variants=======");
                                logger($variants);

                                $currentMagentPrice = $variants['price'];
                                $price = floatval($currentMagentPrice);
                                $price = round($price, 2);
                                $shopify_price = floatval($price);

                                $shopifyVariant = [
                                    'title' => $variants['name'] ? $variants['name'] : $name,
                                    'sku' => $variants['sku'],
                                    'price' => $shopify_price,
                                    'inventory_quantity' => (int) $variants['inventory_quantity'],
                                    'inventory_management' => 'shopify',
                                    'inventory_policy' => 'deny'
                                ];

                                $attsArray = [];
                                if ($variants['attributes']) {

                                    $variants_attributes = $variants['attributes'];
                                    $shopifyOptNum = 0;

                                    foreach ($variants_attributes as $attr) {
                                        $attsArray[] = ['dimension' => $attr->attribute, 'value' => $attr->value];
                                        $shopifyVariant['option'.++$shopifyOptNum] = $attr->value;

                                    }
                                }

                                $shopifyVariants[] = $shopifyVariant;
                                $variantAttsRecs[] = $attsArray;

                                $variantMainImage = "";
                                if ($productInfo['image']) {

                                    $variantMainImage = $productInfo['image'];

                                } else {
                                    if ($productInfo['thumbnail']) {

                                        $variantMainImage = $productInfo['thumbnail'];

                                    } else {
                                        if ($productInfo['small_image']) {

                                            $variantMainImage = $productInfo['small_image'];
                                        }
                                    }
                                }
                                $variantMainImageURLs[] = $variantMainImage;
                                $AllImages[] = $variantMainImage;
                            }
                        }

                        $option = [];
                        $newVariation = [];
                        foreach ($shopifyVariants as $key => $val) {
                            $opt = (@$val['option1']) ? $val['option1'] : '';
                            $opt = (@$val['option2']) ? $opt.$val['option2'] : $opt;
                            $opt = (@$val['option3']) ? $opt.$val['option3'] : $opt;

                            if ($opt != '' && !in_array($opt, $option)) {
                                $newVariation[] = $val;
                            }
                            $option[] = $opt;
                        }

                        $unique_images_array = array_unique($AllImages);

                        for ($im = 0; $im < count($unique_images_array); $im++) {

                            $shopifyImages[] = ['src' => $unique_images_array[$im]];
                        }

                        $shopifyProduct['images'] = $shopifyImages;

                        $mainShopifyVariant = [
                            [
//                    'title' => $productInfo['name'],
                                'sku' => $productInfo['sku'],
                                'weight' => $productInfo['weight'],
                                'price' => $shopify_price,
                                'inventory_management' => 'shopify',
                                'position' => 1,
                                'inventory_policy' => 'deny',
                                "option1" => '',
                                'inventory_quantity' => $inventory_quantity
                            ]
                        ];


                        logger("==============variants-new===============");
                        logger(json_encode($newVariation));

                        if (count($newVariation) > 0) {
                            $shopifyProduct['variants'] = $newVariation;
                        } else {
                            $shopifyProduct['variants'] = $mainShopifyVariant;
                        }


                        logger("--------------------------------------");

                        logger(json_encode($shopifyProduct));

                        logger("--------------------------------------");


                        $productReq = $shop->api()->rest('POST',
                            '/admin/api/'.env('SHOPIFY_API_VERSION').'/products.json', ['product' => $shopifyProduct]);

                        logger("productReq");
                        logger(json_encode($productReq));

                        $get_asset_call_limit = collect($productReq['response']->getHeader('x-shopify-shop-api-call-limit'))->first();

                        if ($get_asset_call_limit == '39/40') {

                            logger("Delay the script execution for 1 seconds ");
                            logger("Get assets, Waiting 1 seconds for a credit");
                            sleep(1);

                        } // Delay the script execution for 1 seconds }


                        if ($productReq['errors']) {
                            throw new \Exception('Shopify API error: '.json_encode((array) $productReq['body']));
                        }

                        if (!$productReq['body']['product']['id']) {
                            throw new \Exception('Could not retreieve product id from Shopify API');
                        }

                        if (!$productReq['errors']) {
                            logger("===========Shopify product Response==============");
                            //logger(json_encode($productReq['body']['product']));

                            if (empty($shopifyProduct['variants'])) {
                                $variant = [
                                    'variant' => [
                                        'id' => $productReq['body']['product']['variants'][0]['id'],
                                        'price' => $shopify_price,
                                    ]
                                ];

                                $endPoint = '/admin/api/'.env('SHOPIFY_API_VERSION').'/variants/'.$productReq['body']['product']['variants'][0]['id'].'.json';

                                $variantReq = $shop->api()->rest('PUT', $endPoint, $variant);

                                logger("empty :: variantReq");
                                logger(json_encode($variantReq));
                            }


//====== SAVE :: Product in Database =================

                            $product = $productReq['body']['product'];

                            $shopifyProductID = $product->id;
                            $shopifyProductHandle = $product->handle;

                            $source_url = $productInfo['url'];

                            $is_auto_update_inventory = 0;

                            if (count($selectedProductsInventory) > 0) {
                                if (in_array($productID, $selectedProductsInventory)) {
                                    $is_auto_update_inventory = 1;
                                }
                            }


                            $membership = null;
                            $membership = $this->getMembership($shop->id);


                            $productRec = [
                                'user_id' => $shop->id,
                                'shopify_id' => $shopifyProductID,
                                'shopify_handle' => $shopifyProductHandle,
                                'magento_product_id' => $productID,
                                'collection_id' => json_encode([]),
                                'type' => $product->product_type,
                                'locale' => $shope_data->country_code,
                                'source' => "Magento",
                                'source_url' => $source_url,
                                'source_currency' => $from_currency,
                                'source_currency_symbol' => $from_currency_symbol,
                                'description' => $productInfo['description'],
                                'saller_ranks' => json_encode([]),
                                'reviews' => json_encode([]),
                                'history' => json_encode([]),
                                'vendor' => $product->vendor,
                                'status' => 1,
                                'is_auto_update_inventory' => $is_auto_update_inventory,
                                'is_exist_in_shopify' => 1,
                                'membership_for_free_store' => $membership_for_free_store,
                                'membership_for_limited_store' => $membership_for_limited_store,
                                'membership_for_enterprize_store' => $membership_for_enterprize_store,
                                'created_at' => date("Y-m-d H:i:s"),
                                'updated_at' => date("Y-m-d H:i:s"),
                            ];

                            $productID_db = DBS::table('products')->insertGetId($productRec);

                            $magento_product_id = $productID;
                            $this->importReviews($productID_db, $magento_product_id);

                            //=======================================================================================================
                            // START :: Assign product to collection in Shopify
                            //=======================================================================================================

                            $this->ManageProductCollection($productID, $shopifyProductID);

                            //=======================================================================================================
                            // END :: Assign product to collection in Shopify
                            //=======================================================================================================


                            //=======================================================================================================
                            // Assign images to product variants in Shopify
                            //=======================================================================================================

                            if ($product->images && $product->variants) {

                                $apiImages = $product->images;

                                $apiVariants = $product->variants;

                                for ($v1 = 0; $v1 < count($apiVariants); $v1++) {


                                    $mainProductImageID = null;
                                    $foundImageIndex = 0;
                                    for ($ij = 0; $ij < count($shopifyImages); $ij++) {

                                        if ($variantMainImageURLs[$v1] == $shopifyImages[$ij]['src']) {
                                            $foundImageIndex = $ij;
                                            break;
                                        }
                                    }

                                    if (isset($apiImages[$foundImageIndex]) && $foundImageIndex !== 0) {

                                        $modVariant = [
                                            'id' => $apiVariants[$v1]->id,
                                            'image_id' => $apiImages[$foundImageIndex]->id
                                        ];

                                        $variantReq = $shop->api()->rest('PUT',
                                            '/admin/api/'.env('SHOPIFY_API_VERSION').'/variants/'.$apiVariants[$v1]->id.'.json',
                                            ['variant' => $modVariant]);


                                    }

                                    $imageRec = [
                                        'product_id' => $shopifyProductID,
                                        'url' => $variantMainImageURLs[$v1],
                                        'created_at' => date("Y-m-d H:i:s"),
                                        'updated_at' => date("Y-m-d H:i:s")
                                    ];

                                    $mainProductImageID = DBS::table('images')->insertGetId($imageRec);


                                    $source_id = $productID;


                                    if (count($productInfo['variants']) > 0) {
                                        for ($v2 = 0; $v2 < count($productInfo['variants']); $v2++) {
                                            $variants = $productInfo['variants'][$v2];
                                            if (!empty($variants)) {

                                                if ($variants['sku'] == $apiVariants[$v1]->sku) {
                                                    $source_id = $variants['entity_id'];
                                                    $source_price = floatval($variants['price']);
                                                    break;
                                                }
                                            }
                                        }
                                    }


                                    logger("position => ".$apiVariants[$v1]->position);


                                    $variantRecs = [
                                        'product_id' => $productID_db,
                                        'shopify_id' => $apiVariants[$v1]->id,
                                        'source_id' => $source_id,
                                        'image_id' => $mainProductImageID,
                                        'is_main' => ($apiVariants[$v1]->position ? 1 : 0),
                                        'title' => $product->title,
                                        'sku' => $apiVariants[$v1]->sku ? $apiVariants[$v1]->sku : $productInfo['sku'],
                                        'in_stock' => $in_stock,
                                        'inventory_quantity' => $apiVariants[$v1]->inventory_quantity ? $apiVariants[$v1]->inventory_quantity : $inventory_quantity,
                                        'inventory_policy' => $apiVariants[$v1]->inventory_policy,
                                        'inventory_management' => $apiVariants[$v1]->inventory_management ? $apiVariants[$v1]->inventory_management : 'shopify',
                                        'prime_eligible' => 0,
                                        'source_price' => $source_price,
                                        'shopify_price' => $shopify_price,
                                        'weight' => $apiVariants[$v1]->weight,
                                        'weight_unit' => $apiVariants[$v1]->weight_unit,
                                        'shipping_type' => 'Standard Shipping',
                                        'created_at' => date("Y-m-d H:i:s"),
                                        'updated_at' => date("Y-m-d H:i:s")
                                    ];

                                    $variantID = DBS::table('variants')->insertGetId($variantRecs);

                                    if (isset($variantAttsRecs) && count($variantAttsRecs) > 0) {
                                        foreach ($variantAttsRecs[$v1] as $attr) {
                                            DBS::table('attributes')->insert([
                                                'variant_id' => $variantID, 'dimension' => $attr['dimension'],
                                                'value' => $attr['value']
                                            ]);
                                        }
                                    }

                                }// END Variants:: LOOP
                            }// END Variants:: IF

//====== SAVE :: Product in Database :: END=================

                        }
                    } // END Product:: LOOP
                }// END Product:: IF
            }
            logger("===============END :: JOB :: Add Product ===============");
        return response()->json(['data' => 'success'], 200);

        }catch( \Exception $e ){
            logger("===============ERROR :: JOB :: Add Product ===============");
            logger(json_encode($e->getMessage()));

        }
    }


    public function ManageProductCollection($product_id,$shopifyProductID){
        try {
            logger('================= START:: Manage product to collection in Shopify =================');

            $shop = User::where('id', $this->shop_id)->first();

            $membership = null;
            $membership =  $this->getMembership($shop->id);

            //$MagentoStoreID = getMagentoStoreID($membership);

            $product_categories = DBS::table('magento_catalog_category_product')->select('category_id')->where('product_id', '=',
                $product_id)->get();

            if (count($product_categories) > 0) {

                for ($ct = 0; $ct < count($product_categories); $ct++) {

                    $magento_collection_id = $product_categories[$ct]->category_id;

                    $collections = DBS::table('collections')->where('user_id',$this->shop_id)->where('magento_collection_id', '=',$magento_collection_id)->get();

                    if (count($collections)>0) {
                        //Product Collection exist in shopify so,only assign product them
                       logger("Product Collection exist in shopify so,only assign product them");
                        for ($cl = 0; $cl < count($collections); $cl++) {
                            $collection_id = $collections[$cl]->shopify_id;
                            logger("collection_id :: ".$collection_id);
                            $this->AssignProductToCollection($shopifyProductID,$collection_id);
                        }
                    }else{
                        logger("Product Collectio does exist in shopify!. First create a collection after that assign product");
                        //Product Collectio does exist in shopify
                        // First create a collection after that assign product

                        $collectionInfo = DBS::table('magento_catalog_category_entity as cat')
                                ->join('magento_catalog_category_entity_varchar as cv', 'cat.entity_id', '=', 'cv.entity_id')
                                ->join('magento_eav_attribute as att', 'att.attribute_id', '=', 'cv.attribute_id')
                                ->join('magento_eav_entity_type as aty', 'att.entity_type_id', '=', 'aty.entity_type_id')
                                ->select('cat.*', 'cv.value as category_name')
                                ->whereNotNull('cv.value')
                                ->where('aty.entity_type_code', '=', 'catalog_category')
                                ->where('att.attribute_code', '=', 'name')
                                ->where('cat.entity_id','=',$magento_collection_id)
                                ->first();

                            if($collectionInfo) {

                                logger("===============Collection name====================");

                                logger($collectionInfo->category_name);

                                // Initialize Shopify product
                                $shopifyCollection = [
                                    'title' => $collectionInfo->category_name,
                                    'metafields' => [
                                        [
                                            'key' => 'magentodropship',
                                            'value' => 'bymagento',
                                            'value_type' => 'string',
                                            'namespace' => 'dropshipper'
                                        ]
                                    ]
                                ];

                                $collectionReq = $shop->api()->rest('POST', '/admin/api/'. env('SHOPIFY_API_VERSION') .'/custom_collections.json', ['custom_collection' => $shopifyCollection]);


                                $get_asset_call_limit = collect($collectionReq['response']->getHeader('x-shopify-shop-api-call-limit'))->first();

                                if ($get_asset_call_limit == '39/40') {

                                    logger("Delay the script execution for 1 seconds ");
                                    logger("Get assets, Waiting 1 seconds for a credit");
                                    sleep(1);

                                } // Delay the script execution for 1 seconds }


                                if (!$collectionReq['errors']) {
                                   logger("===========Shopify collection Response==============");
                                    logger(json_encode($collectionReq['body']));


                                //====== SAVE :: collection in Database =================

                            $collection = $collectionReq['body']['custom_collection'];



                            $collectionRec = [
                                'user_id' => $shop->id,
                                'shopify_id' => $collection['id'],
                                'magento_collection_id' => $collectionInfo->entity_id,
                                'shopify_handle' => $collection['handle'],
                                'title' => $collection['title'],
                                'sort_order' => $collection['sort_order'],
                                'source' => "Magento",
                                'status' => 1,
                                'is_exist_in_shopify' => 1,
                                'updated_at' => date("Y-m-d H:i:s"),
                            ];

                            $collectionID = DBS::table('collections')->insertGetId($collectionRec);

                            $this->AssignProductToCollection($shopifyProductID,$collection['id']);

                                //====== SAVE :: collection in Database :: END=================

                          }
                     }
                }
            }
        }

            return true;

            logger('================= END::  Manage product to collection in Shopify =================');
        }catch( \Exception $e ){
            logger('================= ERROR::  Manage product to collection in Shopify =================');
            logger($e->getMessage());
            return true;
        }
    }


    public function AssignProductToCollection($shopifyProductID,$collection_id)
    {
        try {

            logger('================= START:: Assign product to collection in Shopify =================');

            $shop = User::where('id', $this->shop_id)->first();

            logger("collects API Param");
            logger('product_id :: '.$shopifyProductID.' ||  collection_id :: '.$collection_id);

        $collectReq = $shop->api()->rest('POST','/admin/api/'.env('SHOPIFY_API_VERSION').'/collects.json', [
                'collect' => [
                    'product_id' => $shopifyProductID, 'collection_id' => $collection_id
                ]
            ]);

        logger("===============collectReq=================");
        logger(json_encode($collectReq));
       logger('================= END::  Assign product to collection in Shopify =================');

        return true;

        }catch( \Exception $e ){
            logger('================= ERROR::  Assign product to collection in Shopify =================');
            logger($e->getMessage());
            return true;
        }
    }


}
