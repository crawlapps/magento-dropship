<?php

use App\Models\MagentoUsersPlans;
use Illuminate\Support\Facades\Auth;
use App\Models\ShopifyShop;
use App\Models\User;
use DB as DBS;
use App\Models\MagentoCredential;

if (!function_exists('mysql2Con')) {
    function mysql2Con()
    {
        try {

           // logger('=========== START:: mysql2Con ===========');
            //logger("===================SECOND DB Connection START===========================");

            $mysql2  = DBS::connection('mysql2');

            return $mysql2;

        } catch (\Exception $e) {
            logger('=========== ERROR:: mysql2Con ===========');
            logger(json_encode($e));
        }
    }
}

if (!function_exists('getShopData')) {
    function getShopData($user_id)
    {
        try {

            logger('=========== START:: getShopData ===========');

            logger($user_id);

            $data = ShopifyShop::select('name','email','domain','currency','country_code')->where('user_id',$user_id)->first();

            return $data;

        } catch (\Exception $e) {
            logger('=========== ERROR:: getShopData ===========');
            logger(json_encode($e));
        }
    }
}


//if (!function_exists('LatestExchangeRates')) {
//    /**
//     * @return mixed
//     */
//    function LatestExchangeRates($base, $target)
//    {
//        try{
//            logger('=================== LatestExchangeRates ==============');
//            $rawdata = file_get_contents("https://data.fixer.io/api/latest?access_key=3175c637bf1d91ef7dedd6640654bb1a&base=$base&symbols=$target");
//
//            $data = json_decode($rawdata, true);
//
//            logger("====RATE::DATA===");
//            logger(json_encode($data));
//
//            return (@$data['rates'][$target]) ? $data['rates'][$target] : 0.00;
//        }catch(\Exception $e){
//            dd($e);
//        }
//    }
//}


if (!function_exists('webhooksList')) {
    function webhooksList($shop_id)
    {
        try {

            logger('================= START::webhooks List =================');

            $shop = User::where('id', $shop_id)->first();

            $webhookReq = $shop->api()->rest('GET', '/admin/api/'.env('SHOPIFY_API_VERSION').'/webhooks.json');

            logger("===============webhooks :: Result=================");
            logger(json_encode($webhookReq));
            logger('================= END::  webhooks List =================');

            return true;

        } catch (\Exception $e) {
            logger('================= ERROR::  webhooks List =================');
            logger($e->getMessage());
            return true;
        }
    }
}




if (!function_exists('getShopDataFromAPI')) {
    function getShopDataFromAPI($user, $fields)
    {
        logger('=========== START:: getShopDataFromAPI ===========');
        try {
            $shop = Auth::user();
            $parameter['fields'] = $fields;

            $shop_result = $shop->api()->rest('GET', 'admin/api/'.env('SHOPIFY_API_VERSION').'/shop.json', $parameter);

            if (!$shop_result->errors) {
                return $shop_result->body->shop;
            }

        } catch (\Exception $e) {
            logger('=========== ERROR:: getShopDataFromAPI ===========');
            logger(json_encode($e));
        }
    }
}

if (!function_exists('getMagentoStoreID')) {
    function getMagentoStoreID($code = "default")
    {

        $store_id = 1; //default

        $magento_store = DBS::table('magento_store')->select('store_id')->where('code', $code)->first();
        if($magento_store) {
            $store_id = $magento_store->store_id;
        }

        return $store_id;

    }
}


