<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();

       // $schedule->command('autoupdate:inventory')->everyMinute();
      //  $schedule->command('autoupdate:inventory')->twiceDaily(1, 13);  //Run the task daily at 1:00 & 13:00  //php artisan autoupdate:inventory
        $schedule->command('copy-tables:magento')->twiceDaily(1, 13); //$ php artisan copy-tables:magento
        $schedule->command('auto-delete:membership-products')->twiceDaily(1, 13); //$ php artisan auto-delete:membership-products

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
