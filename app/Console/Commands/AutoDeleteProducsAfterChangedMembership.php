<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\AutoDeleteProducsAfterChangedMembershipJob;

class AutoDeleteProducsAfterChangedMembership extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auto-delete:membership-products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try{
            logger('=============== START:: auto-delete:membership-products :: CRON =============');

            AutoDeleteProducsAfterChangedMembershipJob::dispatchNow();

            logger('=============== END:: auto-delete:membership-products :: CRON=============');
        }catch( \Exception $e ){
            logger('=============== ERROR:: auto-delete:membership-products :: CRON =============');
            logger($e);
        }
        return 0;
    }
}
