<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\AutoUpdateInventoryJob;


class AutoUpdateInventory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'autoupdate:inventory';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Inventory number count syncs with magento. if they add products to shopify we need to have an inventory count. then we need this way for the items inventory count to be updated the twice per day from magento';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        try{
            logger('=============== START:: AutoUpdate Inventory :: CRON =============');

            AutoUpdateInventoryJob::dispatchNow();

            logger('=============== END:: AutoUpdate Inventory :: CRON=============');
        }catch( \Exception $e ){
            logger('=============== ERROR:: AutoUpdate Inventory :: CRON =============');
            logger($e);
        }
        return 0;
    }
}
