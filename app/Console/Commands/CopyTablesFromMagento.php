<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\CopyTablesFromMagentoJob;
class CopyTablesFromMagento extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'copy-tables:magento';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Magento database tables syncs copy in laravel.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try{
            logger('=============== START:: Auto Copy magento Tables :: CRON =============');

            CopyTablesFromMagentoJob::dispatchNow();

            logger('=============== END:: Auto Copy magento Tables :: CRON=============');
        }catch( \Exception $e ){
            logger('=============== ERROR:: Auto Copy magento Tables :: CRON =============');
            logger($e);
        }
        return 0;
    }
}
