<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MagentoCredential extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'email',
        'password',
    ];


//    protected $hidden = [
//        'password',
//        'remember_token',
//    ];

}
