<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class OrdersTrackingInfo extends Model
{
    use HasFactory;

    protected $table = 'orders_tracking_infos';

    protected $fillable = [
        'db_order_id',
        'track_order_id',
        'track_number',
        'title',
        'carrier_code',
        'tracking_infos_more',
    ];

    protected $casts = [
        'tracking_infos_more' => 'array',
    ];

}
