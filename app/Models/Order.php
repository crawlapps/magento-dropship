<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'shopify_order_id',
        'name',
        'client_name',
        'email',
        'billing_address',
        'shipping_address',
        'total_weight',
        'fulfillment_status',
        'order_number',
        'is_assigned',
        'is_tracked'
    ];

    protected $casts = [
        'billing_address' => 'array',
        'shipping_address' => 'array',
    ];

    protected $newDateFormat = 'd-m-Y H:i:s';

    public function getUpdatedAtAttribute($value) {
        return Carbon::parse($value)->format($this->newDateFormat);
    }

    public function getCreatedAtAttribute($value) {
        return Carbon::parse($value)->format($this->newDateFormat);
    }

    public function LineItems(){
        return $this->hasMany(LineItem::class, 'db_order_id', 'id' );
    }


}
