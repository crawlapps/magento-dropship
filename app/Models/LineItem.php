<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LineItem extends Model
{
    use HasFactory;

    protected $fillable = [
        'db_order_id',
        'shopify_lineitem_id',
        'name',
        'product_id',
        'sku',
        'quantity',
        'mark_as_missing_status',
    ];

    public function Images(){
        return $this->hasOne(Images::class, 'product_id', 'product_id' );
    }
}
