<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MagentoUsersPlans extends Model
{
    use HasFactory;

    protected $table = 'magento_users_membership_plans';

    protected $fillable = [
        'user_id',
        'login_id',
        'membership_plan',
    ];

    protected $dates = ['deleted_at'];
}
