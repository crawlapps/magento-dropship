<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        "user_id",
        "shopify_id",
        "magento_product_id",
        "shopify_handle",
        "collection_id",
        "type",
        "locale",
        "source",
        "source_url",
        "source_currency",
        "source_currency_symbol",
        "description",
        "show_prices",
        "saller_ranks",
        "reviews",
        "history",
        "vendor",
        "status",
        "is_exist_in_shopify",
        "is_auto_update_inventory",
        "membership_for_free_store",
        "membership_for_limited_store",
        "membership_for_enterprize_store"
    ];

    protected $casts = [
        'collection_id' => 'array',
        'saller_ranks' => 'array',
        'reviews' => 'array',
    ];

}
