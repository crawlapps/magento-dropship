<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Collections extends Model
{
    use HasFactory;

    protected $fillable = [
        "user_id",
        "shopify_id",
        "magento_collection_id",
        "shopify_handle",
        "title",
        "source",
        "body_html",
        "sort_order",
        "status",
        "is_exist_in_shopify"
    ];


}
