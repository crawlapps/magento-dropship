<?php

namespace App\Traits;
use App\Models\MagentoCredential;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Intl\Currencies;
use Illuminate\Support\Facades\Http;
use App\Models\MagentoUsersPlans;
trait MagentoExportInfoTrait {


    public function getIntegrationAPIURL(){
        return config('const.integration_api_url');
    }

//=====================================================================================================================
// START :: Magento Config Data
//=====================================================================================================================
    public function getMagentoConfigData()
    {
        try {

            //logger('=========== START:: getMagentoConfigData ===========');

            $data = [];


            $magento_currency = DB::table('magento_core_config_data')
                ->where('scope','=','default')
                ->where('path','=','currency/options/default')->value('value');

            // logger("====Magento Config Data====");

            if($magento_currency){
                $data['magento_currency'] = $magento_currency;
            }
            $integration_api_url =  $this->getIntegrationAPIURL();
            $magento_site_url = $integration_api_url ? $integration_api_url : "https://hairbawse.com/";

            $config_web_base_url = DB::table('magento_core_config_data')
                ->where('scope','=','default')
                ->where('path','=','web/secure/base_url')->value('value');

            if($magento_currency){
                $data['magento_site_url'] = $config_web_base_url;
            }

            $data['magento_site_url'] = $magento_site_url;

            return $data;

        } catch (\Exception $e) {
            logger('=========== ERROR:: getMagentoConfigData ===========');
            logger(json_encode($e));
        }
    }
    //=====================================================================================================================
// START :: Magento Config Data
//=====================================================================================================================

    public function importReviews($productID,$magento_product_id)
    {
        try{

            logger('========== START:: ImportProductReview :: Trait  =========');

            $REVIEW_ENTITY_ID = DB::table('magento_review_entity')->select('entity_id')->where('entity_code', '=', 'product')->value('entity_id');
            $REVIEW_STATUS_ID = DB::table('magento_review_status')->select('status_id')->where('status_code', '=', 'Approved')->value('status_id');

            logger("REVIEW_ENTITY_ID :: ".$REVIEW_ENTITY_ID);
            logger("REVIEW_STATUS_ID :: ".$REVIEW_STATUS_ID);

            if($REVIEW_ENTITY_ID && $REVIEW_STATUS_ID){

                $Review_Details = DB::table('magento_review')
                    ->join('magento_review_detail', 'magento_review_detail.review_id', '=', 'magento_review.review_id')
                    ->select('magento_review_detail.title', 'magento_review_detail.detail','magento_review_detail.nickname','magento_review.created_at')
                    ->where('magento_review.entity_pk_value', '=', $magento_product_id)
                    ->where('magento_review.status_id', '=', $REVIEW_STATUS_ID)
                    ->where('magento_review.entity_id', '=',$REVIEW_ENTITY_ID)
                    ->get();

                logger("================REVIEW_DETAILS==================");
                logger(json_encode($Review_Details));

                $db_reviews = [];

                if($Review_Details && count($Review_Details)>0) {
                    for ($i = 0; $i < count($Review_Details); $i++) {

                        $review_data = [
                            "product_id" => $productID,
                            "title" => (@$Review_Details[$i]->title) ? $Review_Details[$i]->title : '',
                            "body" =>  (@$Review_Details[$i]->detail) ? $Review_Details[$i]->detail : '',
                            "date" =>  (@$Review_Details[$i]->created_at) ? $Review_Details[$i]->created_at : '',
                            "position" => $i,
                            "review_by" => (@$Review_Details[$i]->nickname) ? $Review_Details[$i]->nickname : '',
                        ];

                        array_push($db_reviews,$review_data);

                    }
                }

                logger("Product :: Review");
                logger(json_encode($db_reviews));

                DB::table('products')
                    ->where('id', $productID)
                    ->update(['reviews' => $db_reviews]);
            }

            logger('========== END:: ImportProductReview :: Trait =========');

        }catch( \Exception $e ){
            logger('========== ERROR:: ImportProductReview :: Trait =========');
            logger(json_encode($e));
        }
    }

    public function getProductTypesData()
    {
        try{

            // logger('========== START:: Get Product Type :: Trait  =========');

            $products_types_result = [];

            $attribute_code = 'brand';

            $attribute = DB::table('magento_eav_attribute as attr')
                ->join('magento_eav_entity_type as aty', 'attr.entity_type_id', '=', 'aty.entity_type_id')
                ->select('attr.attribute_id','attr.backend_type')
                ->where('aty.entity_type_code', '=', 'catalog_product')
                ->where('attr.attribute_code', '=', $attribute_code)
                ->first();


            if($attribute) {

                $backend_type = $attribute->backend_type;
                $table_name = "magento_catalog_product_entity_".$backend_type;

                logger("get product /type/brand");
                logger("table => ".$table_name );
                logger("attribute_id => ".$attribute->attribute_id );

                if(isset($table_name)) {
                    $attribute_value = DB::table($table_name)
                        ->select('value')
                        //->whereNotNull('value')
                        ->where('store_id', '=', 0)
                        ->where('attribute_id', '=', $attribute->attribute_id)
                        ->distinct()->pluck('value')->toArray();

                    $queryReq = DB::table("magento_magiccart_shopbrand");
                    $records = $queryReq->select('title')->whereIn('option_id',$attribute_value)->distinct()->pluck('title')->toArray();

                    $products_types_result = $records;
                }
            }

            return $products_types_result;

        }catch( \Exception $e ){
            logger('========== ERROR:: Get Product Type :: Trait =========');
            logger(json_encode($e));
        }
    }

    public function getProductCategoryData()
    {
        try {

            //logger('========== START:: Get Product Category :: Trait  =========');

            $products_category_array = [];

            $category_from_api = $this->getIntegrationCategory();

            if ($category_from_api && count($category_from_api) > 0) {

                $APIcategory = array_column($category_from_api,'id');

                $product_categories = DB::table('magento_catalog_category_product')->select('category_id')->whereIn('category_id',$APIcategory)->distinct()->pluck('category_id')->toArray();

                logger("product_categories");
                logger(json_encode($product_categories));

                $products_category_result = DB::table('magento_catalog_category_entity as cat')
                    ->join('magento_catalog_category_entity_varchar as cv', 'cat.entity_id', '=', 'cv.entity_id')
                    ->join('magento_eav_attribute as att', 'att.attribute_id', '=', 'cv.attribute_id')
                    ->join('magento_eav_entity_type as aty', 'att.entity_type_id', '=', 'aty.entity_type_id')
                    ->select('cat.entity_id', 'cv.value as category_name')
                    ->whereNotNull('cv.value')
                    ->where('aty.entity_type_code', '=', 'catalog_category')
                    ->where('att.attribute_code', '=', 'name')
                    ->where('cv.store_id', '=', 0)
                    ->whereIn('cat.entity_id', $product_categories)
                    ->distinct()
                    ->orderBy('category_name', 'ASC')
                    ->get();

                $products_category_array = $products_category_result;
            }
            return $products_category_array;

        }catch( \Exception $e ){
            logger('========== ERROR:: Get Product Categor :: Trait =========');
            logger(json_encode($e));
        }
    }


// NOTE***************************************************************************************************************
// catalog_product_entity.type_id = simple           // No variants
// catalog_product_entity.type_id = configurable     ////  variants available
//
// NOTE

//=====================================================================================================================
//START :: GET list of all magent product with variants //**configurable And Simple ALL Product Records

    public function getMagentoProductRecords()
    {
        try {


            logger('========== START:: Get Product Records :: Magento Trait =========');

            $response = [];

            $productInfo = [];

            $product = $this->getMagentoConfigurableProduct(4598);

            $product_main = DB::table('magento_catalog_product_entity as product')
                ->select('product.entity_id')
                ->where('type_id','=','configurable')
                //  ->offset(10)
                // ->limit(2)
                // ->get();
                ->paginate(15);


            if(count($product_main)>0){

                for($p=0; $p<count($product_main); $p++){

                    $productID = $product_main[$p]->entity_id;

                    $productInfo[$p] = $this->getMagentoConfigurableProduct($productID);

                }

            }

            $response['products'] = $productInfo;
            $response['total_product'] = (@$product_main) ? count($product_main) : 0;

            return $response;

        } catch (\Exception $e) {
            logger('========== ERROR:: Get Product Records :: Magento Trait =========');
            logger(json_encode($e));
        }
    }

//END :: GET configurable And Simple ALL Product Records
//=====================================================================================================================


//=====================================================================================================================
// START :: Product Main (Parent)  //Note : configurable product details
//=====================================================================================================================


    public function getMagentoConfigurableProduct($productID)
    {
        try{


            // logger('========== START:: Get Main Product Record :: Magento Trait  =========');

            // logger("===================SECOND DB Connection START===========================");

            $response = [];

            $productInfo = [];

            $magentoConfigData = $this->getMagentoConfigData();
            $magento_site_url = (isset($magentoConfigData['magento_site_url'])) ? $magentoConfigData['magento_site_url'] : "https://hairbawse.com/";

            // Product Main
            $product = DB::table('magento_catalog_product_entity as product')->select('product.*')->where('type_id','=','configurable')->where('entity_id','=',$productID)->first();

            $productID = $product->entity_id;

            $attribute_value = $this->GetMagentoProductAttrValue($productID);

            $productInfo = $attribute_value;

            $productInfo['entity_id'] = $productID;

            //Product SKU
            $productInfo['sku'] = $product->sku;
            $productInfo['type_id'] = $product->type_id;
            $productInfo['is_main'] = 1;
            //Product (has_options) static==========================================================================
            $productInfo['has_options'] = (@$product->has_options) ? $product->has_options : 0;
            //Product (has_options) static ==========================================================================


            $productInfo['price'] = (@$attribute_value['msrp']) ? $attribute_value['msrp'] : $attribute_value['price'];

            $product_price = $this->GetMagentoProductPrice($productID);

            $productInfo['min_price'] = (@$product_price->min_price) ? $product_price->min_price: 0;
            $productInfo['max_price'] = (@$product_price->max_price) ? $product_price->max_price: 0;
            //Product price ==========================================================================

            //Product (image) ==========================================================================
            $productInfo['image'] = (@$attribute_value['image']) ? $magento_site_url.'pub/media/catalog/product/'.$attribute_value['image'] : "";
            //Product (image) ==========================================================================

            //Product (small_image) ==========================================================================
            $productInfo['small_image'] = (@$attribute_value['small_image']) ? $magento_site_url.'pub/media/catalog/product/'.$attribute_value['small_image'] : "";
            //Product (small_image) ==========================================================================

            //Product (thumbnail) ==========================================================================
            $productInfo['thumbnail'] = (@$attribute_value['thumbnail']) ? $magento_site_url.'pub/media/catalog/product/'.$attribute_value['thumbnail'] : "";
            //Product (thumbnail) ==========================================================================


            //Product (created_at) static==========================================================================
            $productInfo['created_at'] = (@$product->created_at) ? $product->created_at : "";
            //Product (created_at) static==========================================================================

            //Product (updated_at) static==========================================================================
            $productInfo['updated_at'] = (@$product->updated_at) ? $product->updated_at : "";
            //Product (updated_at) static==========================================================================

            //Product (url_key) ==========================================================================
            $url_key = $attribute_value['url_key'];
            //Product (url_key) ==========================================================================

            //Product (currency) ==========================================================================
            $product_currency = (isset($magentoConfigData['magento_currency'])) ? $magentoConfigData['magento_currency'] : "USD";

            $productInfo['currency'] = $product_currency;

            $product_currency_symbol = Currencies::getSymbol($product_currency);

            $productInfo['currency_symbol'] = $product_currency_symbol;
            //Product (currency) ==========================================================================


            //Product (url_path) ==========================================================================
            $productInfo['url'] = $magento_site_url.'catalog/product/view/id/'.$productID.'/s/'.$url_key;
            //Product (url_path) ==========================================================================

            //Product inventory ==========================================================================
            $inventory_quantity = 0;
            $in_stock = 0;

            $product_quantity  = DB::table('magento_cataloginventory_stock_item')->select('is_in_stock','qty')->where('product_id','=',$productID)->first();

            if($product_quantity){
                $inventory_quantity = $product_quantity->qty;
                $in_stock = $product_quantity->is_in_stock;
            }

            $productInfo['inventory_quantity'] = $inventory_quantity;
            $productInfo['inventory_management'] = 'shopify';
            $productInfo['inventory_policy'] = 'deny';
            $productInfo['in_stock'] = $in_stock;

            //Product inventory ==========================================================================

            //Product Category ==========================================================================
            $product_categories = $this->GetMagentoProductCategory($productID);
            $productInfo['categories'] = $product_categories;
            //Product Category ==========================================================================

            //Product variants ==========================================================================

            $product_child = DB::table('magento_catalog_product_super_link')
                ->select('product_id')
                ->where('parent_id', '=', $productID)
                ->get();

            // $product_variants = $this->GetMagentoProductVariant($productID);

            // $productInfo['variants'] = $product_variants;
            $productInfo['variants_count'] = (@$product_child) ? count($product_child) : 0;

            // select * from catalog_product_super_link where product_id not in (select entity_id from catalog_product_entity where attribute_set_id = 31);

            //Product variants ==========================================================================

            $response = $productInfo;

            return $response;

            //logger("===================SECOND DB Connection START===========================");

        }catch( \Exception $e ){
            logger('========== ERROR:: Get Main Product Record :: Magento Trait =========');
            logger(json_encode($e));
        }
    }
//=====================================================================================================================
// Product Main (Parent) :: END
//=====================================================================================================================




//=====================================================================================================================
// START :: Product configurable variant
//=====================================================================================================================

    public function GetMagentoProductVariant($productID) // configurable product id
    {
        try{


            //  logger('========== START:: Magento Product variant :: Trait  =========');


            $parent_attribute_ids = [];

            //Product Attributes ==========================================================================
            $product_attributes = $this->GetMagentoConfigurableProductAttributes($productID);
            $productInfo['attributes'] = $product_attributes;
            //Product Attributes ==========================================================================


            if(count($product_attributes)>0){
                for($i=0;$i<count($product_attributes);$i++){
                    array_push($parent_attribute_ids,$product_attributes[$i]->attribute_id);
                }
            }

            // logger("parent_attribute_ids");
            //  logger(json_encode($parent_attribute_ids));


            //Product variants ==========================================================================
            $product_variants = [];

            $product_child = DB::table('magento_catalog_product_super_link')
                ->select('product_id')
                ->where('parent_id', '=', $productID)
                ->get();

            if(count($product_child)>0){

                for($p=0; $p<count($product_child); $p++){

                    $child_ProductID = $product_child[$p]->product_id;

                    $product_variants[$p]['parent_id'] = $productID;
                    $product_variants[$p] = $this->getMagentoSimpleProduct($child_ProductID,array_values($parent_attribute_ids));
                }
            }

            return $product_variants;


        }catch( \Exception $e ){
            logger('========== ERROR:: Magento Product variant :: Trait =========');
            logger(json_encode($e));
        }
    }

//=====================================================================================================================
// Product configurable variant
//=====================================================================================================================




//=====================================================================================================================
// START :: Product variants as a simple product  //Note : simple product details
//=====================================================================================================================
    public function GetMagentoSimpleProduct($productID,$parent_attribute_ids)
    {


        try{


            //  logger('========== START:: Get variant Product Record :: Magento Trait  =========');

            //  logger("===================SECOND DB Connection START===========================");

            $response = [];

            $productInfo = [];

            $magentoConfigData = $this->getMagentoConfigData();
            $magento_site_url = (isset($magentoConfigData['magento_site_url'])) ? $magentoConfigData['magento_site_url'] : "https://hairbawse.com/";


            // Product Main
            $product = DB::table('magento_catalog_product_entity as product')->select('product.*')->where('entity_id','=',$productID)->where('type_id','=','simple')->first();

            $productID = $product->entity_id;

            $attribute_value = $this->GetMagentoProductAttrValue($productID);
            $productInfo = $attribute_value;

            $productInfo['entity_id'] = $productID;

            //Product SKU
            $productInfo['sku'] = $product->sku;
            $productInfo['type_id'] = $product->type_id;
            $productInfo['is_main'] = 0;
            //Product (has_options) static==========================================================================
            $productInfo['has_options'] = (@$product->has_options) ? $product->has_options : 0;
            //Product (has_options) static ==========================================================================

            //Product Name (title)=====================================================================================

            $productInfo['price'] = (@$attribute_value['msrp']) ? $attribute_value['msrp'] : $attribute_value['price'];

            //Product (image) ==========================================================================
            $productInfo['image'] = (@$attribute_value['image']) ? $magento_site_url.'pub/media/catalog/product/'.$attribute_value['image'] : "";
            //Product (image) ==========================================================================

            //Product (small_image) ==========================================================================
            $productInfo['small_image'] = (@$attribute_value['small_image']) ? $magento_site_url.'pub/media/catalog/product/'.$attribute_value['small_image'] : "";
            //Product (small_image) ==========================================================================

            //Product (thumbnail) ==========================================================================
            $productInfo['thumbnail'] = (@$attribute_value['thumbnail']) ? $magento_site_url.'pub/media/catalog/product/'.$attribute_value['thumbnail'] : "";
            //Product (thumbnail) ==========================================================================

            //Product (created_at) static==========================================================================
            $productInfo['created_at'] = (@$product->created_at) ? $product->created_at : "";
            //Product (created_at) static==========================================================================

            //Product (updated_at) static==========================================================================
            $productInfo['updated_at'] = (@$product->updated_at) ? $product->updated_at : "";
            //Product (updated_at) static==========================================================================

            //Product (url_key) ==========================================================================
            $url_key = $attribute_value['url_key'];
            //Product (url_key) ==========================================================================

            //Product (currency) ==========================================================================
            $product_currency = (isset($magentoConfigData['magento_currency'])) ? $magentoConfigData['magento_currency'] : "USD";

            $productInfo['currency'] = $product_currency;

            $product_currency_symbol = Currencies::getSymbol($product_currency);

            $productInfo['currency_symbol'] = $product_currency_symbol;
            //Product (currency) ==========================================================================

            //Product (url_path) ==========================================================================
            $productInfo['url'] = $magento_site_url.'catalog/product/view/id/'.$productID.'/s/'.$url_key;

//            https://hairbawse.com/catalog/product/view/id/28/s/ampro-shine-n-jam-conditioning-gel-regular-hold-8-oz

            //Product (url_path) ==========================================================================

            //Product inventory ==========================================================================
            $inventory_quantity = 0;
            $in_stock = 0;

            $product_quantity  = DB::table('magento_cataloginventory_stock_item')->select('is_in_stock','qty')->where('product_id','=',$productID)->first();

            if($product_quantity){
                $inventory_quantity = $product_quantity->qty;
                $in_stock = $product_quantity->is_in_stock;
            }

            $productInfo['inventory_quantity'] = $inventory_quantity;
            $productInfo['inventory_management'] = 'shopify';
            $productInfo['inventory_policy'] = 'deny';
            $productInfo['in_stock'] = $in_stock;

            //Product inventory ==========================================================================

            //Product Category ==========================================================================
            $product_categories = $this->GetMagentoProductCategory($productID);
            $productInfo['categories'] = $product_categories;
            //Product Category ==========================================================================

            //Product Category ==========================================================================
            $product_attributes = $this->GetMagentoSimpleProductAttributes($productID,$parent_attribute_ids);
            $productInfo['attributes'] = $product_attributes;
            //Product Category ==========================================================================

            $response = $productInfo;

            return $response;

            // logger("===================SECOND DB Connection START===========================");

        }catch( \Exception $e ){
            logger('========== ERROR:: Get variant Product Record :: Magento Trait =========');
            logger(json_encode($e));
        }

    }

//=====================================================================================================================
// Product variants :: END
//=====================================================================================================================

    public function GetMagentoProductAttrValue($productID)
    {
        try{

            $attributeFieldArray = ["name","short_description","description","msrp","prie","image","small_image","thumbnail","featured","meta_keyword","meta_title","meta_description","price","price_type","product_source","links_exist","links_title","shipment_type","status","url_key","weight","weight_type","wholesale_price"];

            logger('========== START:: EAV (catalog_product) :: Trait  =========');

            $attributes = DB::table('magento_eav_attribute as attr')
                ->join('magento_eav_entity_type as aty', 'attr.entity_type_id', '=', 'aty.entity_type_id')
                ->select('attr.attribute_id','attr.backend_type','attr.attribute_code')
                ->where('aty.entity_type_code', '=', 'catalog_product')
                ->whereIn('attr.attribute_code',$attributeFieldArray)
                ->get();

            //  logger("===============attribute=====================");
            //  logger(json_encode($attributes));

            $attributeInfo = [];

            if(count($attributes)>0) {
                foreach($attributes as $attribute) {

                    $backend_type = $attribute->backend_type;
                    $table_name = "magento_catalog_product_entity_".$backend_type;

                    if (isset($table_name)) {
                        $attribute_value = DB::table($table_name)
                            ->select('value')
                            ->where('store_id', '=', 0)
                            ->where('attribute_id', '=', $attribute->attribute_id)
                            ->where('entity_id', '=', $productID)
                            ->value('value');

                        // logger("==============attribute_val====================");
                        //  logger($attribute_value);

                        if($attribute->attribute_code=="brand"){

                            logger("===Brand===");
                            logger("productID => ".$productID);
                            logger("attribute_value as optionid=> ".$attribute_value);

                            // $record = DB::table("magento_catalog_product_entity_int as brand_int")
                            //     ->join('magento_magiccart_shopbrand as brand_val', 'brand_int.value', '=',
                            //         'brand_val.option_id')
                            //     ->select('brand_val.title')
                            //     ->where('brand_int.store_id', '=', 0)
                            //     ->where('brand_int.attribute_id', '=', $attribute->attribute_id)
                            //     ->where('brand_int.entity_id', '=', $productID)
                            //     ->value('brand_val.title');

                            // logger("===Brand res===".$record);

                            $queryReq = DB::table("magento_magiccart_shopbrand");
                            $record = $queryReq->select('title')->where('option_id',$attribute_value)->distinct()->value('title');

                            logger("record");
                            logger($record);

                            $attributeInfo[$attribute->attribute_code] = $record ? $record : '';
                        }else{
                            $attributeInfo[$attribute->attribute_code] = $attribute_value;
                        }
                    }
                }
            }

            return $attributeInfo;

        }catch( \Exception $e ){
            logger('========== ERROR:: EAV (catalog_product) :: Trait =========');
            logger(json_encode($e));
        }
    }

//=====================================================================================================================
// Product Category :: START
//=====================================================================================================================

    public function GetMagentoProductCategory($productID)
    {
        try{


            //  logger('========== START:: Magento Product Category :: Trait  =========');

            $categoriesList = "";

            $product_categories  = DB::table('magento_catalog_category_product')->select('category_id')->where('product_id','=',$productID)->pluck('category_id')->toArray();

            if(count($product_categories)>0) {
                $collections = DB::table('magento_catalog_category_entity as cat')
                    ->join('magento_catalog_category_entity_varchar as cv', 'cat.entity_id', '=', 'cv.entity_id')
                    ->join('magento_eav_attribute as att', 'att.attribute_id', '=', 'cv.attribute_id')
                    ->join('magento_eav_entity_type as aty', 'att.entity_type_id', '=', 'aty.entity_type_id')
                    ->select('cat.*', 'cv.value as category_name')
                    ->whereNotNull('cv.value')
                    ->where('aty.entity_type_code', '=', 'catalog_category')
                    ->where('att.attribute_code', '=', 'name')
                    ->where('cv.store_id', '=', 0)
                    ->whereIn('cat.entity_id', $product_categories)
                    ->get();

                if(count($collections)>0) {

                    $categoriesList = $collections->pluck('category_name')->toArray();
                }
            }

            return $categoriesList;

        }catch( \Exception $e ){
            logger('========== ERROR:: Magento Product Category :: Trait =========');
            logger(json_encode($e));
        }
    }

//=====================================================================================================================
// Product Category :: END
//=====================================================================================================================



//=====================================================================================================================
// START :: Product Price  //Product  price (configurable product only)
//=====================================================================================================================

    public function GetMagentoProductPrice($productID)
    {
        try{
            //logger('========== START:: Magento Product Price :: Trait  =========');

            $product_price  = DB::table('magento_catalog_product_index_price')->select('price','min_price','max_price')->where('entity_id',$productID)->first();

            return  $product_price;

        }catch( \Exception $e ){
            logger('========== ERROR:: Magento Product Price :: Trait =========');
            logger(json_encode($e));
        }
    }

//=====================================================================================================================
// Product Category :: Price
//=====================================================================================================================



//=====================================================================================================================
//Configurable Product  Attributes :: START
//=====================================================================================================================

    public function GetMagentoConfigurableProductAttributes($productID)
    {
        try{


            //logger('========== START:: Magento Configurable Product Attributes :: Trait  =========');

            $attributesList = [];

            $attributes = DB::table('magento_catalog_product_super_attribute as cat')
                ->select('attr.frontend_label as attribute','attr.attribute_code','attr.attribute_id')
                ->leftJoin('magento_eav_attribute as attr', function($join) {
                    $join->on('cat.attribute_id', '=', 'attr.attribute_id');
                })
                ->leftJoin('magento_catalog_product_super_attribute_label as attr_opt', function($join) {
                    $join->on('cat.product_super_attribute_id', '=', 'attr_opt.product_super_attribute_id');
                })
//                ->leftJoin('eav_attribute_option_value as attr_opt_val', function($join) {
//                    $join->on('attr_opt.option_id', '=', 'attr_opt_val.option_id')
//                        ->where('attr_opt_val.store_id', '=', 0)
//                        ->where('attr_opt_val.value','<>',"");
//                })
                ->leftJoin('magento_eav_entity_type as aty', function($join) {
                    $join->on('attr.entity_type_id', '=', 'aty.entity_type_id')
                        ->where('aty.entity_type_code', '=', 'catalog_product');
                })
                //  ->where('attr_opt_val.store_id', '=', 0)
                //->whereNotNull('attr_opt_val.value')
                ->distinct()
                ->where('cat.product_id', $productID)
                ->get();

            if(count($attributes)>0) {
                $attributesList = $attributes->toArray();
            }
            return $attributesList;

        }catch( \Exception $e ){
            logger('========== ERROR::Magento Configurable Product Attributes :: Trait =========');
            logger(json_encode($e));
        }
    }


//=====================================================================================================================
//Configurable Product Attributes :: END
//=====================================================================================================================



    //=====================================================================================================================
    //Simple Product Attributes :: START
    //=====================================================================================================================


    public function GetMagentoSimpleProductAttributes($productID,$parent_attribute_ids)
    {
        try{


            //logger('========== START:: Magento Simple Product Attributes :: Trait  =========');

            $attributesList = [];

            $attributes = DB::table('magento_catalog_product_entity_int as cat')
                ->select('attr.frontend_label as attribute', 'attr_opt_val.value','attr.attribute_code','attr.attribute_id')
                ->leftJoin('magento_eav_attribute as attr', function($join) {
                    $join->on('cat.attribute_id', '=', 'attr.attribute_id'); // ->groupBy('attr.frontend_label');
                })
                ->leftJoin('magento_eav_attribute_option as attr_opt', function($join) {
                    $join->on('attr.attribute_id', '=', 'attr_opt.attribute_id');
                })
                ->leftJoin('magento_eav_attribute_option_value as attr_opt_val', function($join) {
                    $join->on('cat.value', '=', 'attr_opt_val.option_id');
//                        ->where('attr_opt_val.store_id', '=', 0)
//                        ->where('attr_opt_val.value','<>',"");
                })
                ->leftJoin('magento_eav_entity_type as aty', function($join) {
                    $join->on('attr.entity_type_id', '=', 'aty.entity_type_id')
                        ->where('aty.entity_type_code', '=', 'catalog_product');
                })
                ->where('attr_opt_val.store_id', '=', 0)
                ->whereNotNull('attr_opt_val.value')
                ->whereIn('attr.attribute_id',$parent_attribute_ids)
                ->distinct()
                ->where('cat.entity_id', $productID)
                ->get();

            if(count($attributes)>0) {
                $attributesList = $attributes->toArray();
            }
            return $attributesList;

        }catch( \Exception $e ){
            logger('========== ERROR::Magento Simple Product Attributes :: Trait =========');
            logger(json_encode($e));
        }
    }


//=====================================================================================================================
//Simple Product Attributes :: END
//=====================================================================================================================

    public function connectMagentoAccount($Credential){

        $email = $Credential['email'];
        $password =  $Credential['password'];

        $param = [
            "username" => $email,
            "password" => $password
        ];

        $integration_api_url =  $this->getIntegrationAPIURL();
        $apiAuthURL = $integration_api_url."integration/shopify/loginauth";

        $query_param = http_build_query($param);

        logger("query Param cred");
        logger($query_param);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $apiAuthURL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_POSTFIELDS => $query_param,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/x-www-form-urlencoded",
            ),
        ));

        $response = curl_exec($curl);


        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        logger('HTTP code: ' . $httpcode);

        curl_close($curl);

        if (isset($httpcode) && $httpcode == 200) {
            return json_decode($response,true);
        }else{
            return [];
        }

    }
    public function getMembership($shop_id){

        $membership = null;

        $magento_plan =  MagentoUsersPlans::where('user_id', $shop_id)->first();

        if($magento_plan) {
            $membership = $magento_plan->membership_plan;
            return $membership;
        }

        return $membership;

    }


    public function getIntegrationProduct(){

        $integration_api_url =  $this->getIntegrationAPIURL();
        $response = Http::get($integration_api_url.'integration/shopify/products');
        logger("intergration + api url :: ".$integration_api_url.'integration/shopify/products');
        $data = $response->json();

        return $data;

    }

    public function getIntegrationInventory(){

        $integration_api_url =  $this->getIntegrationAPIURL();
        $response = Http::get($integration_api_url.'integration/shopify/inventory');

        $data = $response->json();

        return $data;

    }


    public function getIntegrationCategory(){

        $integration_api_url =  $this->getIntegrationAPIURL();
        $response = Http::get($integration_api_url.'integration/shopify/category');

        $data = $response->json();

        return $data;

    }

    public function getIntegrationAssignOrder($orderId){

        $integration_api_url =  $this->getIntegrationAPIURL();
        $url = $integration_api_url.'integration/shopify/order?id='.$orderId;

        $response = Http::get($url);

        $data = $response->json();

        return $data;

    }

    public function checkAPI(){

        $integration_api_url =  $this->getIntegrationAPIURL();

        dd($integration_api_url);

    }


    public function getIntegrationProductFromLocal(){


        $products_response = [];

        $response =  DB::table('magento_integration_api_products')->select('products_response')->pluck('products_response')->toArray();

        if($response && count($response) > 0){

            foreach($response as $item) {

                $products_response[] = json_decode($item,true);
            }
        }

        return $products_response;

    }


    public function getIntegrationProductFromLocalByPlan($membership){


        $products_response = [];

        if($membership) {

            $response = DB::table('magento_integration_api_products')->select('products_response');

            if($membership=="free"){

                $response = $response->whereJsonContains("products_response",["available_for_free_store" => "1"])->orWhereJsonContains("products_response",["available_for_free_store" => 1]);
            }else  if ($membership == "enterprise") {

                $response = $response->whereJsonContains("products_response",["available_for_enterprize_store" => "1"])->orWhereJsonContains("products_response",["available_for_enterprize_store" => 1]);
            }

            $response = $response->pluck('products_response')->toArray();

            if ($response && count($response) > 0) {

                foreach ($response as $item) {

                    $products_response[] = json_decode($item, true);
                }
            }
        }

        return $products_response;

    }

}

