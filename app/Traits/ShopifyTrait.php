<?php

namespace App\Traits;
use App\Models\GlueOrders;
use App\Models\OrdersTrackingInfo;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Response;
trait ShopifyTrait {

    public function getShopCollectionsCategoryData($shop)
    {
        logger('=========== START:: getCollectionsData ===========');
        try {

            $errors = [];

            $collections = [];

            $apiRequest = $shop->api()->rest('GET', '/admin/api/'. env('SHOPIFY_API_VERSION') .'/custom_collections.json', ['limit' => 250]);

          //  logger(json_encode($apiRequest['body']['custom_collections']));

            if (isset($apiRequest['body']['custom_collections'])) {
                $collections = $apiRequest['body']['custom_collections'];
            }

           // logger(json_encode($collections));

            return $collections;


        } catch (\Exception $e) {
            logger('=========== ERROR:: getCollectionsData ===========');
            logger(json_encode($e));
        }
    }


    public function updateShopifyOrder($shopifyOrderId,$db_order_id,$user_id,$trackingInfo=[]){

        $res_status = "false";

        logger('====================== START:: updateShopifyOrder ======================');

        logger("================shopifyOrderId :: ".$shopifyOrderId);
        logger("================db_order_id :: ".$db_order_id);
        logger("================user_id :: ".$user_id);
        logger("================trackingCodes :: ".json_encode($trackingInfo));


         $titles = implode(", ",$trackingInfo['title']);

         logger("titles");
         logger($titles);

        $carrier_code = implode(", ",$trackingInfo['carrier_code']);

        $shop = User::where('id', $user_id)->first();
        $lineItemsID = [];

        $lineItems = DB::table('line_items')->select('shopify_lineitem_id')->where("db_order_id",$db_order_id)->get();

        if(count($lineItems)>0){

            foreach($lineItems as $item){
                $lineItemsID[] = ["id" => $item->shopify_lineitem_id];
            }

        }


        logger("================lineItemsID================");
        logger(json_encode($lineItemsID));


        $fulfillments_result = $shop->api()->rest('GET', '/admin/api/'. env('SHOPIFY_API_VERSION') .'/orders/'.$shopifyOrderId.'/fulfillments.json');

        logger("fulfillments_result");
        logger(json_encode($fulfillments_result));

        if(!$fulfillments_result['errors']) {

            logger("============fulfillments=================");

            $fulfillments = $fulfillments_result['body']['fulfillments'];

            if(isset($fulfillments) && count($fulfillments) > 0) {
                foreach ($fulfillments as $key => $item) {

                    logger("====fulfillment===ID:: ".$item['id']);

                    $fulfillmentID = $item['id'];

                    $OrderParam = [
                        "fulfillment" => [
                            "id" => $fulfillmentID,
                            "name" => $titles,
                            "status" => $trackingInfo['status'],
                            "tracking_numbers" => $trackingInfo['track_number'],
                            "carrier_code" => $carrier_code,
                            "tracking_company" => $carrier_code,
                            "line_items" => $lineItemsID
                        ]
                    ];

                    $OrderFullFillmentReq = $shop->api()->rest('PUT',
                        '/admin/api/'.env('SHOPIFY_API_VERSION').'/orders/'.$shopifyOrderId.'/fulfillments/'.$fulfillmentID.'.json', $OrderParam);

                    logger("=====updateShopifyOrder :: Response======================");
                    logger(json_encode($OrderFullFillmentReq));

                    if (!$OrderFullFillmentReq['errors']) {

                        $get_asset_call_limit = collect($OrderFullFillmentReq['response']->getHeader('x-shopify-shop-api-call-limit'))->first();

                        $res_status = "true";

                        logger("====variantReq::get_asset_call_limit");
                        logger($get_asset_call_limit);

                        if ($get_asset_call_limit == '39/40') {

                            logger("Delay the script execution for 1 seconds ");
                            logger("Get assets, Waiting 1 seconds for a credit");
                            sleep(1);

                        }
                    }
                }
            }else{
                logger("=================Create New fulfillments==============");


                $location_id = null;

                $locations = $this->getLocations($user_id);

                if($locations && count($locations)>0){
                    $location_id = $locations[0]['id'];
                 }

                logger("location_id from shopify=> ".$location_id);

                $OrderParam1 = [
                    "fulfillment" => [
                        "location_id" => 123456789,
//                        "name" => $trackingInfo['title'],
//                        "status" => $trackingInfo['status'],
//                        "fulfillment_status" => $trackingInfo['status'],
//                        "tracking_number" => $trackingInfo['track_number'],
//                        "tracking_company" => $trackingInfo['title'],
//                        "carrier_code" => $trackingInfo['carrier_code'],
                        "tracking_info" => [
                            "number" => $trackingInfo['track_number'],
                            "company" => $trackingInfo['title']
                        ],
                    ]
                ];

                $OrderParam = [
                    "fulfillment" => [
                        "location_id" => $location_id,
                        "tracking_numbers" => $trackingInfo['track_number'],
                        "notify_customer" => true,
                        "tracking_company" => $carrier_code,
                        "carrier_code" => $carrier_code,
                        "line_items" => $lineItemsID
                  ]
                ];

                $OrderFullFillmentReq = $shop->api()->rest('POST',
                    '/admin/api/'.env('SHOPIFY_API_VERSION').'/orders/'.$shopifyOrderId.'/fulfillments.json', $OrderParam);


                  logger("OrderFullFillmentReq");
                  logger(json_encode($OrderFullFillmentReq));


                if (!$OrderFullFillmentReq['errors']) {

                    $fulfillments = $fulfillments_result['body']['fulfillments'];

                    $res_status = "true";

                }

                logger("RES :: Create new fulfillments");
                logger(json_encode($fulfillments_result));

            }
        }

        logger($res_status);

        return $res_status;

        logger('====================== END:: updateShopifyOrder ======================');
    }



    public function getLocations($user_id)
    {
        logger('=========== START:: get Location Data ===========');
        try {
            $shop = User::where('id', $user_id)->first();
            $errors = [];

            $locations = [];

            $apiRequest = $shop->api()->rest('GET', '/admin/api/'. env('SHOPIFY_API_VERSION') .'/locations.json', ['limit' => 1]);

            logger("location :: Res");
             logger(json_encode($apiRequest['body']['locations']));

            if (isset($apiRequest['body']['locations'])) {
                $locations = $apiRequest['body']['locations'];
            }

             logger(json_encode($locations));

            return $locations;


        } catch (\Exception $e) {
            logger('=========== ERROR:: get Location Data ===========');
            logger(json_encode($e));
        }
}



    public function getSingleOrder($user_id,$shopify_order_id)
    {
        logger('=========== START:: get specific order Data ===========');
        try {
            $shop = User::where('id', $user_id)->first();
            $errors = [];

            $order = [];


            $apiRequest = $shop->api()->rest('GET', '/admin/api/'. env('SHOPIFY_API_VERSION') .'/orders/'.$shopify_order_id.'.json', ['limit' => 1]);

            logger("location :: Res");
             logger("get specific order Data");

               logger(json_encode($apiRequest));

            if (!$apiRequest['errors']) {
            logger(json_encode($apiRequest['body']['order']));

            if (isset($apiRequest['body']['order'])) {
                $order = $apiRequest['body']['order'];


           return ['success' => true, "data" => $order, 'message' => 'Order details found.'];

            }



            }else{
                 return ['success' => false, "data" => [], 'message' => 'Order details not found.' ];
            }




        } catch (\Exception $e) {
            logger('=========== ERROR:: get specific order Data ===========');
            logger(json_encode($e));
        }
    }

    public function getProductImage($user_id,$shopify_product_id)
    {
        logger('=========== START:: get product image Data ===========');
        try {
            $shop = User::where('id', $user_id)->first();
            $errors = [];

            $images = [];

            $apiRequest = $shop->api()->rest('GET', '/admin/api/'. env('SHOPIFY_API_VERSION') .'/products/'.$shopify_product_id.'/images.json');

         //   logger("Image :: Res");
         //   logger(json_encode($apiRequest));
            if (!$apiRequest['errors']) {
                if (isset($apiRequest['body']['images'])) {
                    $images = $apiRequest['body']['images'];
                }
            }

           // logger(json_encode($images));

            return $images;


        } catch (\Exception $e) {
            logger('=========== ERROR:: get product image Data ===========');
            logger(json_encode($e));
        }
    }






}
