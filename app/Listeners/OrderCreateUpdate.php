<?php

namespace App\Listeners;

use App\Events\CheckOrder;
use App\Models\Images;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\LineItem;
use App\Models\Order;
use App\Models\User;
use App\Models\Product;
use App\Jobs\AddProductImagesJob;
use DB;
class OrderCreateUpdate  implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CheckOrde  $event
     * @return void
     */
    public function handle(CheckOrder $event)
    {
        try {
            logger('==========START::  Listener:: Order create Update ==========');

            $ids = $event->ids;
            $user_id = $ids['user_id'];
            $data = $ids['data'];

            logger("user_id");
            logger($user_id);

            logger("============Order Webhook Data================");
            // logger(json_encode($data));

             $sh_product_ids = [];
             $sh_lineItems = [];

            if( !empty( $data->line_items ) ){
                $sh_lineItems = $data->line_items;
               logger("============Order :: sh_lineItems================");
               logger($sh_lineItems);

                foreach ( $sh_lineItems as $lkey=>$lval ){
                    $sh_product_ids[] = $lval->product_id;
                }
            }

            $is_exist_product = Product::where('user_id', $user_id)->whereIn('shopify_id', $sh_product_ids)->count();

            if( $is_exist_product > 0 ){
                logger("is_exist_product :: ".json_encode($sh_product_ids));

            $is_exist_order = Order::where('shopify_order_id', $data->id)->where('user_id', $user_id)->first();

            $db_order = ( $is_exist_order ) ? $is_exist_order : new Order;
            $db_order->user_id = $user_id;
            $db_order->shopify_order_id = $data->id;
            $db_order->name = $data->name;
            $db_order->client_name = (@$data->customer->first_name || @$data->customer->last_name) ? $data->customer->first_name.' '.$data->customer->last_name : 'No customer';
            $db_order->email = (@$data->email) ? $data->email : '';
            $db_order->billing_address = (@$data->billing_address) ? $data->billing_address : '';
            $db_order->shipping_address = (@$data->shipping_address) ? $data->shipping_address : '';
            $db_order->total_weight = (@$data->total_weight) ? $data->total_weight : 0;
            $db_order->fulfillment_status = (@$data->fulfillment_status) ? $data->fulfillment_status : 'Unfulfilled';
            $db_order->save();


            if( !empty( $sh_lineItems ) ){

                foreach ( $sh_lineItems as $lkey=>$lval ) {

                    $sh_product_id = $lval->product_id;
                    $exist_p = Product::where('shopify_id', $sh_product_id)->where('user_id', $user_id)->first();
                    if ($exist_p) {
                        logger("lval");
                        logger(json_encode($lval));

                        $is_exist_lineItem = LineItem::where('db_order_id', $db_order->id)->where('shopify_lineitem_id',
                            $lval->id)->first();

                        logger('====================================');
                        logger("exist line items");
                        logger(json_encode($is_exist_lineItem));

                        $lineItem = LineItem::firstOrNew([
                            'shopify_lineitem_id' => $lval->id, 'db_order_id' => $db_order->id
                        ]);
                        logger('====================================');
                        logger("lineItem");
                        logger(json_encode($lineItem));

                        //$lineItem = ( $is_exist_lineItem ) ? $is_exist_lineItem : new LineItem;
                        $lineItem->db_order_id = $db_order->id;
                        $lineItem->shopify_lineitem_id = $lval->id;
                        $lineItem->name = $lval->name;
                        $lineItem->product_id = $lval->product_id;
                        $lineItem->sku = $lval->sku;
                        $lineItem->quantity = $lval->quantity;
                        $lineItem->save();
                    }
                  }
                }
            }else{
                $db_order = Order::where('shopify_order_id', $data->id)->where('user_id', $user_id)->get();
                if( count($db_order ) > 0 ){
                    foreach ( $db_order as $dkey=>$dval ){
                        $lineItems = LineItem::where('db_order_id', $dval->id)->get();
                        if( count( $lineItems ) > 0 ){
                            foreach ( $lineItems as $lkey=>$lval ){
                                $lval->delete();
                            }
                        }
                        $dval->delete();
                    }
                }
            }

            logger('==========END::  Listener:: Order create Update ==========');
           }catch ( \Exception $e ){
             logger('========== ERROR:: Listener:: Order create Update ==========');
             logger($e);
            }

    }
}
