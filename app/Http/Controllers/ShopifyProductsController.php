<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;
use Carbon\Carbon;
use DB;
use App\Jobs\AddProductJob;
use Symfony\Component\Intl\Currencies;
use Illuminate\Database\QueryException;
use App\Traits\MagentoExportInfoTrait;
use App\Models\Variant as VariantModel;
class ShopifyProductsController extends Controller
{
    use MagentoExportInfoTrait;


    public function getMyProducts(Request $request){
        try{

            $input = $request->all();

            $filterData = $input['filterData'];

            $shop = Auth::user();
            $shopURL = 'https://' . $shop->getDomain()->toNative();

            $show_total_search_result = false;

            $productRecsQuery = DB::table('products')->where('user_id', $shop->id);

            //Filter :: Start =========================================================================================

            //filter For collection
            if(isset($filterData['collection']) && $filterData['collection']!=="0"){
                $filter_by_collection = $filterData['collection'];
                $productRecsQuery->whereJsonContains('collection_id', $filter_by_collection);
                $show_total_search_result = true;
            }

            //filter For search title, description, variant name
            else if(isset($filterData['search']) && $filterData['search']!=="" && $filterData['search'] !==null){
                $filter_by_search = $filterData['search'];
                $productRecsQuery->where('shopify_handle','LIKE', "%$filter_by_search%");
                $show_total_search_result = true;
                //->orWhere('products.description', 'LIKE', "%$filter_by_search%")->groupBy('variants.product_id');

                $productsBySKU = DB::table('variants')->select('product_id')->where('sku','=', $filter_by_search)->first();

                logger("======================productsBySKU========================================");
                logger(json_encode($productsBySKU));

                if($productsBySKU) {
                    $productRecsQuery->orWhere('id','=',$productsBySKU->product_id);
                }

            }

            //filter For product type (Brand)
            else if(isset($filterData['filterByType']) && $filterData['filterByType']!=="" && $filterData['filterByType'] !==null && $filterData['filterByType'] !=="0"){
                $filter_by_type = $filterData['filterByType'];
                $productRecsQuery->where('type','LIKE', "%$filter_by_type%");
                $show_total_search_result = true;
                //->orWhere('products.description', 'LIKE', "%$filter_by_search%")->groupBy('variants.product_id');
            }


            //filter For SORT-------------------

            else if(isset($filterData['filterBySort']) && $filterData['filterBySort']!=="" && $filterData['filterBySort'] !==null){
                $filter_by_sort = $filterData['filterBySort'];

                if($filter_by_sort=="titleAlpha"){

                    //=== Product title A–Z===
                    $productRecsQuery->orderBy('shopify_handle','ASC');

                }else if($filter_by_sort=="titleReverseAlpha"){

                    //===Product title Z–A===
                    $productRecsQuery->orderBy('shopify_handle','desc');

                }else if($filter_by_sort=="oldestCreated"){

                    //===Created (oldest first)===
                    $productRecsQuery->orderBy('created_at', 'ASC');

                }else if($filter_by_sort=="newestCreated"){

                    //===Created (newest first)===
                    $productRecsQuery->orderBy('created_at', 'desc');


                }else if($filter_by_sort=="typeAlpha"){

                    //===Product type A–Z===
                    $productRecsQuery->orderBy('type', 'ASC');

                }else if($filter_by_sort=="typeReverseAlpha"){

                    //===Product type Z–A===
                    $productRecsQuery->orderBy('type', 'desc');

                }else{
                    $productRecsQuery->orderBy('created_at', 'desc');
                }

            }
            //filter For SORT :: END --------------------------

            //Filter :: End ============================================================================================


            $productRecsQuery->select('products.*');
            $productRecsQuery->where("is_exist_in_shopify",1);


            logger("productRecsQuery");
            logger($productRecsQuery->toSql());

            $totalSearchProduct = $productRecsQuery->get();
            $total_search_product = count($totalSearchProduct);

            $productRecs =  $productRecsQuery->paginate(10);


            //Total Product Query
            $totalProduct =  DB::table('products')->select('products.*')->where('user_id', $shop->id)->where("is_exist_in_shopify",1)->get();
            $total_product = count($totalProduct);

            //End :: Total Product Query===================

            $data['pagination'] = [
                'previousPageUrl' => $productRecs->previousPageUrl(),
                'nextPageUrl' => $productRecs->nextPageUrl(),
            ];

            $products = [];

            if(count($productRecs)>0) {

                for ($p = 0; $p < count($productRecs); $p++) {

                    // Get product variants
                    $updatedAt = date('F/j/Y h:i A', strtotime($productRecs[$p]->updated_at));
                    $reviewCount = count(json_decode($productRecs[$p]->reviews, true));
                    $db_variants = DB::table('variants')->where('product_id', $productRecs[$p]->id)->first();
                    $db_variants_count = DB::table('variants')->where('product_id', $productRecs[$p]->id)->count();
                    if($db_variants) {
                        $db_image = DB::table('images')->select('url')->where('id', $db_variants->image_id)->first();
                    }
                    $image_url = (@$db_image) ? $db_image->url : '';

                    $db_variants_title = (@$db_variants) ? $db_variants->title : '';
                    $db_variants_shipping_type = (@$db_variants) ? $db_variants->shipping_type : '';
                    $db_variants_source_price = (@$db_variants) ? $db_variants->source_price : '';
                    $db_variants_shopify_price = (@$db_variants) ? $db_variants->shopify_price : '';

                    $products[$p] = [
                        'id' => $productRecs[$p]->id,
                        'shopify_id' => $productRecs[$p]->shopify_id,
                        'type' => $productRecs[$p]->type,
                        'title' => (@$productRecs[$p]->title) ? $productRecs[$p]->title : $db_variants_title,
                        'source' => $productRecs[$p]->source,
                        'source_url' => $productRecs[$p]->source_url,
                        'source_currency' => $productRecs[$p]->source_currency,
                        'source_currency_symbol' => $productRecs[$p]->source_currency_symbol,
                        'locale' => $productRecs[$p]->locale,
                        'shipping_type' => (@$productRecs[$p]->shipping_type) ? $productRecs[$p]->shipping_type : $db_variants_shipping_type,
                        'updated_at' => $updatedAt,
                        'source_prices' => $db_variants_source_price,
                        'variants_count' => (@$db_variants_count) ? $db_variants_count : 0,
                        'image_url' => $image_url,
                        'product_url' => $shopURL.'/products/'.$productRecs[$p]->shopify_handle,
                        'edit_url' => $shopURL.'/admin/products/'.$productRecs[$p]->shopify_id,
                        'shopify_price' => $db_variants_shopify_price,
                        'review_count' => $reviewCount,
                        'is_auto_update_inventory' => $productRecs[$p]->is_auto_update_inventory
                    ];
                }
            }

            $shope_data = getShopData($shop->id);

            if(!$shope_data){
                $shope_data = getShopDataFromAPI($shop, 'id, email, name,currency');
            }

            if($shope_data->currency){
                $currency = Currencies::getSymbol($shope_data->currency);
            }

            if($shope_data->currency){
                $currency = Currencies::getSymbol($shope_data->currency);
            }

            // $data['products'] = $products;
            $data['shop']['url'] = $shopURL;
            $data['products'] = $products;
            $data['total_search_product'] = (@$total_search_product) ? $total_search_product : 0;
            $data['total_product'] = (@$total_product) ? $total_product : 0;
            $data['show_total_search_result'] = $show_total_search_result;

            //  return ['success' => true, 'data' => $data];

            return  Response::json(['success' => true,"data" => $data,'currency'=> $currency, 'message' => 'Products Retrived successfully.'], 200);

        }catch( \Exception $e ){
            return [ 'success' => false, "data" => [], 'message' => $e->getMessage()];
        }
    }
    public function getMyProductsVariants(Request $request){

        $shop = Auth::user();
        $shopURL = 'https://' . $shop->getDomain()->toNative();
        $errors = [];
        try {

            $input = $request->all();
            $product_id = $input['product_id'];
            $shopify_id = $input['shopify_id'];

            logger("Get Variants :: product_id :: ".$product_id);


            $variants = VariantModel::with(['Attribute'])->where('product_id', $product_id)->orderBy('id', 'DESC')->get();

            if(count($variants)>0) {
                for ($v = 0; $v < count($variants); $v++) {

                    $db_image = DB::table('images')->select('url')->where('id', $variants[$v]->image_id)->first();
                    $image_url = ($db_image) ? $db_image->url : '';
                    $variants[$v]->image_url = $image_url;

                }
            }

            return ['success' => true, 'errors' => $errors, 'variants' => $variants];

        } catch (QueryException $e) {
            $errors[] = $e->getMessage();
            return ['success' => false, 'errors' => $errors];
        }
    }

    public function deleteProduct(Request $request)
    {
        $shop = Auth::user();

        $errors = [];

        $productID = $request->post('product_id');

        if (!preg_match('~^\d+$~', $productID)) {
            return [ 'success' => false, 'errors' => ["Invalid product id: $productID"]];
        }

        $productRec = DB::table('products')->where('id', $productID)->first();

        if (!$productRec) {
            return [ 'success' => false, 'errors' => ["Product id $productID was not found in the database."]];
        }

      //  DB::beginTransaction();

        try {
            $variantRecs = DB::table('variants')->where('product_id', $productID)->get();

            // Delete attributes
            foreach ($variantRecs as $vRec) {
                DB::table('attributes')->where('variant_id', $vRec->id)->delete();
            }

            // Delete variants
            DB::table('variants')->where('product_id', $productID)->delete();

            // Delete images
            DB::table('images')->where('product_id', $productRec->shopify_id)->delete();

            // Delete tags
            DB::table('tags')->where('product_id', $productID)->delete();

            // Delete product rec
            DB::table('products')->where('id', $productID)->delete();

            // Delete product from Shopify
            // TODO: Verify deletion, throw exception if unsuccessful
            if ($productRec->shopify_id) {
                $productReq = $shop->api()->rest('DELETE', '/admin/api/'. env('SHOPIFY_API_VERSION') .'/products/' . $productRec->shopify_id . '.json');
            }

        } catch (\Exception $e) {
         //   DB::rollBack();
            $errors[] = $e->getMessage();
            return ['success' => false, 'errors' => $errors];
        }

        //DB::commit();

        return ['success' => true, 'message' => 'Product has been deleted.', 'errors' => $errors];
    }

    public function deleteProductAll(Request $request)
    {

        $shop = Auth::user();

        $errors = [];

       // DB::beginTransaction();

        try {

            $products = DB::table('products')->where('user_id', $shop->id)->get();

            for ($i = 0; $i < count($products); $i++) {

                $productRec = $products[$i];
                $productID = $products[$i]->id;

                logger("DELETE :: Product :: ".$productID);

                $variantRecs = DB::table('variants')->where('product_id', $productID)->get();

                // Delete attributes
                foreach ($variantRecs as $vRec) {
                    DB::table('attributes')->where('variant_id', $vRec->id)->delete();
                }

                // Delete variants
                DB::table('variants')->where('product_id', $productID)->delete();

                // Delete images
                DB::table('images')->where('product_id', $productRec->shopify_id)->delete();

                // Delete tags
                DB::table('tags')->where('product_id', $productID)->delete();

                // Delete product rec
                DB::table('products')->where('id', $productID)->delete();

                // Delete product from Shopify
                // TODO: Verify deletion, throw exception if unsuccessful
                if ($productRec->shopify_id) {
                    $productReq = $shop->api()->rest('DELETE',
                        '/admin/api/'.env('SHOPIFY_API_VERSION').'/products/'.$productRec->shopify_id.'.json');
                }

            }

            return ['success' => true, 'message' => 'Product has been deleted.', 'errors' => $errors];

        } catch (\Exception $e) {
           // DB::rollBack();
            $errors[] = $e->getMessage();
            return ['success' => false, 'errors' => $errors];
        }

        //DB::commit();

        //return ['success' => true, 'message' => 'Product has been deleted.', 'errors' => $errors];
    }

    public function getReview(Request $request){
        try{
            $product_id = $request->id;
            $product = DB::table('products')->select('reviews')->where('id', $product_id)->first();
            if( $product ){
                return ['success' => true, 'data' => json_decode($product->reviews)];
            }else{
                return ['success' => false, 'errors' => 'Product not found'];
            }
        }catch( \Exception $e ){
            return ['success' => false, 'errors' => $e];
        }
    }

    public function ChangeAutoUpdateInventoryStatus(Request $request){
        try{

            logger("=======START::updateInventoryStatus=======");

            $products = $request->products;

            logger("products");
            logger(json_encode($products));

//            $is_mark = LineItem::where('db_order_id',$order_id)->whereIn('id', $line_items)
//                ->update(['mark_as_missing_status' => 1]);
//

            if(count($products)>0) {
                for($p=0; $p<(count($products)); $p++) {

                    logger("products id :: ".$products[$p]['id']);
                    logger("is_auto_update_inventory status :: ".$products[$p]['is_auto_update_inventory']);

                     $id = $products[$p]['id'];
                     $is_auto_update_inventory = $products[$p]['is_auto_update_inventory'];

                    $db_variant = DB::table('products')
                        ->where('id', $id)
                        ->update([
                            'is_auto_update_inventory' => $is_auto_update_inventory
                        ]);
                }
            }

            return  Response::json(['success' => true,'message' => 'Auto-update Inventory Status updated.'], 200);


            logger("=======END::updateLineItemsMarkStatus=======");

        }catch( \Exception $e ){

            logger("=======ERROR::updateLineItemsMarkStatus=======");
            logger(json_encode($e->getMessage()));

            return [ 'success' => false, 'message' => $e->getMessage()];
        }
    }


}
