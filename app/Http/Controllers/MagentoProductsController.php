<?php

namespace App\Http\Controllers;

use App\Models\MagentoCredential;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;
use Carbon\Carbon;
use App\Jobs\AddProductJob;
use Symfony\Component\Intl\Currencies;
use Illuminate\Database\QueryException;
use App\Traits\MagentoExportInfoTrait;
use Illuminate\Support\Facades\DB;

class MagentoProductsController extends Controller
{
    use MagentoExportInfoTrait;


    public function getProducts(Request $request){
        try{

            $shop = Auth::user();

            $membership = null;
            $membership =  $this->getMembership($shop->id);

            logger("membership => ".$membership);

            $membershipProducts = $this->getIntegrationProductFromLocal($membership);

            $shopURL = 'https://' . $shop->getDomain()->toNative();

            $input = $request->all();

            $filterData = $input['filterData'];

            $productInfo = [];

            $exist_product_in_shopify = [];

            $show_total_search_result = false;

            if(count($membershipProducts) > 0) {

                $APIProducts = array_column($membershipProducts,'product_id');

                $myProduct = DB::table('products')->where('user_id',$shop->id)->pluck('magento_product_id')->toArray();

                if ($myProduct) {
                    $exist_product_in_shopify = $myProduct;
                }

                $simpleProducts = DB::table('magento_catalog_product_super_link')
                    ->select('product_id')
                    ->pluck('product_id')
                    ->toArray();

                $getSingleProduct = DB::table('magento_catalog_product_entity')
                    ->whereNotIn('entity_id', $simpleProducts)
                    ->pluck('entity_id')
                    ->toArray();

                $productRecsQuery = DB::table('magento_catalog_product_entity as product')
                    ->select('product.entity_id', 'product.type_id')
                    ->whereNotIn('product.entity_id', $exist_product_in_shopify)
                    ->whereIn('product.type_id', ['configurable', 'simple'])
                    ->whereIn('product.entity_id', $APIProducts);
              


                //Filter :: Start =========================================================================================

                //filter For collection
                if (isset($filterData['collection']) && $filterData['collection'] !== "0") {

                    $filter_by_collection = $filterData['collection'];
                    $product_categories = DB::table('magento_catalog_category_product')->select('product_id')->where('category_id',
                        $filter_by_collection)->pluck('product_id')->toArray();

                    $productRecsQuery->whereIn('product.entity_id', $product_categories);

                    $show_total_search_result = true;
                } else if (isset($filterData['search']) && $filterData['search'] !== "" && $filterData['search'] !== null) {

                    $filter_by_search = $filterData['search'];


                    $attribute_code = "name";
                    $attribute = DB::table('magento_eav_attribute as attr')
                        ->join('magento_eav_entity_type as aty', 'attr.entity_type_id', '=', 'aty.entity_type_id')
                        ->select('attr.attribute_id', 'attr.backend_type')
                        ->where('aty.entity_type_code', '=', 'catalog_product')
                        ->where('attr.attribute_code', '=', $attribute_code)
                        ->first();


                    if ($attribute) {

                        $backend_type = $attribute->backend_type;
                        $table_name = "magento_catalog_product_entity_".$backend_type;

                        //   logger("attribute_code :: ".$attribute_code);

                        // logger($table_name);

                        if (isset($table_name)) {
                            $attribute_value = DB::table($table_name)
                                ->select('entity_id')
                                //->whereNotNull('value')
                                ->where('store_id', '=', 0)
                                ->where('attribute_id', '=', $attribute->attribute_id)
                                ->where('value', 'LIKE', "%$filter_by_search%")
                                ->pluck('entity_id')->toArray();

//                              $product_parent_id = DB::table('magento_catalog_product_super_link')
//                                  ->whereIn('product_id',$attribute_value)
//                                  ->select('parent_id')
//                                  ->pluck('parent_id')->toArray();

                            $productRecsQuery->WhereIn('product.entity_id', $attribute_value);


                            // logger("==============attribute_val====================");
                            //  logger($attribute_value);

                        }
                    }

                    $productRecsQuery->orWhere('product.sku', $filter_by_search);

                    $show_total_search_result = true;

                } else if (isset($filterData['filterByType']) && $filterData['filterByType'] !== "" && $filterData['filterByType'] !== null  && $filterData['filterByType'] !== "0") {
                    $filter_by_type = $filterData['filterByType'];
                    $products_brand_response = [];

                    $Brandresponse =  DB::table('magento_integration_api_products')->select('products_response')->where('products_response->brand', 'like', '%'.$filter_by_type.'%')->pluck('products_response')->toArray();


                    if($Brandresponse && count($Brandresponse) > 0){

                        foreach($Brandresponse as $item) {

                            $products_brand_response[] = json_decode($item,true);
                        }
                    }

                     $APIProductsBrand = array_column($products_brand_response,'product_id');

                     $productRecsQuery->WhereIn('product.entity_id', $APIProductsBrand);

                     $show_total_search_result = true;

                } else if (isset($filterData['filterBySort']) && $filterData['filterBySort'] !== "" && $filterData['filterBySort'] !== null) {
                    $filter_by_sort = $filterData['filterBySort'];

                    $is_search_sort = false;

                    if ($filter_by_sort == "titleAlpha") {

                        //=== Product title A–Z===
                        $column = 'value';
                        $order = 'asc';
                        $attribute_code = "name";
                        $is_search_sort = true;

                    } else if ($filter_by_sort == "titleReverseAlpha") {

                        //===Product title Z–A===

                        $column = 'value';
                        $order = 'desc';
                        $attribute_code = "name";
                        $is_search_sort = true;

                    } else if ($filter_by_sort == "oldestCreated") {

                        //===Created (oldest first)===

                        $productRecsQuery->orderBy('product.created_at', 'ASC');

                    } else if ($filter_by_sort == "newestCreated") {

                        //===Created (newest first)===
                        $productRecsQuery->orderBy('product.created_at', 'desc');


                    } else if($filter_by_sort == "typeAlpha") {

                        //===Product type A–Z===

                        $column = 'value';
                        $order = 'asc';
                        $attribute_code = "brand";
                        $is_search_sort = true;


                    } else  if ($filter_by_sort == "typeReverseAlpha") {

                        //===Product type Z–A===
                        $column = 'value';
                        $order = 'desc';
                        $attribute_code = "brand";
                        $is_search_sort = true;

                    }


                    if ($is_search_sort) {

                        $attribute = DB::table('magento_eav_attribute as attr')
                            ->join('magento_eav_entity_type as aty', 'attr.entity_type_id', '=',
                                'aty.entity_type_id')
                            ->select('attr.attribute_id', 'attr.backend_type')
                            ->where('aty.entity_type_code', '=', 'catalog_product')
                            ->where('attr.attribute_code', '=', $attribute_code)
                            ->first();


                        if ($attribute) {

                            $backend_type = $attribute->backend_type;
                            $table_name = "magento_catalog_product_entity_".$backend_type;

                            //   logger("attribute_code :: ".$attribute_code);

                            // logger($table_name);

                            if (isset($table_name)) {
                                $attribute_value = DB::table($table_name)
                                    ->select('entity_id')
                                    //->whereNotNull('value')
                                    ->where('store_id', '=', 0)
                                    ->where('attribute_id', '=', $attribute->attribute_id)
                                    ->orderBy($column, $order)
                                    ->pluck('entity_id')->toArray();

//                              $product_parent_id = DB::table('magento_catalog_product_super_link')
//                                  ->whereIn('product_id',$attribute_value)
//                                  ->select('parent_id')
//                                  ->pluck('parent_id')->toArray();
                                $productRecsQuery->join($table_name.' as tabel2',
                                    'product.entity_id', '=', 'tabel2.entity_id')
                                    ->where('tabel2.store_id', '=', 0)
                                    ->where('tabel2.attribute_id', '=', $attribute->attribute_id)
                                    ->orderBy('tabel2.'.$column, $order);


                                //$productRecsQuery->WhereIn('entity_id', $attribute_value);
                                //  $productRecsQuery->orderBy('entity_id', $order);

                                // logger("==============attribute_val====================");
                                //  logger($attribute_value);

                            }
                        }
                    }

                }else{
                  
                    $productRecsQuery->orderBy('product.created_at', 'desc');
                }


              //  $productRecsQuery = $productRecsQuery->whereIn('product.entity_id', $getSingleProduct);

                //Filter :: END =========================================================================================


                //simple product


                // Product Main
                $totalSearchProduct = $productRecsQuery->get();


                logger("====totalSearchProduct===");
                logger(count($totalSearchProduct));

                $products = $productRecsQuery->paginate(10);


                //Total Product Query

                $totalProduct = DB::table('magento_catalog_product_entity as product')
                    ->select('product.entity_id', 'product.type_id')
                    ->whereNotIn('product.entity_id', $exist_product_in_shopify)
                    ->whereIn('product.type_id', ['configurable', 'simple'])
                    //->WhereIn('product.entity_id', $getSingleProduct)
                    ->get()->toArray();

                $total_product = count($totalProduct);


                //Total Search Product=======================


                $total_search_product = count($totalSearchProduct);


                //End :: Total Product Query===================


                if (count($products) > 0) {

                    for ($p = 0; $p < count($products); $p++) {

                        $productID = $products[$p]->entity_id;

                        logger("productID => ".$productID);

                        if(in_array($productID,$APIProducts)) {

                            if ($products[$p]->type_id == "simple") {
                                $productInfo[$p] = $this->GetMagentoSimpleProduct($productID, []);
                            }
                            if ($products[$p]->type_id == "configurable") {
                                $productInfo[$p] = $this->getMagentoConfigurableProduct($productID);
                            }

                            $productInfo[$p]['inventory_quantity'] = 0;

                            logger("wholesale_price =. ".$productInfo[$p]['wholesale_price']);

                            $wholesale_price = isset($productInfo[$p]['wholesale_price']) ? $productInfo[$p]['wholesale_price'] : 0;

                            $productInfo[$p]['product_type_id'] = $products[$p]->type_id;
                            $productInfo[$p]['able_to_add_shopify'] = "false";


                            if (in_array($productID, array_column($membershipProducts, 'product_id'))) {

                                $found_key = array_search($productID, array_column($membershipProducts, 'product_id'));

                                logger("Found Key :: ".$found_key);

                                $availableProduct = $membershipProducts[$found_key];

                                logger("availableProduct");
                                logger(json_encode($availableProduct));

                                $productInfo[$p]['brand'] = @($availableProduct['brand']) ? $availableProduct['brand'] : '';
                                $productInfo[$p]['inventory_quantity'] = @($availableProduct['qty']) ? $availableProduct['qty'] : 0;
                                $productInfo[$p]['name'] = @($availableProduct['name']) ? $availableProduct['name'] : '';
                                $productInfo[$p]['sku'] = @($availableProduct['sku']) ? $availableProduct['sku'] : $productInfo['sku'];

                                if ($availableProduct) {
                                    logger("membership => ".$membership);
                                    // logger("availableProduct");
                                    //logger($availableProduct);

                                    $productInfo[$p]['wholesale_price'] = $availableProduct['wholesale_price_default'] ? $availableProduct['wholesale_price_default'] : $wholesale_price;

                                    if ($membership == "free") {
                                        logger("membership => is ".$membership);
                                        $available_membership = $availableProduct['available_for_free_store'];
                                        logger("available_membership => is".$available_membership);
                                        if ($available_membership == 1) {
                                            $productInfo[$p]['able_to_add_shopify'] = "true";
                                        }

                                        $productInfo[$p]['wholesale_price'] = $availableProduct['wholesale_price_free'] ? $availableProduct['wholesale_price_free'] : $productInfo[$p]['wholesale_price'];

                                    }else if($membership == "limited") {
                                        logger("membership => is ".$membership);

                                            $available_membership = $availableProduct['available_for_limited_store'];
                                        logger("available_membership => is".$available_membership);
                                            if ($available_membership == "1") {
                                                $productInfo[$p]['able_to_add_shopify'] = "true";
                                            }

                                            $productInfo[$p]['wholesale_price'] = $availableProduct['wholesale_price_limited'] ? $availableProduct['wholesale_price_limited'] : $productInfo[$p]['wholesale_price'];

                                        }else if($membership == "enterprise") {

                                        logger("membership => is ".$membership);
                                                $available_membership = $availableProduct['available_for_enterprize_store'];
                                        logger("available_membership => is".$available_membership);

                                                if ($available_membership == 1) {
                                                    $productInfo[$p]['able_to_add_shopify'] = "true";
                                                }

                                                $productInfo[$p]['wholesale_price'] = $availableProduct['wholesale_price_enterprise'] ? $availableProduct['wholesale_price_enterprise'] : $productInfo[$p]['wholesale_price'];

                                            }
                                }
                            }
                        }
                    }

                }

                $magentoConfigData = $this->getMagentoConfigData();

                $magento_site_url = (isset($magentoConfigData['magento_site_url'])) ? $magentoConfigData['magento_site_url'] : "https://hairbawse.com/";


                $data['magento_site_url'] = $magento_site_url;

                $data['pagination'] = [
                    'previousPageUrl' => $products->previousPageUrl(),
                    'nextPageUrl' => $products->nextPageUrl(),
                ];

            }

            //$FilteredProductsInfo = $this->getFilteredProducts($productInfo,$filterData);

            $data['products'] = $productInfo;
            $data['shop']['url'] = $shopURL;
            //$data['products'] = $FilteredProductsInfo;
            $data['total_search_product'] = (@$total_search_product) ? $total_search_product : 0;
            $data['total_product'] = (@$total_product) ? $total_product : 0;
            $data['show_total_search_result'] = $show_total_search_result;

            //  return ['success' => true, 'data' => $data];



            return  Response::json(['success' => true,"data" => $data,'message' => 'Products Retrived successfully.'], 200);



        }catch( \Exception $e ){
            return [ 'success' => false, "data" => [], 'message' => $e->getMessage()];
        }
    }


// START :: Applied Filter for product ================================================================================
    public function getFilteredProducts($productInfo,$filterData){

        //Filter

        logger("filterData");
        logger($filterData);

        $collection = collect($productInfo);

        // $filtered = $collection->where('price', 100);

        //Filter :: Start =========================================================================================

        //filter For search title, description, variant name
//            if(isset($filterData['search']) && $filterData['search']!=="" && $filterData['search'] !==null){
//                $filter_by_search = $filterData['search'];
//
//                logger("filter_by_search :: ".$filter_by_search);
//
//
//                $filtered = $collection->filter(function ($item) use($filter_by_search){
//
//                      // logger("name ".$item['name']);
//
//                     //  return substr(strrchr($item['name'], $filter_by_search), 1);
//
//                    if(strpos($item['name'], $filter_by_search) !== false){
//
//                        return true;
//
//                    }else if(strpos($item['name'], ucwords($filter_by_search)) !== false){
//
//                        return true;
//
//                    }else if(strpos($item['name'], strtoupper($filter_by_search)) !== false){
//
//                        return true;
//
//                    }else if(strpos($item['name'], strtolower($filter_by_search)) !== false){
//
//                        return true;
//                    }
//                    else{
//                        return  false;
//                    }
//
//                  });
//
//
//                logger("filtered  search:: ".json_encode($filtered));
//
//                $filteredProductInfo =   $filtered->values()->all();
//
//            }else

//           if(isset($filterData['filterByType']) && $filterData['filterByType']!=="" && $filterData['filterByType'] !==null){
//                $filter_by_type = $filterData['filterByType'];
//                $filtered = $collection->filter(function ($item) use($filter_by_type){
//
//                    return $item['brand']==$filter_by_type;
//
//                });
//                $filteredProductInfo =  $filtered->values()->all();
//
//            }else
//
        if(isset($filterData['filterBySort']) && $filterData['filterBySort']!=="" && $filterData['filterBySort'] !==null){
            $filter_by_sort = $filterData['filterBySort'];

            if($filter_by_sort=="titleAlpha"){

                //=== Product title A–Z===
                $sorted = $collection->sortBy([
                    ['name', 'asc'],
                ]);

            }else if($filter_by_sort=="titleReverseAlpha"){

                //===Product title Z–A===
                $sorted = $collection->sortBy([
                    ['name', 'desc'],
                ]);

            }else if($filter_by_sort=="oldestCreated"){

                //===Created (oldest first)===
                $sorted = $collection->sortBy([
                    ['created_at', 'asc'],
                ]);

            }else if($filter_by_sort=="newestCreated"){

                //===Created (newest first)===
                $sorted = $collection->sortBy([
                    ['created_at', 'desc'],
                ]);


            }else if($filter_by_sort=="typeAlpha"){

                //===Product type A–Z===
                $sorted = $collection->sortBy([
                    ['brand', 'asc'],
                ]);

            }else if($filter_by_sort=="typeReverseAlpha"){

                //===Product type Z–A===
                $sorted = $collection->sortBy([
                    ['brand', 'desc'],
                ]);

            }else{
                $sorted = $collection->sortBy([
                    ['id', 'desc'],
                ]);
            }

            $filtered = $sorted->values();

            $filteredProductInfo =  $filtered->all();

        }else{
            $filteredProductInfo = $collection->all();
        }
        //filter For SORT :: END --------------------------

        //Filter :: End ============================================================================================



        return  $filteredProductInfo;

    }

// END :: Applied Filter for product ================================================================================

    public function getProductsVariants(Request $request){

        $errors = [];
        try {

            $input = $request->all();
            $productID = $input['product_id'];

            logger("Get magento product Variants :: product_id :: ".$productID);

            $variants = $this->GetMagentoProductVariant($productID);

            return ['success' => true, 'errors' => $errors, 'variants' => $variants];

        } catch (QueryException $e) {
            $errors[] = $e->getMessage();
            return ['success' => false, 'errors' => $errors];
        }
    }


    public function addProducts(Request $request){
        try{

            $data = [];

            $shop = Auth::user();

            $shopURL = 'https://' .$shop->getDomain()->toNative();

            $productInfo["product_info"] = json_decode($request->post('product_info'));
            $productInfo["auto_update"]  = json_decode($request->post('auto_update'));
            logger(json_encode($productInfo));

            AddProductJob::dispatch($shop->id, $productInfo);

            $jobmsg = 'Some products are being pushed in the background. Check product in few seconds.';

            return  Response::json(['success' => true,"data" => $data,'message' => $jobmsg], 200);

        }catch( \Exception $e ){
            return [ 'success' => false, 'message' => $e->getMessage()];
        }
    }


    public function getProductsTypes(){
        try {

            $productTypesData = [];

            // $product_types = $this->getProductTypesData();

            //$queryReq = DB::table("magento_magiccart_shopbrand");

            //$records = $queryReq->select('title')->distinct()->orderBy('title', 'asc')->pluck('title')->toArray();

            $membershipProducts = $this->getIntegrationProductFromLocal();

            //  logger($product_types);

//            if(count($records)>0) {
//
//                $productTypesData = $records;
//            }


            if(count($membershipProducts) > 0) {

                $productTypesData =  array_unique(array_filter(array_column($membershipProducts, 'brand')));

            }


            return ['success' => true, 'product_types' => $productTypesData];

        } catch (\Exception $e) {
            dump($e);
        }
    }


    public function getProductsCategory(){
        try {

            $productCategoryData = [];

            $productCategoryData = $this->getProductCategoryData();

            return ['success' => true, 'collections' => $productCategoryData];

        } catch (\Exception $e) {
            dump($e);
        }
    }


}
