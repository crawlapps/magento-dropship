<?php

namespace App\Http\Controllers;
use Crypt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Traits\ShopifyTrait;
use App\Traits\MagentoExportInfoTrait;
use App\Models\MagentoCredential;
use App\Models\MagentoUsersPlans;

class AppController extends Controller
{
    use ShopifyTrait;
    use MagentoExportInfoTrait;

    public function getCollectionsCategory(){
        try {
            $shop = Auth::user();

            $collections = $this->getShopCollectionsCategoryData($shop);

            return ['success' => true, 'collections' => $collections];

        } catch (\Exception $e) {
            dump($e);
        }
    }


    public function getMagentoCredentials(Request $request){

//        $str = Crypt::encrypt("1234");
//
//        $decrpt = Crypt::decrypt($str);
//
//        dd($decrpt);

        $errors = [];
        try {

            $shop = Auth::user();

            logger("Get Magento Credential");
             $userInfo = [];
             $user =  MagentoCredential::where('user_id', $shop->id)->first();


            if($user){
                $userInfo['email'] = $user->email;

                $decrypted_pass = $user->password;

                $password = base64_decode($decrypted_pass);

                $userInfo['password'] = $password;
            }

            logger("userInfo".json_encode($userInfo));

            $integration_api_url =  $this->getIntegrationAPIURL();

            $loginauth_api_url= $integration_api_url.'integration/shopify/login';

           return ['success' => true, 'errors' => $errors, "loginauth_api_url" => $loginauth_api_url,  "user" => $userInfo];

        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
            return ['success' => false, 'errors' => $errors];
        }
    }

    public function saveMagentoCredentials(Request $request){

        $errors = [];
        try {

            $shop = Auth::user();

            logger("Save Magento Credential");

            $input = $request->all();
            $email = $input['email'];
            $password = $input['password'];

            logger("USER CRED DATA :: ".json_encode($input));

            $password = base64_encode($password);

            logger("enc : password". $password);

           // $delete = MagentoCredential::truncate();

          //  logger("check-magento-crad-api :: res");


            $response  =  $this->connectMagentoAccount($input);

                if (isset($response['status']) && $response['status']) {


                   // if (isset($response['membership']) && $response['membership']=="free") {

                     //   return ['success' => false, 'errors' => $errors, 'message' => 'Must be Beautyplus account to access products'];


                   // }else {

                        $exist = MagentoCredential::where('email', $email)->where('user_id','=', $shop->id)->first();
                        $message = 'Logged In Successfully.';
                        if($exist) {
                            $message = 'Credential updated Successfully.';
                        }


                        $user = MagentoCredential::firstOrNew(['user_id' => $shop->id,'email' => $email]);

                        $user->user_id = $shop->id;
                        $user->email = $email;
                        $user->password = $password;
                        $user->save();

                        $magento_plan = MagentoUsersPlans::firstOrNew(['user_id' => $shop->id]);

                        $magento_plan->user_id = $shop->id;
                        $magento_plan->login_id = $response['vendor_id'];
                        $magento_plan->membership_plan = $response['membership'];
                        $magento_plan->save();


                        return ['success' => true, 'errors' => $errors, 'message' => $message];
//                    }else{
//                        return ['success' => false, 'errors' => $errors, 'message' => 'This Magento Store Already connected with another shopping store'];
//                    }
                  //  }




                }
                else {
                    return ['success' => false, 'errors' => $errors, 'message' => 'Invalid login or password.'];

                }


        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
            return ['success' => false, 'errors' => $errors];
        }
    }

}
