<?php

namespace App\Http\Controllers;

use App\Models\Collections;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;
use Carbon\Carbon;
use DB;
use App\Jobs\AddCollectionsJob;
use App\Jobs\DeleteCollectionsJob;
use App\Jobs\AddProductJob;
use App\Traits\MagentoExportInfoTrait;

class CollectionsController extends Controller
{

    use MagentoExportInfoTrait;

    public function getCollections(Request $request){
        try{

            $shop = Auth::user();
            $shopURL = 'https://' . $shop->getDomain()->toNative();


            $membership = null;
            $membership =  $this->getMembership($shop->id);

            //$MagentoStoreID = getMagentoStoreID($membership);


            $exist_collections_in_shopify = [0];

            $collections_items = [];

            $myCollection = DB::table('collections')->pluck('magento_collection_id')->toArray();

            if($myCollection){
                $exist_collections_in_shopify = $myCollection;
            }

            $category_from_api = $this->getIntegrationCategory();

            if($category_from_api) {
                $APIcategory = array_column($category_from_api, 'id');

                $collections = DB::table('magento_catalog_category_entity as cat')
                    ->join('magento_catalog_category_entity_varchar as cv', 'cat.entity_id', '=', 'cv.entity_id')
                    ->join('magento_eav_attribute as att', 'att.attribute_id', '=', 'cv.attribute_id')
                    ->join('magento_eav_entity_type as aty', 'att.entity_type_id', '=', 'aty.entity_type_id')
                    ->select('cat.*', 'cv.value as category_name')
                    ->where('aty.entity_type_code', '=', 'catalog_category')
                    ->where('att.attribute_code', '=', 'name')
                    ->whereNotNull('cv.value')
                    ->whereIn('cat.entity_id', $APIcategory)
                    ->orderBy('category_name', 'ASC')
                    ->distinct()
                    ->latest()
                    ->paginate(10);


                if($collections){

                    $collectionsList = $collections->getCollection()->toArray();

                    foreach($collectionsList as $key=>$item){

                        $collections_items[$key] = $item;

                        $collections_items[$key]->is_exist_in_shopify = false;

                        $magento_collection_id = $item->entity_id;

                        $shopify_collection = Collections::where('user_id', $shop->id)
                            ->where('magento_collection_id', $magento_collection_id)
                            ->where('is_exist_in_shopify', 1)->first();


                        if( $shopify_collection ){
                            $collections_items[$key]->is_exist_in_shopify = true;
                            $collections_items[$key]->shopify_id = $shopify_collection->shopify_id;;
                            $collections_items[$key]->collection_edit_url =  $shopURL.'/admin/collections/'.$shopify_collection->shopify_id;
                        }
                    }

                    $data['pagination'] = [
                        'previousPageUrl' => $collections->previousPageUrl(),
                        'nextPageUrl' => $collections->nextPageUrl(),
                    ];

                }

            }


            $data['shop']['url'] = $shopURL;
            $data['collections'] = $collections_items;

            //  return ['success' => true, 'data' => $data];

            return  Response::json(['success' => true,"data" => $data,'message' => 'Collections Retrived successfully.'], 200);

        }catch( \Exception $e ){
            return [ 'success' => false, "data" => [], 'message' => $e->getMessage()];
        }
    }

    public function addCollections(Request $request){
        try{
            $data = [];

            $shop = Auth::user();
            $shopURL = 'https://' . $shop->getDomain()->toNative();

            $categoryInfo = json_decode($request->post('category_info'));
            $is_sync_action_of_products = $request->post('is_sync_action_of_products');

            logger("is_sync_action_of_products. => ". $is_sync_action_of_products);

            if($is_sync_action_of_products) {
                $this->SyncCategoryProductsAction($shop->id,$categoryInfo);
            }else{
                AddCollectionsJob::dispatch($shop->id, $categoryInfo);
            }

            $jobmsg = 'Some collections are being pushed in the background. Check collection in few seconds.';

            return  Response::json(['success' => true,"data" => $data,'message' => $jobmsg], 200);

        }catch( \Exception $e ){
            return [ 'success' => false, 'message' => $e->getMessage()];
        }
    }


    public function SyncCategoryProducts(Request $request){
        try{
            $data = [];

            $shop = Auth::user();
            $shopURL = 'https://' . $shop->getDomain()->toNative();

            $category_ids[] = $request->post('category_id');

            $this->SyncCategoryProductsAction($shop->id,$category_ids);

            $jobmsg = 'Some products are being pushed in the background. Check collection in few seconds.';

            return  Response::json(['success' => true,"data" => $data,'message' => $jobmsg], 200);

        }catch( \Exception $e ){
            return [ 'success' => false, 'message' => $e->getMessage()];
        }
    }

    public function SyncCategoryProductsAction($shop_id,$category_ids){
        try{
            $data = [];

            logger("shop_id ".$shop_id);
            logger("=====Sync-category_id=====  ".json_encode($category_ids));

            $category_products  = DB::table('magento_catalog_category_product')->select('product_id')->whereIn('category_id',$category_ids)->pluck('product_id')->toArray();

            $sync_product = [];
            if(count($category_products)>0) {

                foreach($category_products as $key=>$product_id){

                    $myProduct = DB::table('products')->where('magento_product_id', $product_id)->first();

                    if (!$myProduct) {
                        $sync_product[] = $product_id;
                    }
                }

                if(count($sync_product) > 0) {

                    $productInfo["product_info"] = $sync_product;
                    $productInfo["auto_update"]  = [];

                    AddProductJob::dispatch($shop_id, $productInfo);
                }
            }else{
                AddCollectionsJob::dispatch($shop_id, $category_ids);
            }

            //SynCollectionProductsJob::dispatch($shop->id, $category_id);

            $jobmsg = 'Some products are being pushed in the background. Check collection in few seconds.';

            return  Response::json(['success' => true,"data" => $data,'message' => $jobmsg], 200);

        }catch( \Exception $e ){
            return [ 'success' => false, 'message' => $e->getMessage()];
        }
    }

    public function deleteCollection(Request $request){
        try{
            $data = [];

            $shop = Auth::user();
            $shopURL = 'https://' . $shop->getDomain()->toNative();

            $collection_ids[] = $request->post('collection_id');
            $is_collection_delete_with_products = $request->post('is_collection_delete_with_products');

            logger("====delete collection param====");
            logger(json_encode($request->all()));

            $categoryInfo = [
                "collection_ids" =>$collection_ids,
                "is_collection_delete_with_products" => $is_collection_delete_with_products ? "true" : "false"
            ];

            if($is_collection_delete_with_products) {
                DeleteCollectionsJob::dispatch($shop->id, $categoryInfo);
                $jobmsg = 'Some collection products are being deleted in the background. Check collection in few seconds.';
            }else{
                DeleteCollectionsJob::dispatch($shop->id, $categoryInfo);
                $jobmsg = 'Collection is being deleted in the background. Check collection in few seconds.';
            }

            return  Response::json(['success' => true,"data" => $data,'message' => $jobmsg], 200);

        }catch( \Exception $e ){
            return [ 'success' => false, 'message' => $e->getMessage()];
        }
    }

}
