<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use DB;
use Illuminate\Http\Request;
use Exception;
use App\Models\MagentoUsersPlans;
use App\Jobs\DeleteProducsPreviousPlanAPIJob;

class ApiController extends Controller
{

    public function testAPI()
    {

        $data = [];

        return response()->json(["success" => true, "data" => $data, "message" => "test records successfully fetched."],
            200);

    }


    public function deleteProduct(Request $request)
    {

        try {

            logger('=============== START:: deleteProduct :: API =============');

            $productID = $request->post('product_id');

            if (!preg_match('~^\d+$~', $productID)) {
                return response()->json([
                    'success' => false,
                    "message" => "Invalid product id"
                ], 404);
            }

            $productRec = DB::table('products')->where('magento_product_id', $productID)->first();

            if (!$productRec) {
                return response()->json([
                    'success' => false,
                    "message" => "Product id $productID was not found in the database."
                ], 404);
            }

            $user_id = $productRec->user_id;

            $shop = User::where('id', $user_id)->first();

            $variantRecs = DB::table('variants')->where('product_id', $productRec->id)->get();

            // Delete attributes
            foreach ($variantRecs as $vRec) {
                DB::table('attributes')->where('variant_id', $vRec->id)->delete();
            }

            // Delete variants
            DB::table('variants')->where('product_id', $productRec->id)->delete();

            // Delete images
            DB::table('images')->where('product_id', $productRec->shopify_id)->delete();

            // Delete tags
            DB::table('tags')->where('product_id', $productRec->id)->delete();

            // Delete product rec
            DB::table('products')->where('id', $productRec->id)->delete();

            // Delete product from Shopify
            // TODO: Verify deletion, throw exception if unsuccessful
            if ($productRec->shopify_id) {
                $productReq = $shop->api()->rest('DELETE',
                    '/admin/api/'.env('SHOPIFY_API_VERSION').'/products/'.$productRec->shopify_id.'.json');
            }

            return response()->json([
                'success' => true,
                "message" => "Product has been deleted successfully."
            ], 200);

        } catch (Exception $e) {
            logger('=============== Error:: deleteProduct :: API =============');
            return response()->json([
                "success" => false,
                "message" => $e->getMessage()
            ], 422);
        }

    }


    public function deleteProductPreviousPlan(Request $request)
    {

        try{

            logger('=============== START:: deleteProductPreviousPlan :: API =============');

            $login_id = $request->post('vendor_id');
            $previous_plan = $request->post('previous_plan');
            $current_plan = $request->post('current_plan');

            $errors = [];

            if (Empty($login_id)) {
                    $errors[] =  "vendor_id is required";
            }


            if (Empty($previous_plan)) {
                $errors[] = "previous_plan is required";
            }


            if (Empty($current_plan)) {
                $errors[] = "current_plan is required";
            }


            if(count($errors) > 0) {
                return response()->json([
                    'success' => false,
                    "message" => $errors
                ], 404);

            }else{

                $magento_plan = MagentoUsersPlans::where('login_id', $login_id)->first();

               if($magento_plan){

                    if($magento_plan->membership_plan == $previous_plan) {

                        $shop_id = $magento_plan->user_id;

                        DeleteProducsPreviousPlanAPIJob::dispatchNow($shop_id,$current_plan);

                        MagentoUsersPlans::where('login_id', $login_id)->update([
                            "membership_plan" => $current_plan
                        ]);

                        $jobmsg = 'Some products are being deleted in the background and plan updated successfully.';

                        return  response()->json(['success' => true,'message' => $jobmsg], 200);

                   }else{

                     return response()->json([
                            'success' => false,
                            "message" => "previous_plan was not found in the database."
                        ], 404);
                   }


                }else{

                    return response()->json([
                        'success' => false,
                        "message" => "vendor_id $login_id was not found in the database."
                    ], 404);

                }

            }

        }catch(Exception $e){
            logger('=============== ERROR:: deleteProductPreviousPlan :: API =============');
            return response()->json([
                'success' => false,
                "message" => $e->getMessage()
            ],422);
        }

    }


}
