<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Jobs\GetShopDataJob;
use App\Jobs\OrdersCreateJob;
use App\Jobs\AppUninstalledJob;
use App\Jobs\ProductsDeleteJob;
use App\Jobs\ProductsUpdateJob;
use Auth;
use Osiset\ShopifyApp\Contracts\Objects\Values\ShopDomain;
use Osiset\ShopifyApp\Contracts\Objects\Values\ShopDomain as ShopDomainValue;
use Symfony\Component\Intl\Currencies;
use DB;
use App\Traits\MagentoExportInfoTrait;
use App\Jobs\CopyTablesFromMagentoJob;
use Artisan;

class TestController extends Controller
{

    use MagentoExportInfoTrait;

    public function testModeIndex(Request $request){

      logger("===============STRAT::TEST::MODE======================");

        $shop = Auth::user();


        $IntegrationInventory = $this->getIntegrationInventory();

      //  dd($IntegrationInventory);

        $entity_ids = array_column($IntegrationInventory, 'product_id');


//        $magentoProductRecord = $this->getMagentoProductRecords();
//
//        dd($magentoProductRecord);

        //logger("Shop::Test");
      //  logger($shop);

       // webhooksList($shop->id); // Check list of App - Webhook

      //  $domain = 'crawlapps-satish.myshopify.com';


       // ProductsDeleteJob::dispatch($domain,[]);
       // ProductsUpdateJob::dispatch($domain, []);




      //  $source_currency = "USD";
      //  $from_currency_symbol = Currencies::getSymbol($source_currency);


       // logger("source_currency_symobl");
       // logger($from_currency_symbol);


      //  dd('1111');



       // AppUninstalledJob::dispatch($domain, []);



        //GetShopDataJob::dispatch($shop->id);
       // OrdersCreateJob::dispatch($shop->name,$shop);

        //$shope_data = getShopData($shop->id);

       // logger("===HElPER::Shop-Data");
       // logger(json_encode($shope_data));

       //  return view("app_test_mode");

        return 0;

        logger("===============END::TEST::MODE======================");

    }


    public function CopyMagentoTable(Request $request){

        logger("===============STRAT::TEST::MODE======================");


        //dd("1");

      //  $IntegrationProduct = $this->getIntegrationProduct();
       // $IntegrationInventory = $this->getIntegrationInventory();

       //  dd($IntegrationInventory);


        CopyTablesFromMagentoJob::dispatchNow();


        return 0;

        logger("===============END::TEST::MODE======================");

    }



    public function DeleteMemberShipProductCron(Request $request){

        logger("===============STRAT::TEST::MODE======================");

            Artisan::call('auto-delete:membership-products');

            return 0;

        logger("===============END::TEST::MODE======================");

    }


    public function testConnectDB2(Request $request){

        logger("===============STRAT::TEST::MODE testConnectDB2======================");


         $url = $this->checkAPI();
        dd($url);


                $mysql2 = mysql2Con();



                $queryReq = $mysql2->table("eav_attribute");
                $records = $queryReq->get()->first();

                dd($records);


            return 0;

        logger("===============END::TEST::MODE testConnectDB2======================");

    }




}
