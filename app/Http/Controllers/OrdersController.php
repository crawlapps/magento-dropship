<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\LineItem;
use App\Models\OrdersTrackingInfo;
use Illuminate\Support\Facades\Auth;
use Response;
use App\Http\Resources\PrintOrderPDFResource;
use App\Http\Resources\TestResource;
use Carbon\Carbon;
use PDF;
use DB;
use App\Traits\MagentoExportInfoTrait;
use App\Traits\ShopifyTrait;
use Symfony\Component\Intl\Currencies;

class OrdersController extends Controller
{

     use MagentoExportInfoTrait;
     use ShopifyTrait;

    public function getOrders(Request $request){
        try{

            logger("====getOrders call====");
            logger(json_encode($request->all()));

            $shop = Auth::user();

            $input = $request->all();

            $filterData = $input['filterData'];

           // webhooksList($shop['id']);

            $shopURL = 'https://' . $shop->getDomain()->toNative();


            $orderRecsQuery = Order::with(['LineItems','LineItems.Images'])->where('user_id', $shop->id);

            //filter For product type (Brand)
            if(isset($filterData['filterByType']) && $filterData['filterByType']!=="" && $filterData['filterByType'] !==null){
                $filter_by_type = $filterData['filterByType'];
                $orderRecsQuery->where('fulfillment_status', $filter_by_type);
            }

            $orders = $orderRecsQuery->orderBy('id', 'desc')->paginate(10);

            $data['pagination'] = [
                        'previousPageUrl' => $orders->previousPageUrl(),
                        'nextPageUrl' => $orders->nextPageUrl(),
                ];

           // $data['orders'] = $orders;
            $data['shop']['url'] = $shopURL;
            $data['orders'] = $orders->getCollection();


          //  return ['success' => true, 'data' => $data];

            return  Response::json(['success' => true,"data" => $data,'message' => 'Order Retrived successfully.'], 200);

        }catch( \Exception $e ){
                return [ 'success' => false, 'message' => $e->getMessage()];
        }
    }



    public function getOrdersDetails(Request $request,$id=null){
        try{

            logger("====getOrders call==== ". $id);
            logger(json_encode($request->all()));

            $shop = Auth::user();
            $shopURL = 'https://' . $shop->getDomain()->toNative();

            $orderRecsQuery = Order::with(['LineItems','LineItems.Images'])->where('id', $id)->where('user_id', $shop->id);
            $order = $orderRecsQuery->orderBy('id', 'desc')->first();

            if($order) {
                $order_detail_res = $this->getSingleOrder($shop->id, $order->shopify_order_id);

            if($order_detail_res['success']){

             $order_detail = $order_detail_res['data'];


                $tracking_data = [];

                if($order->is_tracked==1) {
                    $tracking_data = $this->getTrackingInfoData($id);
                }

//            $LineItems = DB::table('line_items')
//                ->join('products', 'line_items.product_id', '=', 'products.shopify_id')
//                ->select('products.source_url','line_items.*')
//                ->where('line_items.db_order_id','=',$id)
//                ->get();

                $LineItems = $order_detail->line_items;
                $line_items = [];

                foreach($LineItems as $key=>$item){
                    $line_items[$key] = $item;
                    $line_items[$key]['is_app_product'] = false;
                    $product_id = $item['product_id'];

                    $exist_product = Product::where('user_id', $shop->id)->where('shopify_id', $product_id)->first();

                    $line_items[$key]['image'] = null;
                    $images = $this->getProductImage($shop->id,$product_id);
                    if(count($images) > 0) {
                        $line_items[$key]['image'] = $images[0]['src'];
                    }

                    $line_items[$key]['edit_url'] =  $shopURL.'/admin/orders/'.$order->shopify_order_id;
                    $line_items[$key]['product_edit_url'] =  $shopURL.'/admin/products/'.$product_id;
                    $line_items[$key]['total_price'] =  $item['price'] * $item['quantity'];

                    if( $exist_product ){
                        $line_items[$key]['is_app_product'] = true;
                        $line_items[$key]['source_url'] = $exist_product->source_url;
                    }
                }

                $shope_data = getShopData($shop->id);

                if(!$shope_data){
                    $shope_data = getShopDataFromAPI($shop, 'id, email, name,currency');
                }

                if($shope_data->currency){
                    $currency = Currencies::getSymbol($shope_data->currency);
                }

                $order_date =  Carbon::parse($order_detail->created_at)->format('d F Y');
                $order_date_time =  Carbon::parse($order_detail->created_at)->format('h:m a');

                $data['order'] = $order;
                $data['order_detail'] = $order_detail;
                $data['order_date'] = $order_date;
                $data['order_time'] = $order_date_time;
                $data['tracking_data'] = $tracking_data;
                $data['line_items'] = $line_items;
                $data['shop_url'] = $shopURL;
                //  return ['success' => true, 'data' => $data];

                return Response::json([
                    'success' => true, "data" => $data,'currency'=> $currency, 'message' => 'Order Details Retrived successfully.'
                ], 200);

            }
            else{
                return Response::json([
                    'success' => false, "data" => [], 'message' => 'Order details not found.'
                ], 200);
            }
        }
            else{
                return Response::json([
                    'success' => false, "data" => [], 'message' => 'Order details not found.'
                ], 200);
            }


        }catch( \Exception $e ){
            return [ 'success' => false, 'message' => $e->getMessage()];
        }
    }



    public function assignOrderNumber(Request $request){
        try{

            $shop = Auth::user();

            $db_order_id = $request->db_order_id;

            $order_number = $request->order_number;


            logger("Assign Order ::Input");
            logger("db_order_id :: ".$db_order_id);
            logger("order_number :: ".$order_number);

          $orderReq = $this->getIntegrationAssignOrder($order_number);

          logger("orderReq::RES");
          logger(json_encode($orderReq));

             if($orderReq['status']){

                 $isUsedOrderNumber = DB::table('orders')->where(["order_number"=>$order_number])->first();

                 if(!$isUsedOrderNumber){


                 $exist_assign = DB::table('orders')->where(["id"=>$db_order_id,"order_number"=>$order_number])->first();


                 if(!$exist_assign){

                     $res = $orderReq['data'];
                     $items =  $res['items'];

                     if(count($items) > 0){


                         $LineItemProducts = [];
                         $LineItemProducts_shopify = LineItem::where('db_order_id',$db_order_id)->pluck('product_id')->toArray();

                         if(count($LineItemProducts_shopify) > 0) {

                                     $LineItemProducts_find = DB::table('products')->whereIn('shopify_id',$LineItemProducts_shopify)->pluck('id')->toArray();
                                     $LineItemProducts = DB::table('variants')->whereIn('product_id',$LineItemProducts_find)->pluck('source_id')->toArray();
                         }

                         $status = 0;

                         if(count($items) > 0 && count($LineItemProducts)>0){
                             foreach($items as $item){
                                 if(in_array($item['prod_id'],$LineItemProducts)){
                                     $status = 1;
                                     break;
                                 }
                             }
                         }

                         if($status==0) {
                             return Response::json([
                                 'success' => false,"data"=>$items, "is_show_error" =>true, 'message' => 'Does not match any product for item of this order.'
                             ], 200);
                         }else{
                             return Response::json([
                                 'success' => true, "data"=>$items
                             ], 200);
                         }

                     }else{
                         return  Response::json(['success' => false,"data"=>[], 'message' => 'Items does not found for this order number'], 200);
                     }

                 }else{
                     return  Response::json(['success' => false, "data"=>[],  'message' => 'This order is already assign.'], 200);
                 }
                 }else{
                     return  Response::json(['success' => false, "data"=>[],  'message' => 'This order number has been used by vendor successfully it cannot be used again'], 200);
                 }

             }else{
                 return  Response::json(['success' => false,"data"=>[], 'message' => $orderReq['error']], 200);
             }

        }catch( \Exception $e ){
            return [ 'success' => false, "data"=>[],  'message' => $e->getMessage()];
        }
    }



    public function assignOrderNumberConfirm(Request $request){
        try{

            $shop = Auth::user();

            $db_order_id = $request->db_order_id;
            $order_number = $request->order_number;

            DB::table('orders')->where('id',$db_order_id)->update(["fulfillment_status"=>"fulfilled","order_number"=>$order_number,"is_assigned"=>1]);

            return Response::json([
                             'success' => true, 'message' => 'Order assign successfully.'
                         ], 200);


        }catch( \Exception $e ){
            return [ 'success' => false, 'message' => $e->getMessage()];
        }
    }


    public function findTrackingInfo(Request $request){
        try{

            $shop = Auth::user();

            $db_order_id = $request->db_order_id;
            $order_number = $request->order_number;

            $orderReq = $this->getIntegrationAssignOrder($order_number);

            logger("orderTrackReq::RES");
            logger(json_encode($orderReq));

            if($orderReq['status']){

                $order = DB::table('orders')->where(["id"=>$db_order_id,"order_number"=>$order_number])->first();

               // if($order->is_tracked!==1){

                    $shopify_order_id = $order->shopify_order_id;
                    $user_id = $order->user_id;

                    $res = $orderReq['data'];

                    if(isset($res['shipment_details'])) {

                    $shipment_details = $res['shipment_details'];

                    if(count($shipment_details) > 0) {

                        $track_numbers = [];
                        $titles = [];
                        $carrier_codes = [];

                        for($i=0;$i<count($shipment_details);$i++) {

                            if (isset($res['shipment_details'][$i])) {

                                $shipment_detail = $res['shipment_details'][$i];

                                $track_number = $shipment_detail['track_number'];
                                $title = $shipment_detail['title'];
                                $carrier_code = $shipment_detail['carrier_code'];

                                $track_numbers[] = $track_number;
                                $titles[] = $title;
                                $carrier_codes[] = $carrier_code;
                            }
                        }

                   $trackingInfo = [
                                    'title' => $titles,
                                    'track_number' => $track_numbers,
                                    'status' => "fulfilled",
                                    "carrier_code" => $carrier_codes
                                ];

                        $fulfillments_status = $this->updateShopifyOrder($shopify_order_id,$db_order_id,$user_id,
                                    $trackingInfo);

                        logger("*fulfillments_status*");
                        logger($fulfillments_status);

                           if ($fulfillments_status == "true") {

                                     for($i=0;$i<count($shipment_details);$i++) {
                                        $shipment_detail = $res['shipment_details'][$i];
                                         $track_number = $shipment_detail['track_number'];
                                         $title = $shipment_detail['title'];
                                         $carrier_code = $shipment_detail['carrier_code'];

                                     $db_data = [
                                        'db_order_id' => $db_order_id,
                                        'track_order_id' => $shipment_detail['order_id'],
                                        'track_number' => $track_number,
                                        'title' => $title,
                                        'carrier_code' => $carrier_code,
                                        'tracking_infos_more' => json_encode($res),
                                    ];

                                    OrdersTrackingInfo::updateOrCreate(['db_order_id' => $db_order_id, 'track_number' => $track_number],$db_data);
                                }

                            DB::table('orders')->where('id', $db_order_id)->update(["is_tracked" => 1]);
                        }



                        return Response::json([
                            'success' => true, 'message' => 'order successfully tracked.'
                        ], 200);

                    }else{
                        return  Response::json(['success' => false, "data"=>[],  'message' => 'No tracking information found.'], 200);
                    }
                    }else{
                        return  Response::json(['success' => false, "data"=>[],  'message' => 'No tracking information found.'], 200);
                    }

//                }else{
//                    return  Response::json(['success' => false, "data"=>[],  'message' => 'This order is already Tracked.'], 200);
//                }

            }else{
                return  Response::json(['success' => false,"data"=>[], 'message' => $orderReq['error']], 200);
            }


        }catch( \Exception $e ){
            return [ 'success' => false, 'message' => $e->getMessage()];
        }
    }


    public function getTrackingInfo(Request $request){
        try{

            $shop = Auth::user();

            $db_order_id = $request->db_order_id;
            $order_number = $request->order_number;

            $data = OrdersTrackingInfo::where('db_order_id',$db_order_id)->get();

            if($data) {

                return Response::json([
                    'success' => true, "data" => $data
                ], 200);
            }else{
                return  Response::json(['success' => false, "data"=>[],  'message' => 'No tracking information found.'], 200);
            }


        }catch( \Exception $e ){
            return [ 'success' => false, 'message' => $e->getMessage()];
        }
    }

    public function getTrackingInfoData($db_order_id){
        try{

            $shop = Auth::user();

            $data = OrdersTrackingInfo::where('db_order_id',$db_order_id)->get();

            return $data;

        }catch( \Exception $e ){
            return [ 'success' => false, 'message' => $e->getMessage()];
        }
    }


}
