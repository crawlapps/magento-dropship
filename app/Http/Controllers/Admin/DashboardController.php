<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use Auth;
use Response;
class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function Dashboard()
    {

        $admin = Auth::guard('admin')->user();
        //logger("=============ADMIN DETAILS==========");
        // logger($admin);

        if(Auth::guard('admin')->check()) // this means that the admin was logged in.
        {
            // logger("admin is");
            // logger("==Admin are able to Dashboard==");
            try{
                $total_products = Product::count();

               return view('admin.home',['total_products'=>$total_products]);

            }catch( \Exception $e ){
                return redirect()->route('admin.dashboard')
                    ->with('error', $e->getMessage());

            }

        }else{
            // logger("admin is not login");
            return redirect('/login');
        }

    }

}
