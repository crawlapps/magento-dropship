<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Variant as VariantModel;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Models\Product;
use DB;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Intl\Currencies;

class ProductsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function index()
    {

        return view('admin.products.list');

     try {

         $productRecsQuery = DB::table('products')
                                                 //->selectRaw('count(user_id) as store_count')
                                                // ->where('magento_product_id',5085)
                                              // ->selectRaw("count('user_id') as store_count")
                                                 //->select(array('*', DB::raw('COUNT((user_id)) as store_count')));
                                                 //->groupBy('user_id');
                                                  // ->distinct('user_id');                                                 // ->groupBy('user_id');
                                             // ->groupBy('magento_product_id');

                                                 ->select("*", DB::raw("count(DISTINCT (user_id)) as user_count"))                                              
                                                 ->groupBy('magento_product_id');


         $productRecs  =  $productRecsQuery->paginate(10);

    //dd($productRecs);


         $products = [];

         if(count($productRecs)>0) {

             for ($p = 0; $p < count($productRecs); $p++) {

                $magento_product_id = $productRecs[$p]->magento_product_id;

                $productRecsQuery_second = DB::table('products')->where('magento_product_id',$magento_product_id)->select("*")->groupBy('user_id')->get();  
                          
                $deactive_store_count = 0;   
                $active_store_count = 0;       
                foreach ($productRecsQuery_second as $key => $value) {

                     $shop = User::where('id', $value->user_id)->withTrashed()->first();

                      if(@$shop->deleted_at) {
                             $deactive_store_count =  $deactive_store_count + 1;
                         }
                         else {
                             $active_store_count =  $active_store_count + 1;
                         }

                }

                     // Get product variants
                     $updatedAt = date('F/j/Y h:i A', strtotime($productRecs[$p]->updated_at));
                     $reviewCount = count(json_decode($productRecs[$p]->reviews, true));
                     $db_variants = DB::table('variants')->where('product_id', $productRecs[$p]->id)->first();
                     $db_variants_count = DB::table('variants')->where('product_id', $productRecs[$p]->id)->count();
                     if($db_variants) {
                         $db_image = DB::table('images')->select('url')->where('id', $db_variants->image_id)->first();
                     }
                     $image_url = (@$db_image) ? $db_image->url : '';

                     $db_variants_title = (@$db_variants) ? $db_variants->title : '';
                     $db_variants_shipping_type = (@$db_variants) ? $db_variants->shipping_type : '';
                     $db_variants_source_price = (@$db_variants) ? $db_variants->source_price : '';
                     $db_variants_shopify_price = (@$db_variants) ? $db_variants->shopify_price : '';

                         logger("else :: magento_product_id  => ".$productRecs[$p]->magento_product_id);

                         $products[] = [
                             'id' => $productRecs[$p]->id,
                             'magento_product_id' => $productRecs[$p]->magento_product_id,
                             'shopify_id' => $productRecs[$p]->shopify_id,
                             'type' => $productRecs[$p]->type,
                             'title' => (@$productRecs[$p]->title) ? $productRecs[$p]->title : $db_variants_title,
                             'source' => $productRecs[$p]->source,
                             'source_url' => $productRecs[$p]->source_url,
                             'source_currency' => $productRecs[$p]->source_currency,
                             'source_currency_symbol' => $productRecs[$p]->source_currency_symbol,
                             'locale' => $productRecs[$p]->locale,
                             'shipping_type' => (@$productRecs[$p]->shipping_type) ? $productRecs[$p]->shipping_type : $db_variants_shipping_type,
                             'updated_at' => $updatedAt,
                             'source_prices' => $db_variants_source_price,
                             'variants_count' => (@$db_variants_count) ? $db_variants_count : 0,
                             'image_url' => $image_url,
                             'product_url' => '/products/'.$productRecs[$p]->shopify_handle,
                             'edit_url' => '/admin/products/'.$productRecs[$p]->shopify_id,
                             'shopify_price' => $db_variants_shopify_price,
                             'review_count' => $reviewCount,
                             'is_auto_update_inventory' => $productRecs[$p]->is_auto_update_inventory,
                             'active_store_count' => $active_store_count,
                             'deactive_store_count' => $deactive_store_count
                         ];

                     
                     logger("products========". json_encode($products));
               

             }
         }
               
         
         $data['products'] = $products;
         $data['productRecs'] = $productRecs;

         return view('admin.products.index',$data);


     }catch(\Exception $e){
         return [ 'success' => false, "data" => [], 'message' => $e->getMessage()];
     }

    }

    public function getProducts(Request $request){
        try {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length");  // total number of rows per page
        $columnIndex_arr = $request->get('order');

        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value

       
        $totalRecords = DB::table('products')->select('count(*) as allcount')->distinct('magento_product_id')->count('magento_product_id');
     
        $totalRecordswithFilter_query = DB::table('products')->select('count(*) as allcount')->distinct('magento_product_id');

        $totalRecordswithFilter_query  = $totalRecordswithFilter_query->where('products.shopify_handle', 'like', '%' . $searchValue . '%')->orWhere('products.type', 'like', '%' . $searchValue . '%');

        $totalRecordswithFilter = $totalRecordswithFilter_query->count('magento_product_id');


        $productRecs = DB::table('products');

         if($columnName=="shopify_handle"){
            $productRecs = $productRecs->orderBy("shopify_handle", $columnSortOrder);
         }

         if($columnName=="type"){
            $productRecs = $productRecs->orderBy("type", $columnSortOrder);
         }

         $productRecs = $productRecs->where('products.shopify_handle', 'like', '%' . $searchValue . '%')->orWhere('products.type', 'like', '%' . $searchValue . '%')             
        ->select("*", DB::raw("count(DISTINCT (user_id)) as user_count"))                                              
        ->groupBy('magento_product_id')
        ->skip($start)
        ->take($rowperpage)
        ->get();

        $data_arr = array();

        if(count($productRecs)>0) {

            for ($p = 0; $p < count($productRecs); $p++) {

               $magento_product_id = $productRecs[$p]->magento_product_id;

               $productRecsQuery_second = DB::table('products')->where('magento_product_id',$magento_product_id)->select("*")->groupBy('user_id')->get();  
                         
               $deactive_store_count = 0;   
               $active_store_count = 0;       
               foreach ($productRecsQuery_second as $key => $value) {

                    $shop = User::where('id', $value->user_id)->withTrashed()->first();

                     if(@$shop->deleted_at) {
                            $deactive_store_count =  $deactive_store_count + 1;
                        }
                        else {
                            $active_store_count =  $active_store_count + 1;
                        }

               }

                    // Get product variants
                    $updatedAt = date('F/j/Y h:i A', strtotime($productRecs[$p]->updated_at));
                    $reviewCount = count(json_decode($productRecs[$p]->reviews, true));
                    $db_variants = DB::table('variants')->where('product_id', $productRecs[$p]->id)->first();
                    $db_variants_count = DB::table('variants')->where('product_id', $productRecs[$p]->id)->count();
                    if($db_variants) {
                        $db_image = DB::table('images')->select('url')->where('id', $db_variants->image_id)->first();
                    }
                    $image_url = (@$db_image) ? $db_image->url : '';

                    $db_variants_title = (@$db_variants) ? $db_variants->title : '';
                    $db_variants_shipping_type = (@$db_variants) ? $db_variants->shipping_type : '';
                    $db_variants_source_price = (@$db_variants) ? $db_variants->source_price : '';
                    $db_variants_shopify_price = (@$db_variants) ? $db_variants->shopify_price : '';

                      //  logger("else :: magento_product_id  => ".$productRecs[$p]->magento_product_id);

                        $data_arr[] = [
                            'id' => $productRecs[$p]->id,
                            'magento_product_id' => $productRecs[$p]->magento_product_id,
                            'shopify_id' => $productRecs[$p]->shopify_id,
                            'type' => $productRecs[$p]->type,
                            'title' => (@$productRecs[$p]->title) ? $productRecs[$p]->title : $db_variants_title,
                            'source' => $productRecs[$p]->source,
                            'source_url' => $productRecs[$p]->source_url,
                            'source_currency' => $productRecs[$p]->source_currency,
                            'source_currency_symbol' => $productRecs[$p]->source_currency_symbol,
                            'locale' => $productRecs[$p]->locale,
                            'shipping_type' => (@$productRecs[$p]->shipping_type) ? $productRecs[$p]->shipping_type : $db_variants_shipping_type,
                            'updated_at' => $updatedAt,
                            'source_prices' => $db_variants_source_price,
                            'variants_count' => (@$db_variants_count) ? $db_variants_count : 0,
                            'image_url' => $image_url,
                            'product_url' => '/products/'.$productRecs[$p]->shopify_handle,
                            'edit_url' => '/admin/products/'.$productRecs[$p]->shopify_id,
                            'shopify_price' => $db_variants_shopify_price,
                            'review_count' => $reviewCount,
                            'is_auto_update_inventory' => $productRecs[$p]->is_auto_update_inventory,
                            'active_store_count' => $active_store_count,
                            'deactive_store_count' => $deactive_store_count
                        ];   

            }
        }

        $active_store_count_sort_type = "asc";      
        if($columnName=="active_store_count"){
            $active_store_count_sort_type  = $columnSortOrder;
            if($active_store_count_sort_type == "desc"){
                array_multisort(array_map(function($element) {
                    return $element['active_store_count'];
                }, $data_arr), SORT_DESC, $data_arr);    
            }else{
             array_multisort(array_map(function($element) {
                        return $element['active_store_count'];
                    }, $data_arr), SORT_ASC, $data_arr);    
                }           

         }

         $deactive_store_count_sort_type  = "asc";
         if($columnName=="deactive_store_count"){
            $deactive_store_count_sort_type  = $columnSortOrder;
            if($deactive_store_count_sort_type == "desc"){
                array_multisort(array_map(function($element) {
                    return $element['deactive_store_count'];
                }, $data_arr), SORT_DESC, $data_arr);    
            }  else{
                array_multisort(array_map(function($element) {
                    return $element['deactive_store_count'];
                }, $data_arr), SORT_ASC, $data_arr);    
            }
         }
        

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data_arr,
        );

        echo json_encode($response);

        }catch(\Exception $e){
            return [ 'success' => false, "data" => [], 'message' => $e->getMessage()];
        }

    }

    public function getMyProductsVariants(Request $request,$product_id){

        try {

            $input = $request->all();
            $product_id = $product_id;
            $variantsInfo = [];

            $products = DB::table('products')->select('*')->where('id', $product_id)->first();

            if($products) {
                $variants = VariantModel::with(['Attribute'])->where('product_id', $product_id)->orderBy('id',
                    'DESC')->get()->toArray();


                if (count($variants) > 0) {
                    for ($v = 0; $v < count($variants); $v++) {

                        $db_image = DB::table('images')->select('url')->where('id', $variants[$v]['image_id'])->first();
                        $image_url = ($db_image) ? $db_image->url : '';
                        $variantsInfo[$v] = $variants[$v];
                        $variantsInfo[$v]['image_url'] = $image_url;

                    }
                }


                $shop = User::where('id', $products->user_id)->withTrashed()->first();

                $shope_data = getShopData($shop->id);

                 if(!$shope_data || $shope_data!==null){
                    $shope_data = getShopDataFromAPI($shop, 'id, email, name,currency');
                 }

                $currency = '';

                if($shope_data && $shope_data->currency){
                    $currency = Currencies::getSymbol($shope_data->currency);
                }

                if($shope_data && $shope_data->currency){
                    $currency = Currencies::getSymbol($shope_data->currency);
                }

                  $data['currency'] = $currency;
                  $data['source_currency'] = $products->source_currency;
                  $data['source_currency_symbol'] = $products->source_currency_symbol;
                  $data['locale'] = $products->locale;

            }

                $data['variants'] = $variantsInfo;


                return view('admin.products.variants', $data);


        } catch (QueryException $e) {
            $errors[] = $e->getMessage();
            return ['success' => false, 'errors' => $errors];
        }
    }


}
