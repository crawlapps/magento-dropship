<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

use App\Events\CheckOrder;
use App\Listeners\OrderCreateUpdate;


class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        CheckOrder::class => [
            OrderCreateUpdate::class,
        ],
//        'App\Events\CheckOrder' => [
//            'App\Listeners\OrderCreateUpdate',
//        ],
        'App\Events\CheckCollections' => [
            'App\Listeners\CollectionsCreateUpdate',
        ]

    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
