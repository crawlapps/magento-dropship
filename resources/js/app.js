/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.Vue = require('vue').default;

import PolarisVue from '@hulkapps/polaris-vue';
import '@hulkapps/polaris-vue/dist/polaris-vue.css';
Vue.use(PolarisVue);

import VueNumberFormat from 'vue-number-format';
Vue.use(VueNumberFormat, {prefix: 'R$ ', decimal: ',', thousand: '.'});


import App from './components/layouts/App.vue';
import router from './routes/index.js';



/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

 // Vue.component('example-component', require('./components/ExampleComponent.vue').default);
 // Vue.component('welcome', require('./components/Welcome.vue').default);
 // Vue.component('test-demo', require('./components/TestDemo.vue').default);
 // Vue.component('dashboard', require('./components/pages/Dashboard.vue').default);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    components: { App },
    router
});
