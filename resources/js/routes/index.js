import VueRouter from 'vue-router';
window.Vue = require('vue').default;
Vue.use(VueRouter);


import Dashboard from '../components/pages/Dashboard.vue';
import OrderDetails from '../components/pages/OrderDetails.vue';
import Home from '../components/Welcome.vue';

const routes = [

    {
        path: '/',
        name: 'dashboard',
        component: Dashboard
    },
    {
        path: '/home',
        name: 'home',
        component: Home
    },
    {
        path: '/order-details',
        name: 'order-details',
        component: OrderDetails
    }
];

const router = new VueRouter({
    mode: 'history',
    routes
})


export default router;
