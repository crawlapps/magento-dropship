@extends('admin.layouts.backend')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2> Variants </h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{ route('admin.products') }}"> Back</a>
                </div>
            </div>
        </div>

        @include('admin.layouts.partials.errors')
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Variants</h6>
            </div>
            <div class="card-body">

                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Image</th>
                            <th>Name</th>
                            <th>SKU</th>
                            <th>Inventory</th>
                            <th>Source Prices</th>
                            <th>Shopify Prices</th>
                            <th>Attribute</th>

                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Image</th>
                            <th>Name</th>
                            <th>SKU</th>
                            <th>Inventory</th>
                            <th>Source Prices</th>
                            <th>Shopify Prices</th>
                            <th>Attribute</th>
                        </tr>
                        </tfoot>
                        <tbody>

                        @if(count($variants)>0)
                            @foreach($variants as $key=>$variant)
                                <tr>
                                    <td>
                                        <img src="{{$variant['image_url']}}" width="40px" alt="{{$variant['title']}}">
                                    </td>
                                    <td>{{$variant['title']}}</td>
                                    <td>{{$variant['sku']}}</td>
                                    <td>{{$variant['inventory_quantity'] > 0 ? "Instock" : "Out of stock" }} </td>
                                    <td>{{$source_currency_symbol}}  {{$variant['source_price']}}</td>
                                    <td>{{$currency}}  {{$variant['shopify_price']}}</td>
                                    <td>
                                        @if($variant['attribute'])

                                        <div>
                                             <h4>{{ $variant['attribute']['dimension'] ? $variant['attribute']['dimension']  : ''}}</h4>
                                        </div>
                                            <div class="Polaris-Stack__Item">
                                                <span class="Polaris-Badge">{{ $variant['attribute']['value'] ? $variant['attribute']['value'] : '' }}
                                                </span>
                                            </div>
                                        @endif
                                    </td>

                                </tr>
                            @endforeach
                        @else

                            <tr>
                                <td colspan="10">No variants have been imported yet...</td>
                            </tr>

                        @endif

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
