@extends('admin.layouts.backend')
@section('content')



    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2> Products </h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{ route('admin.dashboard') }}"> Back</a>
                </div>
            </div>
        </div>

    @include('admin.layouts.partials.errors')
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Products</h6>
            </div>
            <div class="card-body">

                <div class="table-responsive">
                    <table class="table table-bordered"  width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Brand</th>
                            <th>Source</th>
                            <th>Source Prices</th>
                            <th>Shopify Prices</th>
                            <th>Variants</th>
                            <th>Active Store</th>
                            <th>DeActived Store</th>
                            <th>Supplier Link</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Brand</th>
                            <th>Source</th>
                            <th>Source Prices</th>
                            <th>Shopify Prices</th>
                            <th>Variants</th>
                            <th>Active Store</th>
                            <th>Deactivated Store</th>
                            <th>Supplier Link</th>
                        </tr>
                        </tfoot>
                        <tbody>

                        @if(count($products)>0)
                            @foreach($products as $key=>$product)
                                <tr>
                                    <td>
                                        <img src="{{$product['image_url']}}" width="40px" alt="{{$product['title']}}">
                                    </td>
                                    <td>{{$product['title']}}</td>
                                    <td>{{$product['type']}}</td>
                                    <td>{{$product['source']}}</td>
                                    <td>{{$product['source_currency_symbol']}}  {{$product['source_prices']}}</td>
                                    <td>{{$product['locale']}}  {{$product['shopify_price']}}</td>
                                    <td><a href="{{route('admin.products.variants',$product['id'])}}" target="_blank">{{$product['variants_count']}}</a></td>
                                    <td>{{$product['active_store_count']}}</td>
                                    <td>{{$product['deactive_store_count']}}</td>
                                    <td><a href="{{$product['source_url']}}" target="_blank">View Dropship Item</a></td>

                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="10">Does not have any product!</td>
                            </tr>

                        @endif

                        </tbody>
                    </table>

                        <div>
                              {!! $productRecs->links() !!}
                         </div>
                </div>
            </div>
        </div>
    </div>
@endsection
