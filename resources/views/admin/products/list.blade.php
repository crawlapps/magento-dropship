@extends('admin.layouts.backend')
@section('content')


    <!-- Datatables CSS CDN -->
    {{-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css"> --}}

    <!-- jQuery CDN -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Datatables JS CDN -->
    {{-- <script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script> --}}

    <style>
        #productsTable_info {
        display: none;
    }
    </style>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2> Products </h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{ route('admin.dashboard') }}"> Back</a>
                </div>
            </div>
        </div>

    @include('admin.layouts.partials.errors')
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Products</h6>
            </div>
            <div class="card-body">

                <div class="table-responsive">
   
        <table id='productsTable' class="table table-bordered"  width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>Image</th>
                <th>Name</th>
                <th>Brand</th>
                <th>Source</th>
                <th>Source Prices</th>
                <th>Shopify Prices</th>
                <th>Variants</th>
                <th>Active Store</th>
                <th>DeActived Store</th>
                <th>Supplier Link</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>Image</th>
                <th>Name</th>
                <th>Brand</th>
                <th>Source</th>
                <th>Source Prices</th>
                <th>Shopify Prices</th>
                <th>Variants</th>
                <th>Active Store</th>
                <th>Deactivated Store</th>
                <th>Supplier Link</th>
            </tr>
            </tfoot>
    </table>
</div>
</div>
</div>
</div>
    <!-- Script -->
    <script type="text/javascript">
    $(document).ready(function(){

        console.log("Admin :: getProducts");
        
      // DataTable
      $('#productsTable').DataTable({
         processing: true,
         serverSide: true,
         ajax: "{{route('admin.getProducts')}}",
         "order": [[ 8, "desc" ]],
         columns: [
            {
                      data: 'shopify_handle',
                      bSortable: false,
                      mRender: function(data, type, row) {                                               
                          return '<img src="'+row.image_url+'" src="'+row.title+'"  width="40px" class="dataTableImg"/>';
                        }
           },
           {
                      data: 'shopify_handle',
                      bSortable: true,
                      mRender: function(data, type, row) {                                               
                          return row.title;
                        }
           },
           {
                      data: 'type',
                      bSortable: true,
                      mRender: function(data, type, row) {                                               
                          return row.type;
                        }
           },
           {
                      data: 'source',
                      "searchable": false,
                      bSortable: false,
                      mRender: function(data, type, row) {                                               
                          return row.source;
                        }
           },
            
            {
                      data: 'source_prices',
                      "searchable": false,
                      bSortable: false,
                      mRender: function(data, type, row) {                                               
                          return row.source_currency_symbol+" "+row.source_prices; 
                        } 
           },
           {
                      data: 'shopify_price',
                      "searchable": false,
                      bSortable: false,
                      mRender: function(data, type, row) {                                               
                          return row.locale+" "+row.shopify_price; 
                        } 
           },
           {
                      data: 'variants_count',
                      bSortable: false,
                      mRender: function(data, type, row) {                                              
                        
                          return '<a href="{{route("admin.products.variants")}}/'+row.id+'" target="_blank">'+row.variants_count+'</a>';

                        } 
           },
            
            {
                      data: 'active_store_count',
                      bSortable: true,                       
                      mRender: function(data, type, row) {                                              
                        
                          return row.active_store_count;

                        } 
           },
           {
                      data: 'deactive_store_count',
                      bSortable: true,
                      mRender: function(data, type, row) {                                              
                        
                        return row.deactive_store_count;

                        } 
           },
            {
                      data: 'source_url',
                      bSortable: false,
                      mRender: function(data, type, row) {                                              
                        
                          return '<a href="'+row.source_url+'" target="_blank">View Dropship Item</a>';

                        } 
           },
         ]
      });
    });
    </script>
      
@endsection