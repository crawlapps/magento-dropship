<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Document</title>

    <style type="text/css">

        body{
            background: #ffffff;
            /*font-family: sans-serif;*/
            /*color:rgb(56, 56, 56);*/
            /*color: #6d6767;*/
        }

        section{
            padding: 15px;
        }
        ul {
            margin: 0px;
            padding: 0px;
        }
        li{
            list-style: none;
        }
        a{
            text-decoration: none;
        }
        .d-flex{
            display: flex;
        }
        .mt{
            margin-top: 80px;
        }
        .mt-5{
            margin-top: 50px;
        }
        .jc{
            justify-content: space-between;
        }
        .ai{
            align-items: center;
        }
        h2{
            font-size: 16px;
            color: #000;
        }
        .main-header, .main-sec{
            width: 100%;

        }
        .h-left{
            /*width: 60%;*/
        }
        .h-right{
            margin-top: 0px;
            position: absolute;
            top: 4%;
            right:15%
            /*width: 30%;*/
        }
        .h-right img{
            margin-top: 0px;
        }
        .h-left h1{
            font-size: 40px;
            padding-bottom: 30px;
            color: rgb(56, 56, 56);
            font-weight: 600;
        }
        .h-left ul li{
            font-size: 16px;
        }
        .h-left ul .box{

        }
        .h-left ul .box img{
            width: 100%;
            height: 100%;
            object-fit: cover;
        }

        /* -----------------------section1--------------- */
        .main-left{
            width: 50%;
        }
        .main-left .l-title{
            width: 30%;
            font-size: 16px;
        }
        .main-left .l-detail {
            font-size: 14px;
            line-height: 20px;
            /*width: 25%;*/
            color: rgb(56, 56, 56);
        }

        /*my css*/
        div.inline {
            float:left;
            margin: 0px 5px;
            padding: 0px 15px;
        }
        .clearBoth {
            clear:both;
        }

        /*START::Address css*/


          .address_title{
              margin-top: 0px;
          }


        /*END::Address css*/



        /*START::Line items css*/

         .line_item_table td{
             font-size: 14px;
         }
         .tr_head .th{
             border-top: 2px solid rgb(56, 56, 56);
             padding: 4px 0;
             border-bottom: 2px solid rgb(56, 56, 56);
         }
         .tr_item td{
             border-bottom: 2px solid rgb(207, 207, 207);
             padding: 15px 5px;
         }

         .td_qty .icon{
            padding: 0 60px;
        }

        .td_sku .icon p{
            font-size: 16px;
            font-weight: 00;
        }
        .td_qty .icon .text{
            font-size: 14px;
            font-weight: 700;
        }
        .td_img {
            width: 10%;
            height: 100px;
        }
        .td_img img {
            width: 70px;
            height: auto;
            object-fit: cover;
            padding: 5px;
        }
        .td_name {
            font-size: 14px;
            margin-right: 200px;
        }

        /*END::Line items css*/

        /*START::footer css*/

        .main-footer{
            text-align: center;
        }
        .main-footer p, .main-footer a{
            font-size: 14px;
            font-weight:inherit;
            margin: 5px;
            color:#777;
        }
        .main-footer a{
            color:#777;
            text-decoration: auto;
            padding: 5px;
        }
        .social-icon{
            justify-content: center;
            margin: 10px 0;
        }
        .f-detail h2{
            color: #000;
            margin-top: 30px;
            margin-bottom: 5px;
        }
        .f-detail p {
            font-size: 16px;
            color: #777;
            font-weight:inherit;
        }
        .icon-circle img{
            max-width: 50px;
            max-height: 50px;
            display: flex;
            font-size: 20px;
            justify-content: center;
            align-items: center;
            border-radius: 50%;
            color: #fff;
            margin: 0 8px;
        }

        .social-icon{
            justify-content: center;
            align-items: center;
            left: 30%;
            position: absolute;
            margin-top: 15px;

        }
        .social-facebook{
            width: 35px;
            height: 35px;
        }
        .social-instagram{
            width: 37px;
            height: 36px;
        }

        .social-twitter{
            width: 39px;
            height: 38px;
        }

        .f-website {
            color: #000;
            font-size: 16px;
        }

        /*END::footer css*/


    </style>
</head>
<body>

<section>
    <div class="container">
        <div class="main-header d-flex mt jc">
<table>
    <tbody>
    <tr>
        <td width="60%">
            <div class="h-left">
                <h1>Picking Slip</h1>
                <ul>
                    <li><strong>Order</strong> {{@$order['name']}}</li>
                    <li><img src="data:image/png;base64,{{DNS1DBarCode::getBarcodePNG($order['shopify_order_id'], 'I25')}}" alt="barcode" /></li>
                    <li><strong>Order Date</strong> {{$order['order_date']}}</li>
                    <li><strong>Shipping</strong> {{$order['client_name']}}</li>
                    <li><strong>Items</strong> {{$order['quantity']}}</li>
                    <li><strong>Weight</strong> {{$order['total_weight']}} g</li>
                </ul>
            </div>
        </td>
        <td width="40%">
            <div class="h-right">
                <img src="{{public_path('pdf/image/logo.png')}}" alt="">
            </div>
        </td>
    </tr>
    </tbody>
</table>

        </div>
    </div>
</section>



<!-- ----------------START::Address-------------- -->


<div class="inline">
        <p class="address_title"><strong>Bill to</strong></p>
</div>
<div class="inline">
    <div class="l-detail">
        {{@$order['billing']['name']}}<br>
        {{@$order['billing']['address1']}} <br>
        {{@$order['billing']['city']}}<br>
        {{@$order['billing']['province']}} {{@$order['billing']['country']}}<br>
        {{@$order['billing']['zip']}}
    </div>
</div>
<div class="inline">
        <p class="address_title"><strong>Ship to</strong></p>
</div>
<div class="inline">
    <div class="l-detail">
        {{@$order['shipping']['name']}}<br>
        {{@$order['shipping']['address1']}}<br>
        {{@$order['shipping']['city']}}<br>
        {{@$order['shipping']['province']}} {{@$order['shipping']['country']}}<br>
        {{@$order['shipping']['zip']}}
    </div>
</div>
<br class="clearBoth" />


<!-- ----------------END::Address-------------- -->



<!-- ----------------START::Line items-------------- -->


@if(count($order['line_items']) > 0)

    <?php
    $default_product_image = public_path('/images/defaultProduct.png');
    ?>
    <section>
        <div class="container">
            <div class="main-header d-flex mt jc">
    <div class="line_items_section">
        <table class="line_item_table">
            <thead>
            <tr class="tr_head">
                <th class="th" width="5%">Qty</th>
                <th class="th" width="5%"></th>
                <th class="th" width="10%">Item Description</th>
                <th class="th" width="80%"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($order['line_items'] as $item)
                <tr class="tr_item">
                    <td class="td_sku">
                        <strong> {{@$item['sku']}} </strong>
{{--                        {{@$item['sku']}} x {{@$item['quantity']}}--}}
                    </td>
                    <td class="td_qty">
                        <div class="icon">
                            <p><strong>x</strong></p>
                            <p class="text"><strong> {{@$item['quantity'] ? intval($item['quantity']) : 0 }} </strong></p>
                        </div>

                        {{--                        {{@$item['sku']}} x {{@$item['quantity']}}--}}
                    </td>
                    <td class="td_img"> <img  src="{{$item['images']!==null ? $item['images']['url'] : $default_product_image }}"
                                  alt="item.name"
                                  width="40px"
                                  class="img-fluid">
                    </td>
                    <td class="td_name">
                       {{@$item['name']}}
                    </td>
                </tr>

            @endforeach
            </tbody>
        </table>
    </div>
            </div>
        </div>
    </section>
@endif

<!-- ----------------END::Line items-------------- -->




<!-- ----------------START::Footer-------------- -->


<footer>
    <div class="container">
        <div class="main-footer mt-5">
            <h2>Thanks for your business!</h2>
            <p>If you have any questions, please do get in contact.</p>
            <a href="#">info@greedygwin.co.uk</a>
            <div class="f-detail">
                <h2><strong>Greedy Gwin</strong></h2>
                <p>DPJ TRADING LTD</p>
                <p>UNIT B8 WORKSPACE, 5-7 TOBERMORE ROAD, DRAPERSTOWN, BT457AG</p>
                <a href="www.greedydwin.co.uk"><strong class="f-website">www.greedydwin.co.uk</strong></a>
               <center>
                <div class="social-icon">
                    <div class="icon-circle inline">
                        <a href="https://www.facebook.com/shopify"> <img class="social-facebook" src="{{public_path('pdf/image/facebook.png')}}" alt="facebook"></a>
                    </div>
                    <div class="icon-circle inline">
                        <a href="https://instagram.com/shopify">  <img class="social-instagram" src="{{public_path('pdf/image/instagram.png')}}" alt="instagram"></a>
                    </div>
                    <div class="icon-circle inline">
                        <a href="https://twitter.com/shopify"> <img class="social-twitter" src="{{public_path('pdf/image/twitter.png')}}" alt="twitter"></a>
                    </div>
                    <br class="clearBoth" />
                </div>
              </center>
            </div>
        </div>
    </div>
</footer>




<!-- ----------------END::Footer-------------- -->















</body>
</html>
