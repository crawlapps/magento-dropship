<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('includes.head')

    @if(config('shopify-app.appbridge_enabled'))
        <script src="https://unpkg.com/@shopify/app-bridge"></script>
        <script>
            var AppBridge = window['app-bridge'];
            var createApp = AppBridge.default;

            window.shopify_app_bridge = createApp({
                apiKey: '{{ config('shopify-app.api_key') }}',
                shopOrigin: '{{ Auth::user()->name }}',
                forceRedirect: true,
            });
        </script>
    @endif
    <?php
    header("Content-Security-Policy: frame-ancestors https://".auth()->user()->name."  https://admin.shopify.com");
    ?>
</head>
<body>
<div class="">
{{--    <header class="row">--}}
{{--        @include('includes.header')--}}
{{--    </header>--}}

        <div id="app">
        @yield('content')
        </div>

{{--    <footer class="row">--}}
{{--        @include('includes.footer')--}}
{{--    </footer>--}}
</div>

<script src="{{ mix('/js/app.js') }}"></script>


</body>
</html>
