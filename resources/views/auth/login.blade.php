<!DOCTYPE html>

<html lang="en">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Shopify App</title>
    <meta name="title" content="Shopify App">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">    
    <link href="{{ asset('bootstrap-4.6.0/css/bootstrap.min.css') }}" rel="stylesheet">   
    <script src="{{ asset('bootstrap-4.6.0/js/bootstrap.min.js') }}" defer></script>
    <script src="{{ asset('js/jquery-3.5.1.min.js')}}"></script>
    <link href="{{ asset('css/login-page.css') }}" rel="stylesheet">
</head>
<body>
<main class="" role="main">
    <section class="login-block">
        <div class="row">
            <div class="col-lg-3 login-sec">
               <div class="logo-sec text-center">
               <img src="{{asset('images/my_beauty_site/logo.png')}}">
               </div>
                <div class="form-heading">
                   <h1 class="text-center">Enter your shopify domain to log in or install this app.</h1>
                </div>

                @if (session()->has('error'))
                    <div class="error">
                        <strong>Oops! An error has occured:</strong> {{ session('error') }}
                    </div>
                @endif

          

                <form method="POST" action="{{ route('authenticate') }}" class="login-form">
                    {{ csrf_field() }}
                        <input id="shop" name="shop" type="text" autofocus="autofocus" placeholder="example.myshopify.com" class="form-control marketing-input form_email" required>
                       
                        <button type="submit" class="arketing-button btn btn-login form-control">Install</button>
               </form>
                <div class="form-footer text-center">
                   <p>New to MyBeautySite? <span><a href="https://mybeautysite.com/" target="_blank">Start for free</a></span></p>
                </div>
              
           </div>
           <div class="col-lg-9 banner-sec">

            <div class="banner-heading">
                  <h1>#1 Beauty DropShipper</h1>
                  <h4>Shopify App</h4>
            </div>
               
            <div class="mockup-sec desktop-view d-none d-md-block" >     
                 <div class="row justify-content-between">
                     <div class="d-md-flex align-items-center grp_1">
                         
                        <img src="{{asset('images/my_beauty_site/Group1.png')}}" class="img-fluid">
                        <p class="ml-3 mb-0">1000+ Authentic <br>
                            Beauty Products</p>
                     </div>
                     <div class="d-md-flex align-items-center grp_2">
                        <p class="mr-3 mb-0">Effortless <br>
                            Scaling</p>
                        <img src="{{asset('images/my_beauty_site/Group2.png')}}" class="img-fluid">
                        
                     </div>
                 </div>
                 <div class="row justify-content-between align-items-center ">
                    <div class="text-center grp_3">
                        <img src="{{asset('images/my_beauty_site/Group3.png')}}" class="img-fluid">

                        <p class="mt-3 mb-0">Unmatched <br>
                            Selection</p>
                    </div>
                    <div class="center_img">
                       <img src="{{asset('images/my_beauty_site/Mockup1.png')}}" class="img-fluid">
                    </div>
                    <div class="text-center grp_4">
                        <img src="{{asset('images/my_beauty_site/Group4.png')}}" class="img-fluid">
                        <p class="mt-3 mb-0">Profit</p>

                     </div>
                </div>
                <div class="row justify-content-between">
                    <div class="d-md-flex align-items-center grp_5">
                       <img src="{{asset('images/my_beauty_site/Group5.png')}}" class="img-fluid">
                       <p class="ml-3 mb-0">Multiple Beauty Niches</p>

                    </div>
                    <div class="d-md-flex align-items-center grp_6">
                        <p class="mr-3 mb-0">100 % Free to use</p>
                       <img src="{{asset('images/my_beauty_site/Group6.png')}}"  class="img-fluid">
                    </div>
                </div>
            </div>

            <div class="mockup-sec mobile-view d-block d-md-none">     
                <div class="row justify-content-between text-center">
                    <div class="d-md-flex align-items-center my-4">
                       <img src="{{asset('images/my_beauty_site/Group1.png')}}" class="img-fluid">
                       <p class="ml-3 mb-0">1000+ Authentic 
                           Beauty Products</p>
                    </div>
                    <div class="d-md-flex align-items-center my-4">
                       
                       <img src="{{asset('images/my_beauty_site/Group2.png')}}" class="img-fluid">
                       <p class="mr-3 mb-0">Effortless 
                        Scaling</p>
                       
                    </div>
                </div>
                <div class="row justify-content-between align-items-center ">
                   <div class="text-center my-4">
                       <img src="{{asset('images/my_beauty_site/Group3.png')}}" class="img-fluid">

                       <p class="mt-3 mb-0">Unmatched 
                           Selection</p>
                   </div>
                   <div class="center_img my-5">
                      <img src="{{asset('images/my_beauty_site/Mockup1.png')}}" class="img-fluid">
                   </div>
                   <div class="text-center my-4">
                       <img src="{{asset('images/my_beauty_site/Group4.png')}}" class="img-fluid">
                       <p class="mt-3 mb-0">Profit</p>

                    </div>
               </div>
               <div class="row justify-content-between text-center">
                   <div class="d-md-flex align-items-center my-4">
                      <img src="{{asset('images/my_beauty_site/Group5.png')}}" class="img-fluid">
                      <p class="ml-3 mb-0">Multiple Beauty Niches</p>

                   </div>
                   <div class="d-md-flex align-items-center my-4">
                       
                      <img src="{{asset('images/my_beauty_site/Group6.png')}}"  class="img-fluid">
                      <p class="mr-3 mb-0">100 % Free to use</p>
                   </div>
               </div>
           </div>

            <section class="video-sec"> 
                <div class="videoWrapper">
                    <!-- Copy & Pasted from YouTube -->
                          <iframe  src="https://stylistsolutions.wistia.com/medias/td13r2lbsj" frameborder="0" allowfullscreen></iframe>
                          <!-- <p class="position-absolute">Shopify App Overview</p> -->
                    </div>
                    
            </section>
        
            <section class="footer-logo-sec">
                <div class="align-items-center">
                    <div class="text-center my-4">
                        <img width="200px" src="{{asset('images/my_beauty_site/Shopify-Logo.png')}}" class="img-fluid">
                    </div>
                </div>
            </section>
            <section class="footer-sec"> 
                <ul class="footer-link text-center">
                    <li><a href="https://stylistsolutions.ladesk.com/" target="_blank">Help Center</a> |</li>
                    <li><a href="https://mybeautysite.com/" target="_blank">Open wholesale account</a> </li>             
                </ul>
            </section>               
           </div>
           </div>   
        </div>
   </section>  
</main>
</body>
</html>
