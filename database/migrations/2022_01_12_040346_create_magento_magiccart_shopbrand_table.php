<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMagentoMagiccartShopbrandTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('magento_magiccart_shopbrand', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('shopbrand_id')->unsigned()->nullable()->comment('shopbrand ID');
            $table->string('title')->nullable()->comment('brand title');
            $table->string('urlkey')->nullable()->comment('brand urlkey');
            $table->string('meta_key')->nullable();
            $table->string('meta_description')->nullable();
            $table->bigInteger('option_id')->unsigned()->nullable()->comment('Brand ID');
            $table->string('image')->nullable();
            $table->string('product_ids')->nullable();
            $table->integer('stores')->default(0)->comment('Store ID');
            $table->string('description')->nullable();
            $table->integer('order')->default(0);
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('magento_magiccart_shopbrand');
    }
}
