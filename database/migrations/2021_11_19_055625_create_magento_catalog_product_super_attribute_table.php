<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMagentoCatalogProductSuperAttributeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('magento_catalog_product_super_attribute', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('product_super_attribute_id')->unsigned()->nullable()->comment('Product Super Attribute ID');
            $table->integer('product_id')->default(0)->comment('Product ID');
            $table->integer('attribute_id')->default(0)->comment('Attribute ID');
            $table->integer('position')->default(0)->comment('Position');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('magento_catalog_product_super_attribute', function (Blueprint $table) {
            //
        });
    }
}
