<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMagentoEavAttributeOptionValueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('magento_eav_attribute_option_value', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('value_id')->unsigned()->nullable()->comment('Value ID');
            $table->bigInteger('option_id')->unsigned()->nullable()->comment('Option ID');
            $table->integer('store_id')->default(0)->comment('Store ID');
            $table->string('value')->nullable()->comment('Value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('magento_eav_attribute_option_value', function (Blueprint $table) {
            //
        });
    }
}
