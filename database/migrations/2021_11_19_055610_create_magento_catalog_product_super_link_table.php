<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMagentoCatalogProductSuperLinkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('magento_catalog_product_super_link', function (Blueprint $table) {
            $table->id();
            $table->integer('link_id')->default(0)->comment('Link ID');
            $table->integer('product_id')->default(0)->comment('simple Product ID');
            $table->integer('parent_id')->default(0)->comment('configrable Parent ID');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('magento_catalog_product_super_link', function (Blueprint $table) {
            //
        });
    }
}
