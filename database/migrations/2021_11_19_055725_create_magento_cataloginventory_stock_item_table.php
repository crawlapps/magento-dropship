<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMagentoCataloginventoryStockItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('magento_cataloginventory_stock_item', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('item_id')->unsigned()->nullable()->comment('Item ID');
            $table->bigInteger('product_id')->unsigned()->nullable()->comment('Product ID');
            $table->integer('stock_id')->default(0)->comment('Stock ID');
            $table->string('qty')->nullable()->comment('Qty');
            $table->string('min_qty')->nullable()->comment('Min Qty');
            $table->integer('use_config_min_qty')->default(1)->comment('Use Config Min Qty');
            $table->integer('is_qty_decimal')->default(0)->comment('Is Qty Decimal');
            $table->integer('backorders')->default(0)->comment('Backorders');
            $table->integer('use_config_backorders')->default(1)->comment('Use Config Backorders');
            $table->string('min_sale_qty')->nullable()->comment('Min Sale Qty');
            $table->integer('use_config_min_sale_qty')->default(1)->comment('Use Config Min Sale Qty');
            $table->string('max_sale_qty')->nullable()->comment('Max Sale Qty');
            $table->integer('use_config_max_sale_qty')->default(1)->comment('Use Config Max Sale Qty');
            $table->integer('is_in_stock')->default(0)->comment('Is In Stock');
            $table->string('low_stock_date')->nullable()->comment('Low Stock Date');
            $table->string('notify_stock_qty')->nullable()->comment('Notify Stock Qty');
            $table->integer('use_config_notify_stock_qty')->default(1)->comment('Use Config Notify Stock Qty');
            $table->integer('manage_stock')->default(0)->comment('Manage Stock');
            $table->integer('use_config_manage_stock')->default(1)->comment('Use Config Manage Stock');
            $table->integer('stock_status_changed_auto')->default(0)->comment('Stock Status Changed Automatically');
            $table->integer('use_config_qty_increments')->default(1)->comment('Use Config Qty Increments');
            $table->string('qty_increments')->nullable()->comment('Qty Increments');
            $table->integer('use_config_enable_qty_inc')->default(1)->comment('Use Config Enable Qty Increments');
            $table->integer('enable_qty_increments')->default(0)->comment('Enable Qty Increments');
            $table->integer('is_decimal_divided')->default(0)->comment('Is Divided into Multiple Boxes for Shipping	');
            $table->integer('website_id')->default(0)->comment('Website ID');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('magento_cataloginventory_stock_item', function (Blueprint $table) {
            //
        });
    }
}
