<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMembershipColumnToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->boolean('membership_for_free_store')->default(0)->comment('0 = (No), 1 = (Yes)')->after('is_exist_in_shopify');
            $table->boolean('membership_for_limited_store')->default(0)->comment('0 = (No), 1 = (Yes)')->after('membership_for_free_store');
            $table->boolean('membership_for_enterprize_store')->default(0)->comment('0 = (No), 1 = (Yes)')->after('membership_for_limited_store');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            //
        });
    }
}
