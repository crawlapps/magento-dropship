<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLineItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('line_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('db_order_id');
            $table->string('shopify_lineitem_id')->nullable();
            $table->string('name')->nullable();
            $table->bigInteger('product_id');
            $table->string('sku')->nullable();
            $table->decimal('quantity')->nullable();
            $table->boolean('mark_as_missing_status')->default(0)->comment('0 = false, 1 = true');
            $table->timestamps();
            $table->foreign('db_order_id')->references('id')->on('orders')->onUpdate('NO ACTION')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('line_items');
    }
}
