<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMagentoIntegrationApiProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('magento_integration_api_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('product_id')->default(0)->comment('product_id');
            $table->string('sku')->nullable()->comment('sku');
            $table->json('products_response')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('magento_integration_api_products');
    }
}
