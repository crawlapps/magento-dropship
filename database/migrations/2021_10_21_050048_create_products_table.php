<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('shopify_id')->unsigned()->nullable();
            $table->bigInteger('magento_product_id')->unsigned();
            $table->string('shopify_handle')->nullable();
            $table->longText('collection_id')->nullable();
            $table->string('type', 150);
            $table->char('locale', 2)->default('US');
            $table->enum('source', ['Amazon', 'Walmart','Magento'])->default('Magento');
            $table->string('source_url', 2100);
            $table->string('source_currency')->nullable();
            $table->string('source_currency_symbol')->nullable();
            $table->longText('description')->nullable();
            $table->boolean('show_prices')->default(1);
            $table->longText('saller_ranks')->nullable();
            $table->json('reviews')->nullable();
            $table->json('history')->nullable();
            $table->string('vendor')->nullable();
            $table->boolean('status')->default(1)->comment('0 = Deactiv, 1 = Active');
            $table->boolean('is_exist_in_shopify')->default(0)->comment('0 = pending (No), 1 = added (Yes)');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
