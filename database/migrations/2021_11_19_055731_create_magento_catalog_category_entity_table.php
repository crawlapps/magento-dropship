<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMagentoCatalogCategoryEntityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('magento_catalog_category_entity', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('entity_id')->unsigned()->nullable()->comment('Entity ID');
            $table->bigInteger('attribute_set_id')->unsigned()->nullable()->comment('Attribute Set ID');
            $table->integer('parent_id')->default(0)->comment('Parent Category ID');
            $table->string('created_at', 150)->nullable()->comment('Creation Time');
            $table->string('updated_at', 150)->nullable()->comment('Update Time');
            $table->string('path')->nullable()->comment('Tree Path');
            $table->integer('position')->default(0)->comment('Position');
            $table->integer('level')->default(0)->comment('Tree Level');
            $table->integer('children_count')->default(0)->comment('Child Count');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('magento_catalog_category_entity', function (Blueprint $table) {
            //
        });
    }
}
