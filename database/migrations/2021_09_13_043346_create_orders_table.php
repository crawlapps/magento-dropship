<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('shopify_order_id')->nullable();
            $table->string('name')->nullable();
            $table->string('client_name')->nullable()->comment('Shipping address name');
            $table->string('email')->nullable();
            $table->json('billing_address')->nullable();
            $table->json('shipping_address')->nullable();
            $table->integer('total_weight')->default(0);
            $table->string('fulfillment_status')->nullable();
            $table->string('order_number')->nullable();
            $table->boolean('is_assigned')->default(0)->comment('0 = order_number is not assigned (Default), 1 = order_number is assigned');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
