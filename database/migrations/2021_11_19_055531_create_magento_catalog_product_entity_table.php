<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMagentoCatalogProductEntityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('magento_catalog_product_entity', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('entity_id')->unsigned()->comment('Entity ID');
            $table->integer('attribute_set_id')->default(0)->comment('Attribute Set ID');
            $table->string('type_id')->nullable()->comment('Type ID');
            $table->string('sku')->nullable()->comment('SKU');
            $table->integer('has_options')->default(0)->comment('Has Options');
            $table->integer('required_options')->default(0)->comment('Required Options');
            $table->string('created_at', 150)->nullable()->comment('Creation Time');
            $table->string('updated_at', 150)->nullable()->comment('Update Time');            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('magento_catalog_product_entity', function (Blueprint $table) {
            //
        });
    }
}
