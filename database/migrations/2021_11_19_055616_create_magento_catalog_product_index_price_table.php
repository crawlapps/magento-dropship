<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMagentoCatalogProductIndexPriceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('magento_catalog_product_index_price', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('entity_id')->unsigned()->nullable()->comment('Entity ID');
            $table->integer('customer_group_id')->default(0)->comment('Customer Group ID');
            $table->integer('website_id')->default(0)->comment('Website ID');
            $table->integer('tax_class_id')->default(0)->comment('Tax Class ID');
            $table->string('price')->nullable()->comment('Price');
            $table->string('final_price')->nullable()->comment('Final Price');
            $table->string('min_price')->nullable()->comment('Min Price');
            $table->string('max_price')->nullable()->comment('Max Price');
            $table->string('tier_price')->nullable()->comment('Tier Price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('magento_catalog_product_index_price', function (Blueprint $table) {
            //
        });
    }
}
