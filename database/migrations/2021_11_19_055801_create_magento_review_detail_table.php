<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMagentoReviewDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('magento_review_detail', function (Blueprint $table) {
            $table->id();
            $table->integer('detail_id')->default(0)->comment('Review detail ID');
            $table->bigInteger('review_id')->unsigned()->nullable()->comment('Review ID');
            $table->bigInteger('store_id')->unsigned()->nullable()->comment('Store ID');
            $table->string('title')->nullable()->comment('Title');
            $table->string('detail')->nullable()->comment('Detail description');
            $table->string('nickname')->nullable()->comment('User nickname');
            $table->integer('customer_id')->default(0)->comment('Customer ID');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('magento_review_detail', function (Blueprint $table) {
            //
        });
    }
}
