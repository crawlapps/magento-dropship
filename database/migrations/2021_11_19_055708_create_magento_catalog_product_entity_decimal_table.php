<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMagentoCatalogProductEntityDecimalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('magento_catalog_product_entity_decimal', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('value_id')->unsigned()->nullable()->comment('Value ID');
            $table->bigInteger('attribute_id')->unsigned()->nullable()->comment('Attribute ID');
            $table->integer('store_id')->default(0)->comment('Store ID');
            $table->integer('entity_id')->default(0)->comment('Entity ID');
            $table->string('value')->nullable()->comment('Value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('magento_catalog_product_entity_decimal', function (Blueprint $table) {
            //
        });
    }
}
