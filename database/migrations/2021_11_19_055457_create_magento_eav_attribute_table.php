<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMagentoEavAttributeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('magento_eav_attribute', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('attribute_id')->unsigned()->nullable()->comment('Attribute ID');
            $table->bigInteger('entity_type_id')->unsigned()->default(0)->comment('Entity Type ID');
            $table->string('attribute_code')->nullable()->comment('Attribute Code');
            $table->longText('attribute_model')->nullable()->comment('Attribute Model');
            $table->longText('backend_model')->nullable()->comment('Backend Model');
            $table->string('backend_type')->nullable()->comment('Backend Type');
            $table->string('backend_table')->nullable()->comment('Backend Table');
            $table->longText('frontend_model')->nullable()->comment('Frontend Model');
            $table->string('frontend_input')->nullable()->comment('Frontend Input');
            $table->string('frontend_label')->nullable()->comment('Frontend Label');
            $table->string('frontend_class')->nullable()->comment('Frontend Class');
            $table->longText('source_model')->nullable()->comment('Source Model');
            $table->integer('is_required')->default(0)->comment('Defines Is Required');
            $table->integer('is_user_defined')->default(0)->comment('Defines Is User Defined');
            $table->text('default_value')->nullable()->comment('Default Value');
            $table->integer('is_unique')->default(0)->comment('Defines Is Unique');
            $table->text('note')->nullable()->comment('Note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('magento_eav_attribute', function (Blueprint $table) {
            //
        });
    }
}
