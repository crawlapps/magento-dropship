<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMagentoEavAttributeOptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('magento_eav_attribute_option', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('option_id')->unsigned()->nullable()->comment('Option ID');
            $table->integer('attribute_id')->default(0)->comment('Attribute ID');
            $table->integer('sort_order')->default(0)->comment('Sort Order');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('magento_eav_attribute_option', function (Blueprint $table) {
            //
        });
    }
}
