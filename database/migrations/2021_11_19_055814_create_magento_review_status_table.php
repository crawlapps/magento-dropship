<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMagentoReviewStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('magento_review_status', function (Blueprint $table) {
            $table->id();
            $table->integer('status_id')->default(0)->comment('Status ID');
            $table->string('status_code')->nullable()->comment('Status code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('magento_review_status', function (Blueprint $table) {
            //
        });
    }
}
