<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMagentoReviewEntityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('magento_review_entity', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('entity_id')->default(0)->comment('Review entity ID');
            $table->string('entity_code')->nullable()->comment('Review entity code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('magento_review_entity', function (Blueprint $table) {
            //
        });
    }
}
