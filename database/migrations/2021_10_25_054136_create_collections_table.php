<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collections', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('shopify_id')->unsigned()->nullable();
            $table->bigInteger('magento_collection_id')->unsigned();
            $table->string('shopify_handle')->nullable();
            $table->string('title');
            $table->enum('source', ['Amazon', 'Walmart','Magento'])->default('Magento');
            $table->longText('body_html')->nullable();
            $table->string('sort_order')->nullable();
            $table->boolean('status')->default(1)->comment('0 = Deactiv, 1 = Active');
            $table->boolean('is_exist_in_shopify')->default(0)->comment('0 = pending (No), 1 = added (Yes)');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collections');
    }
}
