<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMagentoCatalogProductSuperAttributeLabelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('magento_catalog_product_super_attribute_label', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('value_id')->unsigned()->nullable()->comment('Value ID');
            $table->bigInteger('product_super_attribute_id')->unsigned()->nullable()->comment('Product Super Attribute ID');
            $table->integer('store_id')->default(0)->comment('Store ID');
            $table->integer('use_default')->default(0)->comment('Use Default Value');
            $table->string('value')->nullable()->comment('Value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('magento_catalog_product_super_attribute_label', function (Blueprint $table) {
            //
        });
    }
}
