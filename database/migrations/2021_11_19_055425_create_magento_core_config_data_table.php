<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMagentoCoreConfigDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('magento_core_config_data', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('config_id')->unsigned();
            $table->string('scope')->nullable();
            $table->bigInteger('scope_id')->unsigned()->default(0);
            $table->string('path')->nullable();
            $table->longText('value')->nullable();
            $table->string('updated_at', 150)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('magento_core_config_data', function (Blueprint $table) {
            //
        });
    }
}
