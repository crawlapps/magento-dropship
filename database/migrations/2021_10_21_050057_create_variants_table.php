<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('variants', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('product_id')->unsigned();
            $table->string('shopify_id')->nullable();
            $table->bigInteger('image_id')->unsigned();
            $table->boolean('is_main')->default(0);
            $table->string('title')->nullable();
            $table->string('sku')->nullable();
            $table->boolean('in_stock')->default(1);
            $table->string('inventory_policy')->nullable();
            $table->string('inventory_management')->nullable();
            $table->boolean('prime_eligible')->default(0);
            $table->string('shipping_type', 150)->default('Standard Shipping');
            $table->decimal('source_price', 8, 2)->default('0.00');
            $table->decimal('shipping_price', 8, 2)->default('0.00');
            $table->decimal('shopify_price', 8, 2)->default('0.00');
            $table->string('weight')->nullable();
            $table->string('weight_unit')->nullable();
            $table->timestamps();
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('image_id')->references('id')->on('images');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('variants');
    }
}
