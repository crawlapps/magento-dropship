<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMagentoCatalogCategoryProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('magento_catalog_category_product', function (Blueprint $table) {
            $table->id();
            $table->integer('entity_id')->default(0)->comment('Entity ID');
            $table->bigInteger('category_id')->unsigned()->nullable()->comment('Category ID');
            $table->bigInteger('product_id')->unsigned()->nullable()->comment('Product ID');
            $table->integer('position')->default(0)->comment('Position');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('magento_catalog_category_product', function (Blueprint $table) {
            //
        });
    }
}
