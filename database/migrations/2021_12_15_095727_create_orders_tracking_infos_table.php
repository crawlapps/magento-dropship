<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTrackingInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_tracking_infos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('db_order_id');
            $table->string('track_order_id')->nullable();
            $table->string('track_number')->nullable();
            $table->string('title')->nullable();
            $table->string('carrier_code')->nullable();
            $table->json('tracking_infos_more')->nullable();
            $table->timestamps();
            $table->foreign('db_order_id')->references('id')->on('orders')->onUpdate('NO ACTION')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_tracking_infos');
    }
}
