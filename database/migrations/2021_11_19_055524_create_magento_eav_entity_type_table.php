<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMagentoEavEntityTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('magento_eav_entity_type', function (Blueprint $table) {
            $table->id();
            $table->integer('entity_type_id')->unsigned()->nullable()->comment('Entity Type ID');
            $table->string('entity_type_code')->nullable()->comment('Entity Type Code');
            $table->string('entity_model')->nullable()->comment('Entity Model');
            $table->longText('attribute_model')->nullable()->comment('Attribute Model');
            $table->longText('entity_table')->nullable()->comment('Entity Table');
            $table->string('value_table_prefix')->nullable()->comment('Value Table Prefix');
            $table->string('entity_id_field')->nullable()->comment('Entity ID Field');
            $table->integer('is_data_sharing')->default(1)->comment('Defines Is Data Sharing');
            $table->string('data_sharing_key')->nullable()->comment('Data Sharing Key');
            $table->integer('default_attribute_set_id')->default(0)->comment('Default Attribute Set ID');
            $table->string('increment_model')->nullable()->comment('Increment Model');
            $table->integer('increment_per_store')->nullable()->comment('Increment Per Store');
            $table->integer('increment_pad_length')->default(8)->comment('Increment Pad Length');
            $table->string('increment_pad_char')->default(0)->comment('Increment Pad Char');
            $table->text('additional_attribute_table')->nullable()->comment('Additional Attribute Table');
            $table->text('entity_attribute_collection')->nullable()->comment('Entity Attribute Collection');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('magento_eav_entity_type', function (Blueprint $table) {
            //
        });
    }
}
