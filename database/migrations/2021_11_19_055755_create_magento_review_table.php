<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMagentoReviewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('magento_review', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('review_id')->unsigned()->nullable()->comment('Review ID');
            $table->string('created_at', 150)->nullable()->comment('Review create date');
            $table->integer('entity_id')->default(0)->comment('Entity ID');
            $table->integer('entity_pk_value')->default(0)->comment('Product ID');
            $table->integer('status_id')->default(0)->comment('Status code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('magento_review', function (Blueprint $table) {
            //
        });
    }
}
