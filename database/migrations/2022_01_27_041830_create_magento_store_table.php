<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMagentoStoreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('magento_store', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('store_id')->unsigned()->nullable()->comment('Store ID');
            $table->string('code')->nullable()->comment('Code');
            $table->bigInteger('website_id')->default(0)->comment('Website ID');
            $table->integer('group_id')->default(0)->comment('Group ID');
            $table->string('name')->nullable()->comment('Store Name');
            $table->integer('sort_order')->default(0)->comment('Store Sort Order');
            $table->integer('is_active')->default(0)->comment('Store Activity');
            $table->integer('vendor_id')->default(0)->comment('Vendor Id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('magento_store');
    }
}
