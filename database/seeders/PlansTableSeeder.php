<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use exception;
class PlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            logger("========START :: PlansTableSeeder=============");

              if(DB::table('plans')->count()==0){

                   DB::table('plans')->insert([
                        "type" => "ONETIME",
                        "name" => "ONETIME",
                        "price" => 9.99,
                        "capped_amount" => 0.00,
                        "terms" => "ONETIME",
                        "trial_days" => 0,
                        "test" => 0,
                        "on_install" => 1,
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s")
                   ]);
              }

            logger("========END :: PlansTableSeeder=============");

        }catch(Exception $e){
            logger("===============ERROR :: PlansTableSeeder ===============");
            logger(json_encode($e->getMessage()));
        }
    }
}
