<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Global constants
    |--------------------------------------------------------------------------
    */

    'integration_api_url'  => env('INTEGRATION_API_URL', 'https://hairbawse.com'),

];
