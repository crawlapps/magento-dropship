<?php

use App\Http\Controllers\OrdersController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TestController;
use App\Http\Controllers\AppController;
use App\Http\Controllers\MagentoProductsController;
use App\Http\Controllers\ShopifyProductsController;
use App\Http\Controllers\CollectionsController;




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/login', function () {
    return view('auth.login');
})->name('login');

Route::get('/', function () {
    return view('pages.home');
})->middleware(['verify.shopify'])->name('home');

Route::get('flush', function(){
    request()->session()->flush();
});

Route::get('/test-connect-db2', [TestController::class, 'testConnectDB2']);

Route::group(['middleware' => ['verify.shopify']], function () {

    Route::get('/test', [TestController::class, 'testModeIndex']);

    Route::get('/test-copy-table', [TestController::class, 'CopyMagentoTable']);
    Route::get('/test-delete-membership-product', [TestController::class, 'DeleteMemberShipProductCron']);

// START ::  Magento Products----------------------------------------------------------------------
    Route::prefix('products')->group(function () {
        Route::post('/get-products', [MagentoProductsController::class, 'getProducts']);
        Route::post('/get-products-variants', [MagentoProductsController::class, 'getProductsVariants']);
        Route::post('/add-products', [MagentoProductsController::class, 'addProducts']);

        //Filter listing
        Route::post('/get-products-type', [MagentoProductsController::class, 'getProductsTypes']);
        Route::post('/get-products-category', [MagentoProductsController::class, 'getProductsCategory']);
        //Filter listing :: END
    });
// END ::  Magento Products-------------------------------------------------------------------------


// START ::  Shopify Products-------------------------------------------------------------------------
    Route::prefix('my-products')->group(function () {
        Route::post('/get-products', [ShopifyProductsController::class, 'getMyProducts']);
        Route::post('/get-products-variants', [ShopifyProductsController::class, 'getMyProductsVariants']);
        Route::post('/delete-product', [ShopifyProductsController::class, 'deleteProduct']);
        Route::get('/delete-product-all', [ShopifyProductsController::class, 'deleteProductAll']);
        Route::get('/get-reviews', [ShopifyProductsController::class, 'getReview']);
        Route::post('/change/auto-update-inventory/status', [ShopifyProductsController::class, 'ChangeAutoUpdateInventoryStatus']);
    });
 // END ::  Shopify Products-------------------------------------------------------------------------


    Route::get('/collections/get-collection', [CollectionsController::class, 'getCollections']);
    Route::post('/collections/add-collection', [CollectionsController::class, 'addCollections']);
    Route::post('/collections/sync-collection-product', [CollectionsController::class, 'SyncCategoryProducts']);
    Route::post('/collections/delete-collection', [CollectionsController::class, 'deleteCollection']);
    Route::post('/get-collections-category',   [AppController::class, 'getCollectionsCategory']);

    Route::post('/orders/get-orders', [OrdersController::class, 'getOrders']);
    Route::get('/orders/order-details/{id}', [OrdersController::class, 'getOrdersDetails']);
    Route::post('/orders/assign-order-number', [OrdersController::class, 'assignOrderNumber']);
    Route::post('/orders/assign-order-number/confirm', [OrdersController::class, 'assignOrderNumberConfirm']);
    Route::post('/orders/find-tracking-info', [OrdersController::class, 'findTrackingInfo']);
    Route::post('/orders/get-tracking-info', [OrdersController::class, 'getTrackingInfo']);

    Route::get('/get-magento-credentials',   [AppController::class, 'getMagentoCredentials']);
    Route::post('/save-magento-credentials',   [AppController::class, 'saveMagentoCredentials']);

});

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('admin.home');


Route::get('/admin/login', [App\Http\Controllers\Auth\LoginController::class, 'showAdminLoginForm'])->name('admin.login');
Route::post('/admin/login', [App\Http\Controllers\Auth\LoginController::class, 'authenticate'])->name('admin.authenticate');


Route::group(['prefix' => 'admin','middleware' => ['auth:admin'],'namespace'=>'Admin'], function() {

    Route::get('/', [App\Http\Controllers\Admin\DashboardController::class, 'Dashboard'])->name('admin.dashboard');
    Route::get('/products', [App\Http\Controllers\Admin\ProductsController::class, 'index'])->name('admin.products');
    Route::get('/products/getProducts', [App\Http\Controllers\Admin\ProductsController::class, 'getProducts'])->name('admin.getProducts');
    Route::get('/products/variants/{product_id?}', [App\Http\Controllers\Admin\ProductsController::class, 'getMyProductsVariants'])->name('admin.products.variants');
});


Auth::routes();
